var id = chrome.contextMenus.create({"title": "Send to Focal", "contexts":["link"],
                                       "onclick": linkClick});
function linkClick(info, tab){
    chrome.tabs.getSelected(null, function(tab) {
      chrome.tabs.sendRequest(tab.id, {id: "popup", url: info.linkUrl}, function(response) {
        //console.log(response.farewell);
      });
    });
}


var firstRun = localStorage.getItem('firstRun');
if(!firstRun){
    chrome.windows.getAll({populate : true}, function (window_list) {
            var list = [];
            for(var i=0;i<window_list.length;i++) {
                list = list.concat(window_list[i].tabs);
            }
            for(var i=0;i<list.length;i++){
                chrome.tabs.executeScript(list[i].id, { file: "contentScript.js" });
            }
    });
    localStorage.setItem('firstRun', '0');
}
jQuery.fn.extend({
	getPosition: function(relative){
		if ( !this[0] ) {
			return null;
		}

		var elem = this.eq(0),
			offset = elem.offset(),
			scroll = {'left':elem.scrollLeft(), 'top':elem.scrollTop()},
			position = {
				'left': offset.left - scroll.left,
				'top' : offset.top - scroll.top
			};

		if (relative){
			var relativePosition = relative.getPosition();
			return {
				'left': position.left - relativePosition.left - 
							(parseFloat( jQuery.css(relative[0], "borderLeftWidth") ) || 0), 
				'top' : position.top - relativePosition.top - 
							(parseFloat( jQuery.css(relative[0], "borderTopWidth") ) || 0)
			}
		}
		return position;
	},
	getCoordinates: function(element){
		//if (isBody(this)) return this.getWindow().getCoordinates();
		if ( !this[0] ) {
			return null;
		}

		var elem = this.eq(0);
		var position = elem.getPosition(element),
			size = {'width':elem.outerWidth(), 'height': elem.outerHeight()};
		var obj = {
			left: position.left,
			top: position.top,
			width: size.width,
			height: size.height
		};
		obj.right = obj.left + obj.width;
		obj.bottom = obj.top + obj.height;
		return obj;
	}
});	

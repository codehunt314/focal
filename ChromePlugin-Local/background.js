var id = chrome.contextMenus.create({"title": "Local Send to Focal", "contexts":["link"],
                                       "onclick": linkClick});
function linkClick(info, tab){
    //console.log(JSON.stringify(info));
    chrome.tabs.getSelected(null, function(tab) {
      chrome.tabs.sendRequest(tab.id, {id: "popup", url: info.linkUrl}, function(response) {
        //console.log(response.farewell);
      });
    });
}


var firstRun = localStorage.getItem('firstRun');
if(!firstRun){
    chrome.windows.getAll({populate : true}, function (window_list) {
            var list = [];
            for(var i=0;i<window_list.length;i++) {
                list = list.concat(window_list[i].tabs);
            }
            for(var i=0;i<list.length;i++){
                chrome.tabs.executeScript(list[i].id, { file: "contentScript.js" });
            }
    });
    localStorage.setItem('firstRun', '0');
}

/*function initDB(){
    saveObj('activities',[
        {
            'id':1,
            'name':'Random',
            'total_links':240,
            'your_links':33,
            'new':['http://www.google.com','http://www.facebook.com'],
            'users':['nitinhayaran@gmail.com','priyanka.hayaran@gmail.com']
        },
        {
            'id':2,
            'name':'Personal',
            'total_links':20,
            'your_links':20,
            'new':['http://www.google.com','http://www.facebook.com'],
            'users':['nitinhayaran@gmail.com']
        }
    ]);
    saveObj('links', [
        [
            1,
            'http://www.google.com',
            'Google Home Page',
            'vipul'
        ],
        [
            1,
            'http://code.google.com/chrome/extensions/tabs.html',
            'Google Home Page',
            'vipul'
        ]
    ]);
}*/
/*
function get_timestamp(){
  var timestamp = localStorage.getItem('time_stamp');
	return (timestamp) ? timestamp : "";
}
function set_timestamp(t){
  localStorage.setItem('time_stamp',t);
}
function init_sync(){
  //if(this.counter==0){localStorage.removeItem('new_count');localStorage.removeItem('activities');localStorage.removeItem('time_stamp');localStorage.removeItem('links');}
  this.counter++;
	var jsonRequest = new Request.JSON({url: DOMAIN + '/sync_activity/', onSuccess: function(obj){
		set_timestamp(obj.time_stamp);
		saveActivityObj(obj['activites']);
		processLinks(obj['links']);
	}}).get({'timestamp':get_timestamp()});
}

function processLinks(links){
  links.each(function(link){
    if(link[4]=='add'){
      updateLinkInDB(link[1], link[2], link[0], link[3], 1);
      //url, title, activity_id, user
      addNewLink(link[0], link[1], link[3]);
    }else{
      updateLinkInDB(link[1], link[2], link[0], link[4], 0);
    }
  });
  updateActivityCount();
}


function addNewLink(act_id, link, user){
  if(user != activeUser()){
    var flag = false;
    var activities = getObj('activities');
    for(var i=0;i<activities.length; i++){
        if(activities[i].id == act_id){
            activities[i]['new'].include(link);
            flag = true;            
        }
    }
    if(flag){
      saveObj('activities',activities);
      chrome.browserAction.setBadgeText({text:String(modNewCount(1,1))});
    }
  }
}

function saveActivityObj(activites){
	var act_to_store = [];
	for(var i=0;i<activites.length;i++){
	  var act = activites[i];
		act_to_store[act_to_store.length] = {
            'id':act.id,
            'name':act.name,
            'total_links':0,
            'your_links':0,
            'new':[],
            'users':[act.created_by]
      };
	}
	saveObj('activities', act_to_store)
}
*/
//var Site = { counter: 0 };
//init_sync.periodical(1000*60*5, Site);
//var myBoundFunction = init_sync.bind(Site);
//myBoundFunction();

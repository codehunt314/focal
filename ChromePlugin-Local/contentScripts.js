var Focalify = function() {
    var HOST = "localhost";
    
    
	return {
        HOSTNAME : 'localhost:8008',
		
		isLocalHost : function(hostname) {
            return hostname == HOST ;
		},
        
        getSupportedProp: function(proparray){
            var root=document.documentElement //reference root element of document
            for (var i=0; i<proparray.length; i++){ //loop through possible properties
                if (typeof root.style[proparray[i]]=="string"){ //if the property value is a string (versus undefined)
                    return proparray[i] //return that string
                }
            }
        },
        
        close : function(){
            var el = document.getElementById('Focalify-container');
            el.parentNode.removeChild(el);
        },
        
        resizeDiv:function(){
            var el = document.getElementById('Focalify-container');
            el.getElementsByTagName('img')[1].style.display = 'none';
            el.getElementsByTagName('iframe')[0].style.visibility = 'visible';
        },
        
        init : function(link,title){
            link = link || window.location;
            if(document.getElementById('Focalify-container')){
                var el = document.getElementById('Focalify-container');
                el.parentNode.removeChild(el);
            }
            var div = document.createElement('div');
            div.id = 'Focalify-container';
            div.style.width = '400px';
            div.style.height = '400px';
            div.style.position = 'fixed';
            div.style.top = '24px';
            div.style.right = '24px';
            div.style.backgroundColor = '#fff';
            div.style.textAlign = 'center';
            div.style.zIndex = '1000';
            div.style.border = "2px solid #8DEDFC";
            
            var boxradiusprop=this.getSupportedProp(['borderRadius', 'MozBorderRadius', 'WebkitBorderRadius']);
            div.style[boxradiusprop] = '5px';

            var boxshadowprop=this.getSupportedProp(['boxShadow', 'MozBoxShadow', 'WebkitBoxShadow']);
            div.style[boxshadowprop] = "0px 0px 5px #8c8c8c";
            
            document.body.appendChild(div);
            var src = 'http://'+this.HOSTNAME+'/link_status/?link='+escape(link);
            div.innerHTML = '<a href="javascript:void(0);" onclick="this.parentNode.parentNode.removeChild(this.parentNode);"><img style="padding:0px;border:0px;position:absolute;right:-14px;top:-14px;" src="http://'+this.HOSTNAME+'/site_media/img/close.png" alt=""/></a>'+
                '<img id="Focalify_loader" src="http://'+this.HOSTNAME+'/site_media/img/22.gif" style="margin:20px auto" />'+
                '<iframe frameborder=0 style="border:0px;visibility:hidden;" onload="this.contentWindow.focus();document.getElementById(\'Focalify_loader\').style.display=\'none\';this.style.visibility=\'visible\'" src="'+src+'" width="100%" height="100%"></iframe>';
            
        }

	};

}();



(function () {
    var HOST = "localhost";

    function isLocalHost(hostname) {
        if(hostname == HOST ) {
            return true;
        }
    }

    var port = null;
    try {
        port = chrome.extension.connect();
    } catch(e) {
        //console.log("Oops..might be incognito mode:" + e);
    }
    if(!port) {
        //console.log("Unable to connect to the extension port");
    } else if(window.location && isLocalHost(window.location.hostname)) {
      //check for cookies if present
    	if(document.cookie) {
           
    		var cookie = getCookie("sessionid");
    		//console.log(cookie);
        port.postMessage({msg: "updateCookie", cookie: cookie});
    	} else {
        //console.log("No document.cookie");
        port.postMessage({msg: "updateCookie", cookie: null});
      }
    }

    chrome.extension.onRequest.addListener(
      function(request, sender, sendResponse) {
        //if(!sender.tab) { //From XT
            if (request.id == "pageDetails") {
                sendResponse(composeBookmarkObject());
            }
            if (request.id == "popup"){
                Focalify.init(request.url);
            }
        //}
      }
    );
})();


function composeBookmarkObject() {
    try {
		var url = window.location.href;
		var title = document.title;
		if(!url) {
			return null;
		}
		var notes = "";
		var selection = window.getSelection();
		if(selection && selection.toString().length) {
			notes = selection.toString();
		}
        return {
		    url: url,
		    title: title,
		    notes: notes,
		    description: getMetaValueByName('description'),
		    thumb: getMetaValueByProperty('og:image'),
		    images: getImages()
	    };
	} catch(e) {
	    alert(e)
		//console.log("contentScripts::compoesBookmarkObject::Error"+e);
	}	
}

function getCookie(cookieName) {
	try {
    var dCookie = document.cookie;
    var cookieLen = dCookie.length;
		if (cookieLen) {
				var beg = dCookie.indexOf(cookieName + "=");
				if (beg != -1) {
						var delim = dCookie.indexOf(";", beg);
                        beg = dCookie.indexOf("=", beg) + 1;
						if (delim == -1) delim = cookieLen;
						return dCookie.substring(beg, delim);
				}
		}
	} catch(e) {
		//console.log("contentScripts::getCookie::Error"+e);
	}	
	return "";
}


function getMetaValueByName(meta_name) {
	var my_arr=document.getElementsByTagName("META");
	for (var counter=0; counter<my_arr.length; counter++) {
		if (my_arr[counter].name.toLowerCase() == meta_name.toLowerCase()) {
			return my_arr[counter].content;
		}
	}
	return '';
}

function getMetaValueByProperty(property_name){
	var my_arr=document.getElementsByTagName("META");
	for (var counter=0; counter<my_arr.length; counter++) {
		var property = my_arr[counter].getAttribute('property');
		if(property && property.toLowerCase() == property_name){
			return my_arr[counter].getAttribute('content');
		}
	}
	return '';
}

function getImages(){
	var imgs = document.getElementsByTagName('img');
	var ret_images = [];
	for(var i=0;i<imgs.length;i++){
	    //console.log(imgs[i].width)
		if(imgs[i].width > 200 && ret_images.length < 3)
			ret_images[ret_images.length] = imgs[i].src;
	}
	return ret_images;
}

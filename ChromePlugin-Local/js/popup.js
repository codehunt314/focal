var DOMAIN = 'http://localhost:8008';


var getUrl = function(object) {
	if (!(object && object.url)) return null;
	return _.isFunction(object.url) ? object.url() : object.url;
};

// Throw an error when a URL is needed, and none is supplied.
var urlError = function() {
	throw new Error('A "url" property or function must be specified');
};

Backbone.sync = function(method, model, options) {
	var obj = model.toJSON();
	obj.action = method;
	var params = _.extend({
			type: method=="read"?"GET":'POST',
			data: obj,
			dataType: 'json'
	}, options);
	if (!params.url) {
		params.url = getUrl(model) || urlError();
		if(params.url.indexOf("?")>=0){
			params.url = params.url + '&method='+ method;
		}else{
			params.url = params.url + '?method='+ method;
		}
	}
	//  params.beforeSend = beforeSend;
	//  params.complete = onComplete;
	
	
    $('#status').show().removeClass('success').stop().fadeIn().text('connecting..');
	return $.ajax(params);
};

// var beforeSend = function(jqXHR, settings){
//     $('#status').removeClass('success').fadeIn().text('working..');
// }
// var onComplete = function(jqXHR, textStatus){
//     if(textStatus === 'success'){
//         $('#status').text('success').addClass('success').delay(2000).fadeOut();
//     }else{
//         $('#status').removeClass('success').fadeIn().text('Error occurred : '+textStatus);
//     }
// }


$(function(){
var UrlInfo = Backbone.Model.extend({
	initialize: function() {
		chrome.windows.getCurrent(_.bind(function(obj) {
		    chrome.tabs.getSelected(obj.id, _.bind(function(tab) {
		        try {
		        	this.set({
		        		tabId : tab.id,
		        		title : tab.title,
		        		link : tab.url,
		        		favicon : tab.favIconUrl
		        	});
		        	CURRENT_URL = tab.url;
		            
		        } catch (e) { };
		        
		        if( tab.url.indexOf('http://') == 0 ||  tab.url.indexOf('https://') == 0 ){
		            var ifCalled = false;

    		        setTimeout(function(){
    		            if(!ifCalled){
    		                //console.log('not called');
    		                window.App.trigger('missingScriptHandler');
    		            }
    		        }, 4000);
    		        chrome.tabs.sendRequest(tab.id, {id: "pageDetails"},
    		          _.bind(function(response) {
    		            this.set({
    		          		images : response.images,
    		          		thumb : response.thumb,
    		          		description : response.description,
    		          		data : true
    		          	});
    		          	window.App.trigger('request');
    		          	ifCalled = true;
    		        }, this));
		        }else{
		            window.App.trigger('wrongURLHandler');
		        }
		    }, this));
		}, this));
	}
});

function setError(message){
    message = message || "Error connecting server. Try again later.";
    $('#status').removeClass('success').stop().fadeIn().text(message);
}

var PersonalCollection = Backbone.Model.extend({
	url: DOMAIN + '/addtofav/?sessionkey='+ localStorage.getItem('sessionid'),
	toggle: function() {
	    this.save({favorite: !this.get("favorite")},{error:this.onError,success:this.onSuccess});
	},
	onError:function(model, response){
	    model.set({favorite: !model.get("favorite")});
	    setError();
	},
	onSuccess:function(model, response){
	    $('#status').text('Saved').stop().addClass('success').delay(1500).fadeOut();
	}
});

var LinkStatus = Backbone.Model.extend({
//	url: DOMAIN + "/getlinkdetail/"
	url: function() {
		var u = DOMAIN + '/getlinkdetail/?sessionkey=' + localStorage.getItem('sessionid');
		return u;
	},
	onError:function(model, response){
	    setError();
	},
	onSuccess:function(model, response){
	    $('#status').hide();
	}
});


var CURRENT_URL;
var Tag = Backbone.Model.extend({
    url: DOMAIN + "/tag/",
    onError:function(model, response){
	    setError("Error Modifying Tags.");
	},
	onSuccess:function(model, response){
	    $('#status').text('Saved').stop().addClass('success').delay(1500).fadeOut();
	}
});
var TagSet = Backbone.Collection.extend({
	model: Tag,
	url: DOMAIN + "/tag/",
	setTags: function(tags){
		var object = this;
		var obj = [];
		_.map(tags, function(tag){
				var tags = object.pluck('tag');
				if(tags.indexOf(tag) == -1)
					object.add({tag:tag, url:CURRENT_URL},{silent: true});
				
			});
	},
	comparator: function(tag) {
		return tag.get('tag');
	},
	onError:function(model, response){
	    setError("Error Modifying Tags.");
	},
	onSuccess:function(model, response){
	    $('#status').text('Saved').stop().addClass('success').delay(1500).fadeOut();
	    $("#view_tags").html($("#view_tags").html()+', '+model.get('tag'));
	}
});



var Context = Backbone.Model.extend({
	url:DOMAIN + '/sharealink/',
	toggle:function(){
		if(!this.get('added')){
			this.save({added: !this.get("added"), 'by':'Self', 'can_delete':true },{error:this.onError, success:this.onSuccess});
		}else{
			this.save({added: !this.get("added")},{error:this.onError, success:this.onSuccess});
		}
		//e.stopPropagation();
		//e.preventDefault();
		$("#input_context").blur();
	},
	onError:function(model, response){
	    model.set({added: !model.get("added")});
	    setError("Error Sharing in Context. Try Again.")
	},
	onSuccess:function(model, response){
	    $('#status').text('Saved').stop().addClass('success').delay(1500).fadeOut();
	}
});

var ContextList = Backbone.Collection.extend({
	model : Context,
	selected: function(){
		return this.filter(function(context){ return context.get('added'); });
	},
	remaining: function() {
		return this.without.apply(this, this.selected());
	},
	// comparator: function(context) {
	// 	return context.get('order');
	// },
	search: function(query){
		var contexts = this.remaining();
		query = query.toLowerCase();
		return contexts.filter(function(context){ return (context.get('name').toLowerCase().indexOf(query) != -1) });
	}
});

var ContextView = Backbone.View.extend({
	tagName:  "li",
	events:{
		"click"		: 	"toggleAdded",
		"mousedown"	:	"mouseDown",
		"add"		:	"addContext"
	},
	template: _.template($('#context-template').html()),
	render: function(index) {
		$(this.el).html(this.template(this.model.toJSON()));
		$(this.el).addClass(this.model.get('can_delete')?'active':'inactive');
		$(this.el).addClass(index%2==0?'odd':'');
		return this;
	},
	toggleAdded: function(){
		if(this.model.get('can_delete')){
			this.model.toggle();
			$('#input_context').val('');
		}
	},
	addContext: function() {
		this.model.toggle();
		$('#input_context').val('');
	},
	mouseDown:function(e){
		e.preventDefault();
	}
});

window.AppView = Backbone.View.extend({
	el: $("#container"),
	
	events: {
		"click #a_favorite"		: 	"changeFavorite",
		"click #a_edit_tags"	: 	"showEditTag",
		//"dblclick #view_tags"	: 	"showEditTag",
		"keyup #input_context"	: 	"changeContextSearch",
		"focus #input_context"	: 	"changeContextSearch",
		"keydown #input_context": 	"handleContextKeys",
		"blur #input_context"	: 	"handleBlurContext"
	},
	initialize: function() {
		this.linkData = new UrlInfo;
		this.bind('request', this.requestURLStat, this);
		this.bind('wrongURLHandler', this.errorHandler, {'message':'Wrong URL'});
		this.bind('missingScriptHandler', this.errorHandler, {'message':'Content Script not found'});
	},
	requestURLStat: function(){
		this.linkStatus = new LinkStatus({'link':this.linkData.get('link')});
		this.linkStatus.bind('change',this.initUI, this);
		this.linkStatus.fetch({error:this.linkStatus.onError, success:this.linkStatus.onSuccess});
		$('#head h1').html(this.linkData.get('title'));
	},
	initUI: function(){
		/* Tags */
		if(this.linkStatus.get('message') == 'Invalid Session Key'){
			this.el.hide();
			$("#login").show();
			return;
		}else{
		    this.el.css('background','white');
		    $('#inner-container',this.el).removeClass('hide');
		    this.fetchTags();
		}
		this.tagSet = new TagSet;
		this.tagSet.setTags(this.linkStatus.get('tags'));
		
		/* Initialize Personal Collection Flag and its handler */
		this.personalCollection = new PersonalCollection(this.linkData.toJSON());
		this.personalCollection.bind('change', this.showFavorite, this);
		this.personalCollection.set({favorite:this.linkStatus.get('favorite')});
		
		/* Contexts */
		this.contextList = new ContextList;
		this.contextList.bind('change', this.showDefaultContexts, this);
		this.contextList.reset(this.linkStatus.get('activities'), {silent: true});
		this.contextList.each(function(item){
			item.set(this.linkData.attributes,{silent:true});
		}, this);
		this.showDefaultContexts();
	},
	errorHandler:function(){
	    if(this.message === 'Wrong URL'){
	        $('#container').css('background','#fff');
	        setError("Cannot bookmark this URL. Not a valid URL");
	    }
	    if(this.message === 'Content Script not found'){
	        $('#container').css('background','#fff');
	        setError("Please refresh this page and try again to save it.");
	    }
	},
	showFavorite:function() {
		var className = this.personalCollection.get('favorite') ? 'active' : 'inactive';
		$('#a_favorite').removeClass('active inactive').addClass(className);
		if(className == 'active'){
			this.showTagInput();
		}else{
			this.hideTagInput();
		}
	},
	changeFavorite:function(){
		this.personalCollection.toggle();
	},
	fetchTags:function(){
	    var obj = this;
	    $.getJSON(DOMAIN + '/get_tags/', function(data){
          	    var tags = _.map(obj.linkStatus.get('tags'), function(item){ return {value: item, name: item};});
        		$("#input_tag").removeClass('loading');
        		$("#input_tag").removeAttr("readonly");
        		var input_tags = _.map(data, function(tag){return{'value':tag}});
        		$("#input_tag").autoSuggest(input_tags, {
        			//selectedItemProp: "name", 
        			//searchObjProps: "name", 
        			preFill:tags,
        			neverSubmit:true,
        			selectionAdded:_.bind(obj.tagAdded, obj),
        			selectionRemoved:_.bind(obj.tagRemoved, obj)
        		});
          	  });
	},
	hideTagInput:function() {
		$("#tags").hide();
	},
	showTagInput:function() {
		//var data = this.tags;
		if(this.tagSet.length > 0){
		    $("#edit_tags").hide();
		    $("#view_tags").show().html( this.linkStatus.get('tags').join(', ') );
		}else{
		    $("#view_tags").hide();
		    $("#edit_tags").show();
		}
		$("#tags").show();
	},
	showEditTag: function(){
	    $("#view_tags").toggle();
	    $("#edit_tags").toggle();
	},
	tagAdded:function(elem){
		var tags = this.tagSet.pluck('tag');
		var tag = elem.text().substr(1);
		if(tags.indexOf(tag) == -1)
			this.tagSet.create({tag:tag,url:CURRENT_URL},{error:this.tagSet.onError, success:this.tagSet.onSuccess});
	},
	tagRemoved:function(elem){
		var tags = this.tagSet.pluck('tag');
		var tag = elem.text().substr(1);
		var index = _.indexOf(tags, tag);
		if(index!=-1){
			var model = this.tagSet.at(index);
			model.id = 1;
			this.tagSet.remove(model, {silent:false});
			model.destroy({error:model.onError, success:model.onSuccess});
		}
		elem.fadeTo("fast", 0, function(){ elem.remove(); });
		this.updateViewTags();
	},
	updateViewTags: function(){
		$("#view_tags").html(this.tagSet.map(function(e){return e.get('tag')}).join(', '));
	},
	addOneContext: function(context, index) {
		var view = new ContextView({model: context});
		$("#context-list").append(view.render(index).el);
	},
	showDefaultContexts: function(){
		if(this.contextList.size() > 0){
			$("#context-list").empty();
			var contexts = this.contextList.selected();
			var index = 0;
			_.each(contexts, function(context){this.addOneContext(context,index++);}, this);
			var remaining_contexts = this.contextList.remaining();
			_.each(remaining_contexts, function(context){this.addOneContext(context,index++);}, this);
			$("#contexts").show();	$("#no-context").hide();
		}else{
			$("#contexts").hide();	$("#no-context").show();
		}
	},
	selectContext : function(direction){
	
		var newFocusedItem = $('li.focus', $("#context-list")).eq(0)[direction]();
        if(newFocusedItem.length>0){
        	$('li.focus', $("#context-list")).removeClass('focus');
            newFocusedItem.addClass('focus');
            
            var focusedItemCoordinates = newFocusedItem.getCoordinates($("#context-list")),
                scrollTop = $("#context-list").scrollTop();
            if (direction == 'next'){
                var delta = focusedItemCoordinates.bottom - $("#context-list").outerHeight();
                if ((delta - scrollTop) > 0){
                    $("#context-list").scrollTop(delta);
                }
            } else {
                var top = focusedItemCoordinates.top;
                if (scrollTop && scrollTop > top){
                    $("#context-list").scrollTop(top);
                }
            }
        }
	},
	handleContextKeys: function(e){
		this.previousValue = $("#input_context").val();
		switch (e.keyCode) {
			case 38: // up
				this.selectContext('prev');
				e.preventDefault();e.stopPropagation();
				break;
			case 40: // down
				this.selectContext('next');
				e.stopPropagation();e.preventDefault();
				break;
			case 13:
				// Enter
				$('#context-list li.focus').eq(0).trigger('add');
				break;
			case 27:
				//Esc
				e.stopPropagation();
				e.preventDefault();
				$("#input_context").blur();
				break;
			default:
				break;
		}
	},
	handleBlurContext: function(){
		$('#no-context-filtered').hide();
		this.showDefaultContexts();
	},
	changeContextSearch: function(e){
		var query = $("#input_context").val();
//		console.log('came i' + e);
		if( query == this.previousValue && e.type != 'focusin')
			return;
		$("#context-list").empty();
		
		//console.log(query);
		var contexts = this.contextList.search(query);
		if(contexts.length>0){
			_.each(contexts, function(context, index){
				this.addOneContext(context, index);
			}, this);
			$("#context-list li").eq(0).addClass('focus');
			$('#context-list').scrollTop(0);
			$('#no-context-filtered').hide();
		}else{
			$('#no-context-filtered').show();
		}
	}
});
window.App = new AppView;

});

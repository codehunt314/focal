/*
* AutoSuggest
* Copyright 2011-2012 
* Spaces 23 Software Labs
* www.23spaces.com
*
* Author : Nitin Hayaran
* Version 0.1.0   -   Updated: Dec. 06, 2011
*
* This Plug-In will do some sexy work
*/

(function($) {

  if (!window.FacetSearch) window.FacetSearch = {};
  
  FacetSearch.VERSION = '0.1.0';

  FacetSearch.Search = function(options) {
    var defaults = {
      container   : '',
      query       : '',
      facet		  : '',
      softLimit	  : 10,
      callbacks   : {
        search          : $.noop,
        focus           : $.noop,
        removeFocus		: $.noop
      }
    };
    this.options           = $.extend(defaults, options);
    this.options.callbacks = $.extend(defaults.callbacks, options.callbacks);

    this.searchBox     = new FacetSearch.SearchBox(this);
    // Disable page caching for browsers that incorrectly cache the visual search inputs.
    // This is forced the browser to re-render the page when it is retrieved in its history.
    // $(window).bind('unload', function(e) {});

    // Gives the user back a reference to the `searchBox` so they
    // can use public methods.
    return this;
  };

  // Entry-point used to tie all parts of VisualSearch together. It will either attach
  // itself to `options.container`, or pass back the `searchBox` so it can be rendered
  // at will.
  FacetSearch.init = function(options) {
    return new FacetSearch.Search(options);
  };

})(jQuery);




FacetSearch.SearchBox = function (app){
	var inputBox = $(app.options.container);
	inputBox.css('display','none');
	inputBox.parent().css('position','relative');
	inputBox.parent().append('<div class="FS-search">\n  <div class="FS-search-box-wrapper FS-search-box">\n    <div class="FS-icon FS-icon-search"></div>\n    <div class="FS-search-inner"></div>\n  </div>\n<ul class="ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all FS-interface"></ul></div><div class="FS-help"></div>');
	app.searchBox = this.searchBox = $('.FS-search', inputBox.parent());
	app.autoSuggest = $('.ui-autocomplete', inputBox.parent()).width($('.ui-autocomplete', inputBox.parent()).width()-2);
	app.isFocused = false;
	var self = this;
	$('input', this.searchBox).autoGrowInput();
	$(document).click(function(e) {
		self.removeFocus();
	});
	this.searchBox.click(function(e) {
		clearTimeout(app.mayLostFocus);
		e.stopPropagation();e.preventDefault();
	});
	$('.FS-search-box', this.searchBox).bind({
		'keydown':  function(e) {self.keyDown(e);},
		'click':	function(e)	{self.focusSearch(e);},
		'dblclick':	function(e) {self.highlightSearch(e);}
	});
	app.autoSuggest.on('click', 'li', function(){
		$('li.active',$(this).parent('ul')).removeClass('active');
		$(this).addClass('active');
		inputElement.selectFacet();
	});
	var inputElement;
	this.highlightSearch = function (e) {};
	this.initQuery = function(sTerm, filter, shouldFocus){
		this.clearSearch();
		shouldFocus = typeof shouldFocus == 'undefined'?true:false;
		var flag = false; 
		if(sTerm.trim().length > 0){
			inputElement.input.val(sTerm.trim()).trigger('update');;
			flag = true;
		}
		if(filter.trim().length > 0){
			flag = true;
			var mayBeFacets = filter.trim().split(",");
			var facets = [];
			for(var i=0;i<mayBeFacets.length; i++){
				if( i>0 && mayBeFacets[i-1][mayBeFacets[i-1].length-1]=='\\'){
					facets[facets.length-1] = facets[facets.length-1].slice(0, facets[facets.length-1].length-1) + ',' +mayBeFacets[i];
				}else {
					facets[facets.length] = mayBeFacets[i];
				}
			}
			for(var i=0;i<facets.length; i++){
				var term = facets[i].slice(0,facets[i].indexOf(":"));
				var value = facets[i].slice(facets[i].indexOf(":")+1);
				self.addFacet(term,value.replace(/\\:/g,':'),inputElement);
			}
		}
		if(flag && shouldFocus)
			self.focusSearch();
	};
	this.clearSearch = function(){
		$('.FS-Facet', this.searchBox).remove();
		$(inputElement.input).val('');
	},
	this.getFacetList = function(e) {
		return $('.FS-Facet', self.searchBox).map(function(){return [$('em',this).text().slice(0,$('em',this).text().length-3), $('span',this).text()];});
	};
	this.keyDown = function (e) {
		switch (e.which) {
			case 8:  	// Mac delete == PC backspace
				if($('.selected-facet', self.searchBox).length>0){
					$('.selected-facet', self.searchBox).addClass('to-be-deleted');
					self.selectFacetAfterDeletion('prev');
					self.deleteSelectedFacets();
					e.preventDefault();e.stopPropagation();
				}
//				if($('input',self.searchBox).hasAttr('readonly'))
//					e.preventDefault();e.stopPropagation();
				break;
//			case 46:  	// PC delete == Mac fn+delete
//				if($('.selected-facet', this.searchBox).length>0){
//					self.selectFacetAfterDeletion('next');
//					self.deleteSelectedFacets();
//				}
//				if($('input',self.searchBox).hasAttr('readonly'))
//					e.preventDefault();e.stopPropagation();
//				break;
			default:
				
		}
		
	};
	this.makeSearch = function(){
		var facets = self.getFacetList();
		var searchString = '';
		if(inputElement.input.val().trim().length>0 || facets.length > 0){
			if(facets.length>0){
				for(var i=0;i<facets.length;i+=2){
					searchString += facets[i] +':'+ facets[i+1].replace(/:/g,'\\:').replace(/,/g,'\\,').replace(/\//g, "&#47;" );
					if(i<facets.length-2)
						searchString += ','
				}
			}
			app.options.callbacks.search(inputElement.input.val().trim().replace(/\//g, "&#47;" ), searchString);
		}
		
	};
	this.removeFocus = function(e){
		if(inputElement){
			inputElement.removeFocus();
		}
	};
	this.focusSearch = function(e){
		$('input',self.searchBox).last().removeAttr('readonly')
									    .setCaretPosition($('input',self.searchBox).last().val().length);
		//app.options.callbacks.focus();
		//$('input',self.searchBox).last().focus();
	};
	this.addFacet = function(facet, term, inputBox){
		var html = '<em>'+facet+' : </em><span>'+term+'</span>';
		var div = $('<div/>',{
					'class': 'FS-Facet search_facet',
					'html' : html
				  }).insertBefore(inputBox.input)
				  	.click(function (e) {self.clickFacet(e);});
		//inputBoxes[inputBoxes.length] = new FacetSearch.InputBox(div, app, this);
	};
	this.clickFacet = function (e) {
		e.stopPropagation();e.preventDefault();
		if (!e.shiftKey) {
	        self.unSelectAllFacets();
	    }
	    $(e.currentTarget).addClass('selected-facet');
	    inputElement.input.attr('readonly','readonly').focus();
	    inputElement.removeFocus();
	};
	this.deleteSelectedFacets = function () {
		$('.to-be-deleted', this.searchBox).remove();
	};
	this.selectFacetAfterDeletion = function(direction){
		if($('.FS-Facet:not(.to-be-deleted)', self.searchBox).length == 0){
			//console.log('--'+$('input', self.searchBox).val().length);
			$('input', self.searchBox).removeAttr('readonly').setCaretPosition($('input', self.searchBox).val().length).trigger('focus');
		}else if ($('.FS-Facet', self.searchBox).first().hasClass('to-be-deleted')){
			self.selectFacet('next');
		}else {
			self.selectFacet(direction);
		}
	};
	this.selectFacet = function (direction) {
		var next;
		//console.log($('.selected-facet', self.searchBox).length);
		if($('.selected-facet', self.searchBox).length > 0){
			if	(direction == 'prev'){
				next = $('.selected-facet', self.searchBox).prev('.FS-Facet');
			}
			else{
				next = $('.selected-facet', self.searchBox).next('.FS-Facet');
			}
		}
		else {
			if (direction == 'prev'){
				next = $('.FS-Facet', self.searchBox).last();
			}
		}
		if( $(next).eq(0).length == 1){
			$('.selected-facet', self.searchBox).removeClass('selected-facet');
			next.addClass('selected-facet');
		}else if ($('.FS-Facet', self.searchBox).last().hasClass('selected-facet') && direction == 'next') {
			$('.selected-facet', this.searchBox).removeClass('selected-facet');
			//$('input', app.searchBox).delay(2000).setCaretPosition(0);
			
			// hack to set caret position at 0
			//setTimeout(function(){
				//$('input', app.searchBox.searchBox);
				$('input', app.searchBox.searchBox).removeAttr('readonly').setCaretPosition(0).focus();
			//}, 10);
		}
	};
	this.unSelectAllFacets = function(){
		$('.selected-facet', self.searchBox).removeClass('selected-facet');
	}
	inputElement = new FacetSearch.InputBox($('.FS-search-inner',this.searchBox), app, this);
	if( typeof searchQuery != 'undefined' || typeof searchFacet != 'undefined' ){
		this.initQuery(searchQuery, searchFacet);
	}else{
		this.initQuery(app.options.query,app.options.facet); 
	}
	
	return this;
};


FacetSearch.InputBox = function(obj, app, searchBox){
	this.input = $('<input/>',{'type':'text'});
	if(obj.hasClass('FS-Facet')){
		this.input.insertBefore(obj);
		this.input.addClass('inner-input');
	}
	else{
		this.input.appendTo(obj);
		this.input.autoGrowInput();
	}
	
	var self = this;
	
	this.input.bind({ 
		'focus'		: 	function (e) {self.addFocus(e);},
		'blur'		: 	function (e) {self.lostFocus(e);},
		'click'		:	function (e) {self.input.removeAttr('readonly');e.stopPropagation();},
		'dblclick'	:	function (e) {self.highlightSearch(e);},
		'keydown'	:	function (e) {self.inputKeyDown(e);},
		'keyup'		:	function (e) {self.inputKeyUp(e);}
	});
	this.addFocus = function (e) {
		
		//self.input.trigger('keyup');
		if(self.input.attr('readonly') != 'readonly'){
			self.prev = '';
			searchBox.unSelectAllFacets();
		}
		if(!app.isFocused){
			app.isFocused = true;
			app.options.callbacks.focus();
		}
//		console.log(self.input);
	};
	this.lostFocus = function (e) {
		app.mayLostFocus = setTimeout(function () {
			app.isFocused = false;
			self.removeFocus();
			if(!(searchBox.getFacetList().length > 0 || self.input.val().trim().length > 0))
				app.options.callbacks.removeFocus();
		}, 100);
	};
	this.removeFocus = function (e) {
		self.hideAutoSuggest();
	};
	this.highlightSearch = function (e) {};
	this.inputKeyDown = function (e) {
		self.prev = this.getCurrentInputText();//.input.val();
		switch(e.which) {
			case 37: 	// Left Arrow and 
				if(self.input.getCaretPosition()==0 && $('.FS-Facet', self.searchBox).length > 0){
					searchBox.selectFacet('prev');
					self.removeFocus();
					self.input.attr('readonly','readonly');//.trigger('blur');
				}
				break;
			case 8: //  Backspace / delelte(mac)
/*				console.log($('.selected-facet', self.searchBox).length);
				if( 
					(self.input.getCaretPosition()==0) && 
					($('.FS-Facet', self.searchBox).length > 0) && 
					($('.selected-facet', self.searchBox).length == 0) 
				)
*/				if(($('.selected-facet', self.searchBox).length == 0) && ($('.FS-Facet', self.searchBox).length > 0) && (self.input.getCaretPosition()==0) && (self.input.getSelectedText().length == 0)){
					searchBox.selectFacet('prev');
					self.removeFocus();
					self.input.attr('readonly','readonly');//.trigger('blur');
					e.preventDefault();e.stopPropagation();					
				}
				break;
			case 39: 	// Right Arrow
				if(this.input.attr('readonly') == 'readonly'){
					searchBox.selectFacet('next');
					e.preventDefault();
				}
				break;
			case 38: 	// up
				e.preventDefault();
				self.moveSelection("up");
				break;
			case 40: 	// down
				e.preventDefault();
				self.moveSelection("down");
				break;
			case 9:  	// tab
				break;
			case 13: 	// return
				self.selectFacet();
				break;
			default:
				break;
		}
	};
	this.removeCurrentText = function(){
		if(!self.input.hasAttr('readonly')){
			if(this.input.val().indexOf("@")!=-1){
				var string = self.input.val();
				var cursorPosition = self.input.getCaretPosition()-1;
				var lastSep = string.indexOf(' ', cursorPosition) == -1 ? string.length : string.indexOf(' ', cursorPosition);
				var stringList = string.slice(0, lastSep).split('@');
				string = string.slice(0, (lastSep-stringList[stringList.length-1].length)-1) + string.slice(lastSep, string.length);
				self.input.val(string)
						  .setCaretPosition(string.slice(0, (lastSep-stringList[stringList.length-1].length)).length);
			}
			else{
				self.input.val('').focus();
			}
		}
	};
	this.getCurrentInputText = function(){
		if(!self.input.hasAttr('readonly')){
			if(this.input.val().indexOf("@")!=-1){
				var string = self.input.val();
				var cursorPosition = self.input.getCaretPosition()-1;
				var lastSep = string.indexOf(' ', cursorPosition) == -1 ? string.length : string.indexOf(' ', cursorPosition);
				var stringList = string.slice(0, lastSep).split('@');
				var myStr = stringList[stringList.length-1];
				return myStr;
			}else {
				return self.input.val().replace(/[\\]+|[\/]+/g,"");
			}
		}else{
			return self.prev;
		}
	};
	this.inputKeyUp = function (e) {
		var string = self.getCurrentInputText();
		//console.log(self.prev + ' --- ' + string);
		if(self.prev === string)
			return;
		// show autocomplete
		self.prev = string;
		if (string.length >= 1) {
			obj.parent('.FS-search-box').addClass("loading");
			var refreshAutocomplete = true;
			app.options.callbacks.valueMatches(string, function(matches) {
				matches = matches || {};
				var mayBeShown = {};
				var matcher = new RegExp('\\b' + string, 'i');
				//var matcher = new RegExp('' + string, 'i');
				var totalMatches = 0; 
				var selectedFacets = searchBox.getFacetList();
				for(facet in matches){
					mayBeShown[facet] = $.grep(matches[facet], function(item, i){
						if(matcher.test(item)){
							for(var i=0;i<selectedFacets.length;i+=2){
								if(selectedFacets[i].toLowerCase() === facet.toLowerCase() && selectedFacets[i+1] === item)
									return false;
							}
							return true;
						}
						return false;
					});
					//console.log(mayBeShown);
					totalMatches += mayBeShown[facet].length;
				}
				var toBeShown = {};
				for(facet in mayBeShown){
					var show = Math.ceil((app.options.softLimit * mayBeShown[facet].length)/totalMatches);
					toBeShown[facet] = $(mayBeShown[facet]).slice(0, show);
				}
				if(totalMatches>0)
					self.showAutoSuggest(toBeShown);
				else 
					self.hideAutoSuggest();
			});
			
			
			obj.parent('.FS-search-box').removeClass("loading");
		}else {
			self.hideAutoSuggest();
		}
	};
	this.showAutoSuggest = function(matches){
		app.autoSuggest.empty();
		for(facet in matches){
			for(var i=0;i<matches[facet].length;i++){
				if(i==0)
					app.autoSuggest.append('<li class="sep"><label>'+facet+'</label>'+matches[facet][i]+'</li>');
				else
					app.autoSuggest.append('<li><label><span>'+facet+'</span></label>'+matches[facet][i]+'</li>');
			}
		}
		app.autoSuggest.css('display','block');
	};
	this.hideAutoSuggest = function () {
		app.autoSuggest.css('display','none');
	};
	this.moveSelection = function(direction){
	    if(app.autoSuggest.css('display') == 'none')
	    	return;
		if (direction=='up') {
			var next = $('li.active', app.autoSuggest).length > 0 ? $('li.active', app.autoSuggest).prev() : $('li',app.autoSuggest).last();
		}else {
			var next = $('li.active', app.autoSuggest).length > 0 ? $('li.active', app.autoSuggest).next() : $('li',app.autoSuggest).first();
		}
		$('li.active', app.autoSuggest).removeClass('active');
		next.addClass('active');
	};
	this.selectFacet = function () {
		if($('li.active', app.autoSuggest).length > 0){
			var facet = $('label', $('li.active', app.autoSuggest).first()).text();
			var term  = $('li.active', app.autoSuggest).first().text().replace(facet, '');
			$('li.active', app.autoSuggest).removeClass('active');
			this.hideAutoSuggest();
			searchBox.addFacet(facet, term, this);
			//this.input.val('').focus();
			self.removeCurrentText();
		}else{
			this.removeFocus();
			searchBox.makeSearch();
		}
	};
	return this;
};

(function($){
    $.fn.autoGrowInput = function(o) {
        o = $.extend({
            maxWidth: 1000,
            minWidth: 3,
            comfortZone: 15
        }, o);
        this.filter('input:text').each(function(){
            var minWidth = o.minWidth || $(this).width(),
                val = '',
                input = $(this),
                testSubject = $('<tester/>').css({
                    position: 'absolute',
                    top: -9999,
                    left: -9999,
                    width: 'auto',
                    fontSize: input.css('fontSize'),
                    fontFamily: input.css('fontFamily'),
                    fontWeight: input.css('fontWeight'),
                    letterSpacing: input.css('letterSpacing'),
                    whiteSpace: 'nowrap'
                }),
                check = function() {
                    if (val === (val = input.val())) {return;}
                    // Enter new content into testSubject
                    var escaped = val.replace(/&/g, '&amp;').replace(/\s/g,'&nbsp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
                    testSubject.html(escaped);
                    // Calculate new width + whether to change
                    var testerWidth = testSubject.width(),
                        newWidth = (testerWidth + o.comfortZone) >= minWidth ? testerWidth + o.comfortZone : minWidth,
                        currentWidth = input.width(),
                        isValidWidthChange = (newWidth < currentWidth && newWidth >= minWidth)
                                             || (newWidth > minWidth && newWidth < o.maxWidth);
                    // Animate width
                    if (isValidWidthChange) {
                        input.width(newWidth);
                    }
                };
            testSubject.insertAfter(input);
            $(this).bind('keyup keydown blur update', check);
        });
        return this;
    };
})(jQuery);


(function($){
  $.fn.insertAtCaret = function(text, opts) {
    var element = $(this).get(0);

    if (document.selection) {
      element.focus();
      var orig = element.value.replace(/\r\n/g, "\n");
      var range = document.selection.createRange();

      if (range.parentElement() != element) {
        return false;
      }

      range.text = text;

      var actual = tmp = element.value.replace(/\r\n/g, "\n");

      for (var diff = 0; diff < orig.length; diff++) {
        if (orig.charAt(diff) != actual.charAt(diff)) break;
      }

      for (var index = 0, start = 0; tmp.match(text) && (tmp = tmp.replace(text, "")) && index <= diff; index = start + text.length ) {
        start = actual.indexOf(text, index);
      }
    } else if (element.selectionStart) {
      var start = element.selectionStart;
      var end   = element.selectionEnd;

      element.value = element.value.substr(0, start) + text + element.value.substr(end, element.value.length);
    }
    
    if (start) {
      setCaretTo(element, start + text.length);
    } else {
      element.value = text + element.value;
    }
    
    return this;
  }
  
  $.fn.setCaretPosition = function(start, end) {
    var element = $(this).get(0);
    element.focus();
    setCaretTo(element, start, end);
    return this;
  }
  
  
  $.fn.getCaretPosition = function() {
    var element = $(this).get(0);
    //$(element).focus();
    return getCaretPosition(element);
  }

  $.fn.getSelectedText = function() {
    var element = $(this).get(0);
    
    // workaround for firefox because window.getSelection does not work inside inputs
    if (typeof element.selectionStart == 'number') {
      return $(element).val().substr(element.selectionStart, element.selectionEnd - element.selectionStart);
    } else if (document.getSelection) {
      return document.getSelection();
    } else if (window.getSelection) {
      return window.getSelection();
    } else if (document.selection){
      return document.selection.createRange().text;
    } else return false;
  }
  
  // privates
  function setCaretTo(input, selectionStart, selectionEnd) {
    if (input.setSelectionRange) {
        input.focus();
        input.setSelectionRange(selectionStart, selectionEnd||selectionStart);
      }
      else if (input.createTextRange) {
        var range = input.createTextRange();
        range.collapse(true);
        range.moveEnd('character', selectionEnd);
        range.moveStart('character', selectionStart||selectionStart);
        range.select();
      }
      
  }
  
  function getCaretPosition(element) {
    if (typeof element.selectionStart == 'number'){
      return element.selectionStart;
    } else if (document.selection) {
      var range = document.selection.createRange();
      var rangeLength = range.text.length;
      range.moveStart('character', -element.value.length);
      return range.text.length - rangeLength;
    }
  }
})(jQuery);
$.fn.hasAttr = function(name) {  
   return this.attr(name) !== undefined;
};
//new function($) {
//    $.fn.getCursorPosition = function() {
//        var pos = 0;
//        var input = $(this).get(0);
        // IE Support
//        if (document.selection) {
//            input.focus();
//            var sel = document.selection.createRange();
//            var selLen = document.selection.createRange().text.length;
//            sel.moveStart('character', -input.value.length);
//            pos = sel.text.length - selLen;
//        }
        // Firefox support
//        else if (input.selectionStart || input.selectionStart == '0')
//            pos = input.selectionStart;
//
//        return pos;
//    }
//} (jQuery);

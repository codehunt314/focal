var MoogPlus = new Class({

	options: {
		rowSelector: '.row',
		activeClass: 'active',
		event: 'click',
		'class': 'MoogPlus',
		link: 'cancel',
		duration: 'short'
	},

	initialize: function(wrapper, options){
		wrapper = this.wrapper = $(wrapper);
		var element = this.element = $('<div/>').appendTo(wrapper);
		//this.parent(element, options);
		element.addClass(this.options['class']);
	},

    deactivate:function(){
        this.element.stop().animate({
			top: 0,
			height: 0
		});
		if (this.current) this.current.removeClass(this.options.activeClass);
		currentEntry = null;
    },
	activate: function(element){
		var coords = element.position();//getCoordinates(this.wrapper),
			activeClass = this.options.activeClass;
		this.element.stop().animate({
			top: coords.top,
			height: element.height()
		});
		if (this.current) this.current.removeClass(activeClass);
		this.current = element.addClass(activeClass);
		currentEntry = element;
	}

});

// var editModalBox = new ModalBox();

$(document).ready(function(){

	$("#div_edit_context").modal({keyboard:true,backdrop:true});
	$("#div_link_page").modal({keyboard:true,backdrop:true});
	
	if( gup('tour') == 'true' ){
        $('.bubble').removeClass('hide');
        $('.bubble .close').click(function(e){
        	e.stopPropagation();e.preventDefault();
        	var ele = $('#' + $(this).attr('rel'));
        	var close = $(this), bubble = $(this).parent();
        	bubble.animate({'opactiy':0,'width':0,'height':0,'left': ele.position().left + ele.width()/2,'top':ele.position().top+ele.height()/2},200,function(){bubble.remove();});
        });
    }

	// $('.new_context_link').click(function(e){
	//     e.stopPropagation(); 
	//     e.preventDefault();
	//     var id = $(this).attr('rel');
	//     var div = $('#'+id).clone();
	//     div.removeClass('hide');
	//     editModalBox.setContent(div);
	// });
	
	// $('#notification_count').click(function(e){
	//         e.stopPropagation();
	//         e.preventDefault();
	//         $('#notification').removeClass('hide');
	//         $(this).addClass('tab-active');
	//         $(document).bind('click',function(e){
	//             $('#notification').addClass('hide');
	//             $('#notification_count').removeClass('tab-active');
	//             $(this).unbind('click');
	//         });
	//         $('#notification').addEvent('click',function(e){
	//             e.stopPropagation();e.preventDefault();
	//         });
	//     });
	//     
	
	$('.header_link').click(function(e){
	    e.stopPropagation();e.preventDefault();
	    var div = $('#'+$(this).attr('rel'));
	    var link = $(this);
	    $(document).trigger('click');
	    div.removeClass('hide');
		link.addClass('tab-active');
		$(document).bind('click',function(e){
			div.addClass('hide');
			link.removeClass('tab-active');
			//$(this).unbind('click');
		});
		div.bind('click',function(e){
		    e.stopPropagation();
		});
	});	
});
function isUrl(s) {
	var regexp = /^((https?|ftp):\/\/)?(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i;
	return regexp.test(s);
}

function add_new_link(frm){
    var link = $('.txt_link', $(frm)).val().trim();
    if(isUrl(link)!=''){
    	$('#div_new_link').modal('hide');
    	$("#div_edit_context").modal('show');
    	$('#Focalify_loader').css('display','block');
        $('iframe',$("#div_edit_context")).css('visibility','hidden').attr('src', '/link_status/?link='+escape(link));
    }else{
        alert('Enter a valid URL');
    }
    return false;
}

function setNotificationHeight(height){
	$('#notification').css('height',500);
	$('#notification_frame').css('height',500);
}

function set_checked(ele){
    ele.toggleClass('selected');
    ele.getNext().checked = !(ele.getNext().checked);
}

function submit_form(form){ 
	var frm = $(form);
	$('.loader',frm).removeClass('hide');
	$('.message',frm).addClass('hide').html('');
	$.ajax({
		url: frm.attr("action") + '?is_ajax=true',
		type: 'post',
		dataType: 'json',
		data: frm.serialize(),
		success: function(obj){
			$('.loader',frm).addClass('hide');
			if(obj.status == 1){
	            message = "You're done !!";
	            window.location = obj.next;
	            $('.message',frm).removeClass('error');
	            $('#'+frm.attr('modal')).modal('hide')
	        }else{
	            message = obj.message;
	            $('.message',frm).addClass('error');
	        }
	        $('.message',frm).html(message).removeClass('hide');
		}
	});
    return false;
}

function gup( name )
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return "";
  else
    return results[1];
}
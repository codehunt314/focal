$(document).ready(function() {
   $('div.changeColor').hover(
       function(){
           var parent = $(this);
           var div = $('.change-color-downArrow',$(this));
           if(div.length>0){
               div.fadeIn();
           }else{
               parent.css('position','relative');
               var div = $('<div/>', {
                   'class':'change-color-downArrow',
                   css:{
                       'position':'absolute',
                       'left':0,
                       'top':0,
                       'width':parent.width(),
                       'height':parent.height(),
                       'line-height':parent.height()+'px',
                       'opacity':1,
                       'text-align':'center',
                       'overflow':'hidden',
                       'font-size': (parent.height()/2>10?parent.height()/2:10)+'px',
                       'cursor':'pointer'
                   },
                   html:'&#9660;',
                   click:function(){showColorBox(function(color){parent.css('background-color',color);});}
               }).appendTo(parent);
            }
       },
       function(){
           $('.change-color-downArrow',$(this)).fadeOut();
       }
   );
   
});
var availableColors = ['#800','#080','#008'];
function showColorBox(callback){
    
}
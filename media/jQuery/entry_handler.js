var EntryHandler = new Class({
    Implements: [Options, Events],
    options: {
    },
    initialize: function(options){
        this.setOptions(options);
        this.container = $('#entry_container');
        this.moogplus = new MoogPlus(this.container,{'rowSelector':'.entry'});
        this.scrollItem = scrollableElement('html', 'body');
        this.nextPageURL = '/?is_ajax=true';
        //this.windowScroll = new Fx.Scroll(window);
    },
    reset:function(){
        this.moogplus.deactivate();
        this.popup&&this.popup.addClass('hide');
        this.currentEntry = null;
    },
    click:function(e,ele){
        // if Click is on MORE_CONTEXT button (comment form)
        var target = $(e.target);
        if(target.hasClass('more_context')){
            var popup = target.next('.ul_dropdown');
            if(this.popup && this.popup != popup) this.popup.addClass("hide");
            this.popup = popup.toggleClass('hide');
            if(target.hasClass('up')){
                popup.css('bottom',-10);
                e.stopPropagation();e.preventDefault();
                return;
            }
        }else{
            if(target.hasClass('popupli')){
                this.changeLi(target);
            }
            else
                this.popup = this.popup?this.popup.addClass('hide'):this.popup;
        }
        
        // for full comment page
        if(target.hasClass('link_all_comment')){
            e.stopPropagation();e.preventDefault();
            this.open_link_page(e,target);
        }

        // for comment form toggle
        if(target.hasClass('a_show_form')){
            var element = $('#'+target.attr('rel'));
            element.slideToggle();
            // var height = element.css('height','auto').height();
            // element.height(0).removeClass("hide").animate({'height':height},300);
            // $(this.scrollItem).stop().animate({scrollTop: element.parents('.entry').position().top }, 400, function() {
            //             $('textarea',ele).focus();
            //           });
            //this.windowScroll.toElementCenter(id,'y').chain(function(){$(id).getElement('textarea').focus();});
            e.stopPropagation();
            e.preventDefault();
        }
        if(target.hasClass('cancel')){
            target.parent().animate({'height':0},200,function(){
                target.parent().addClass('hide');
                $('textarea',target.parent()).val('');
            });
            e.stopPropagation();
            e.preventDefault();
        }
        if(target.hasClass('edit')){
        	this.edit_context(e, target);
        	e.stopPropagation();
        	e.preventDefault();
        }
        if(!this.currentEntry || this.currentEntry[0]!=ele[0]){
            $(document).trigger('click');
        }
        this.focus(ele);
        e.stopPropagation();
    },
    focus:function(ele){
            this.currentEntry = ele;
            this.moogplus.activate(ele);
    },
    focusNext:function(){
        var currentEntry = this.currentEntry ? this.currentEntry.next('.entry') : $('.entry', $('#entry_container')).eq(0);
        if(currentEntry.length>0){
            this.focus(currentEntry);
            this.setPosition();
        }
    },
    focusPrev:function(){
        currentEntry = this.currentEntry ? this.currentEntry.prev('.entry') : $('.entry', $('#entry_container')).eq(0);
        if(currentEntry.length>0){
            this.focus(currentEntry);
            this.setPosition();
        }
    },
    setPosition:function(){
        $(this.scrollItem).stop().animate({scrollTop: this.currentEntry.position().top }, 400);
    },
    // enter:function(e,ele){
    // 	$('.hidden_link',ele).eq(0).stop().fadeTo('fast',1);//.fadeIn(300);
    // },
    // leave:function(e,ele){
    // 	$('.hidden_link',ele).eq(0).stop().fadeTo('fast',0);//.stop().fadeOut(500);
    // },
    
    submit:function(e,form){
        e.stopPropagation();e.preventDefault();
        var context = '';
        if($('textarea',form).val().trim() == ''){
            alert('Can not post empty Comment.');
            return;
        }
        if($('ul.popup',form)){
            context =   $('ul.popup li',form).filter(function(index){
                            return index!=0 && $(this).hasClass('selected');
                        }).map(function(index){
                        	return $(this).attr('rel');
                        }).toArray().join(',');
            var ele = $('<input/>',{'type':'hidden','value':context,'name':'contexts'}).appendTo(form);
        }
        
        $.ajax( form.attr('action')+"?is_ajax=true", {
        	type:'POST',
        	data:form.serialize(),
            beforeSend:function(){ $('.loader',form).removeClass('hide');},
            success:function(data){
                $('textarea',form).val('');
                if(form.parents('.comment').length>0){
                    var div = $('<div/>',{'html':data}).insertAfter(form.parents('.comment'));
                    form.addClass('hide')
                }else if(form.hasClass('popup-form')){
                    var cont = form.prev('#popup-comments');
                    var div = $('<div/>',{'html':data}).appendTo(cont);
                    cont.animate({scrollTop: cont.height()}, 500);
                }
                else{
                    var div = $('<div/>',{'html':data}).appendTo(form.prev('.comments_container'));
                }
                if(!form.hasClass('popup-form')){
                    form.slideToggle();
                }
            },
            complete:function(){
                $('.loader',form).addClass('hide');
            }
        });
    },
    changeLi: function(li){
        li.toggleClass('selected');
        var ul = li.parent('ul');
        if(li[0]==$('li',ul)[0]){
            if(li.hasClass('selected'))
                $('li',ul).addClass('selected');
            else
                $('li',ul).removeClass('selected');
        }else{
            if(!li.hasClass('selected'))
                $('li',ul).eq(0).removeClass('selected');
        }
        if($('li.selected',ul).length==0){
            $('input[type="submit"]', ul.parents('form'))[0].disabled = true;
            $('input[type="submit"]', ul.parents('form')).addClass('disabled')
        }else{
            $('input[type="submit"]', ul.parents('form'))[0].disabled = false;
            $('input[type="submit"]', ul.parents('form')).removeClass('disabled');
        }
    },
    getNextPageURL: function(){
        /*
            TODO 
            - Add mechanism to have correct url for next page
        */
        //return new URI().toString();
        return this.nextPageURL + "&page_num=" + (this.currentPage + 1);
        // if(window.location.search=="")
        //     return window.location.pathname + "?page_num="+ (this.currentPage + 1) + "&is_ajax=true";
        // else
        //     return window.location.pathname + window.location.search +"&page_num="+ (this.currentPage + 1) + "&is_ajax=true";
    },
    // getPageURL: function(li){
    //     var link = $('a', li);
    //     return '/?page_num=1&is_ajax=true&activity='+link.attr('key');
    // },
    injectEntries: function(html, currentPage, hasNext, append_or_replace){
        $(window).trigger(hasNext==="True"?"enableLoad":"disableLoad");
        //if(hasNext==="True"){$(window).trigger("enableLoad");} // enable scroll load if there are more pages.
        $('span',$('#load_more')).eq(0).text(hasNext==="True"?'More...':'');
        $('#entry_container')[append_or_replace](html);
    },
    showPage: function(obj,scroll){
        this.currentPage = parseInt(obj.page.number);
        this.injectEntries(obj.html, parseInt(obj.page.number), obj.page.has_next,'append');    
        $('#load_more').removeClass('loading');  
    },
    showLinks: function(obj, element){
        $('#load_links').removeClass('loading'); 
        this.currentPage = 1;
        if(typeof element === "string" && element === "FROM_SEARCH"){
            this.injectEntries(obj.html, 1, obj.page.has_next,'html');
            $("#span_total_results").html(obj.page.items);
        }
        else {
            if(typeof element === 'undefined'){
                this.injectEntries(obj.html, 1, obj.page.has_next,'html');
                if( obj.context.length){
                    $('.homeContent .headSmall').hide();
                    $('.homeContent .headBig').html(obj.context[0].name).show();
                    $('.homeContent .subLinks').html('<li>Links</li><li><a href="#/context/'+obj.context[0].key+'/manage">Members</a></li>').show();
                    facetSearch.searchBox.initQuery('','context:'+obj.context[0].name.replace(/,/g,'\\,'), false);
                    if(obj.context[0].name == 'Personal' && readCookie('no-import-message') === null ){
                        $("#import_message").removeClass('hide');
                    }else{
                        $("#import_message").addClass('hide');
                    }
                }else if(obj.user.length){
                    $('.homeContent .headSmall').hide();
                    $('.homeContent .subLinks').hide();
                    $('.homeContent .headBig').html(obj.user[0].name).show();
                    facetSearch.searchBox.initQuery('','from:'+obj.user[0].name.replace(/,/g,'\\,'), false);
                }else{
                    $('.homeContent .headSmall').html('Feed').show();
                    $('.homeContent .headBig').hide();
                    $('.homeContent .subLinks').hide();
                }
            }else{
                $(element).html(obj.html);
                $(window).trigger(obj.page.has_next==="True"?"enableLoad":"disableLoad");
                //if(obj.page.has_next==="True"){$(window).trigger("enableLoad");} // enable scroll load if there are more pages.
                $('span',$('#load_more')).eq(0).text(obj.page.has_next==="True"?'More...':'');
            }
        }
    },
    edit_context: function(e,ele){
        e.stopPropagation();
        e.preventDefault();
        var entry = ele.parents('.entry');
        var a = $('a.bookmark', entry).eq(0);
        //$("#div_edit_context").modal({backdrop:true,keyboard:true,show:true});
        $("#div_edit_context").modal('show');
        $('.modal-backdrop').css('background-color',"#fff");
        $('#Focalify_loader').css('display','block');

        var isTags = ele.hasClass('tags') ? '&edit_tags=true':'';
        $('iframe',$("#div_edit_context")).css('visibility','hidden').attr('src', '/link_status/?link='+escape(a.attr('href'))+isTags);

     //    var entry = ele.parents('.entry');
    	// var div = $('#div_edit_context').clone();
    	// div.removeClass('hide');
    	
    	// editModalBox.setWidth(402);
    	// editModalBox.setContent(div);
    },
    open_link_page: function(e,ele){
        var id = ele.attr('rel');
        var div = $("#div_link_page");
        var url = '/getcomment/?linkid='+id;
        div.modal('show');
        $('.modal-backdrop').css('background-color',"#fff");
        var entry = ele.parents('.entry').eq(0);
        $('h3',div).html($('h3',entry).html());
        $('h5.tags',div).html($('h5.tags',entry).html());
        $('.comments_container',div).empty();
        $('.loader',div).removeClass('hide');

        $.ajax({
            'url':url,
            'type':'GET',
            success : function(data) {
                $('.loader',div).addClass('hide');
                $('.comments_container',div).html(data);
                $("#popup-comments", div).css('max-height', div.height()-$('.popup-head',div).outerHeight()-$('.popup-form',div).outerHeight());
            }
        });
    }
    
});

// use the first element that is "scrollable"
  function scrollableElement(els) {
    for (var i = 0, argLength = arguments.length; i <argLength; i++) {
      var el = arguments[i],
          $scrollElement = $(el);
      if ($scrollElement.scrollTop()> 0) {
        return el;
      } else {
        $scrollElement.scrollTop(1);
        var isScrollable = $scrollElement.scrollTop()> 0;
        $scrollElement.scrollTop(0);
        if (isScrollable) {
          return el;
        }
      }
    }
    return [];
  }

function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
/*
(function($){
	$.fn.fancyShow = function() {
		return this.each(function() {
			this.fadein(300);
		}
	};
})(jQuery);


Element.implement({
    //implement show
    fancyShow: function() {
        this.set('tween', {duration: 300});
        this.fade('in');
    },
    //implement hide
    fancyHide: function() {
        this.fade('out');
    },
    append: function(newhtml){
        return this.adopt(new Element('div', {html: newhtml}).getChildren());
    }
});
*/
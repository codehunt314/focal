var currentEntry;
document.bind('keypress', function(e){
    var key = VS.app.hotkeys.key(e);
    if(key=='down'){
        currentEntry = currentEntry ? currentEntry.next('.entry') : $('.entry', $('#entry_container')).eq(0);
        setPosition(currentEntry);
        e.preventDefault();
    }
    if(key=='up'){
        currentEntry = currentEntry ? currentEntry.prev('.entry') : $('.entry', $('#entry_container')).eq(0);
        setPosition(currentEntry);
        e.preventDefault();
    }
    if(key=='esc'){
        unselect();
    }
});

function setPosition(entry){
    if(entry)
        moogplus.activate(entry);
    new Fx.Scroll(window).toElementCenter(entry, 'y');
}
function unselect(){
    moogplus.deactivate();
}
function hide_popup(){
    $$('.popup').addClass('hide');
}
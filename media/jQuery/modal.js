var ModalBox = new Class(
{
	Implements: [Options],
	options:
	{
       	top:-500,
       	actionClass:'action'
    },
	initialize: function(options)
	{	
		this.setOptions(options);
		this.initUI();
	},
    initUI:function(){
    	var modal = this; //save reference 
        this.divBG = $('<div/>',{
        	'class':'light-bg',
        	'click':function(e){
        		if($(e.target).hasClass('light-bg'))
        	    	modal.hide();
        	}
        }).appendTo($(document.body));
        
        this.divContent = $('<div/>',{
        	'class' : 'modalBox drop-shadow raised',
        	'html' : '<a href="javascript:void(0);"><img class="modal-close" src="/site_media/img/close.png" /></a><div class="modal-container">asdf</div>'
        	});
        
        //this.divContent.set('tween',{'duration':250});
        this.divContent.appendTo(this.divBG);
        this.divContainer = $('div.modal-container', this.divContent);
        $(document).bind('keydown', function(event){
			if(event.key=="esc"){
				modal.hide();
			}
		});
		$('a',this.divContent).bind('click',function(e){
		    e.stopPropagation();
		    modal.hide();
		});
    },
    setWidth:function(width){
        this.divContent.css('width',width);
    },
    setContent:function(html){
    	this.divContainer.empty();
    	$(document.body).addClass('theaterMode');
    	if(typeof(html) === 'string'){
    		this.divContainer.html(html);
    	}else{
    		html.appendTo(this.divContainer);
    	}
    	this.divContent.animate({'top':100},250);
    	if($('input[type="text"]', this.divContent)){
    		$('input[type="text"]', this.divContent).focus();
    	}
    },
    hide:function(){
        $(document.body).removeClass('theaterMode');
        this.divContent.animate({'top':this.options.top},250);
    }
});


function submit_form(form){
    new Request({
        url: form.get("action") + '?is_ajax=true',
        method: "post",
        data: form,
        onRequest: function() {
            form.getElement('.loader').removeClass('hide');
            form.getElement('.message').set('html','');
            //form.getElement('.message').set("html", "sending...");
        },
        onComplete: function() {
            form.getElement('.loader').addClass('hide');
            try{
                var obj = JSON.decode(this.response.text);
                var message = ''
                if(obj.status == 1){
                    message = "You're done. Hangon we are redirecting you to the correct page";
                    window.location = obj.next;
                }else{
                    message = obj.message;
                }
            }catch(e){
                message = "Some error occurred. Try Again Later";
            }
            form.getElement('.message').set("html", message);
        }
    }).send();

    return false;
}
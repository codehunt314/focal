$(document).ready(function(){
    showHome();
});

function showHome(){
	var height = $('#main_pane').height();
	height = height > 500 ? 500 : height;
	$('#notification-ul').height(height-50);
	if(parent.setNotificationHeight){
		parent.setNotificationHeight(height);
	}
}
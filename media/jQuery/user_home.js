var home_router;
$(document).ready(function() {
    var entry_handler = new EntryHandler();
    entry_handler.currentPage = currentPage;

    var HomeRoutes = Backbone.Router.extend({
        routes: {
            "": "home",
            "/context/:key": "contextPage", // #context/key
            "/context/:key/manage": "contextManage",
            "/user/:key": "userPage",// #user/key
            "/search/:query/filter/:filter": "searchPage",
            "/search/:query/filter/:filter/:sub_filter": "searchFilter"
        },
        loadJSON: function(url, element) {
            $('#load_links').addClass('loading');
            var self = this;
            $.getJSON(url, function(obj) {
                entry_handler.showLinks(obj, element);
                $("#goto-top").trigger('click');
            });
            this.initialLoadDone = true;
        },
        loadHTML: function(url, element) {
            $(window).trigger("disableLoad");
            $('#load_links').addClass('loading');
            $(element).load(url, function() {
                $('#load_links').removeClass('loading');
                $("#goto-top").trigger('click');
            });
            this.initialLoadDone = true;
        },
        home: function() {
            if (this.initialLoadDone) {
                this.loadJSON('/?full_page=true', '#main');
                entry_handler.nextPageURL = '/?is_ajax=true';
            }
            facetSearch.searchBox.initQuery('', '');
        },
        contextPage: function(key) {
            this.loadJSON('/context/' + key + '/?is_ajax=true');
            entry_handler.nextPageURL = '/context/' + key + '/?is_ajax=true';
        },
        contextManage: function(key) {
            this.loadHTML('/context/' + key + '/manage?is_ajax=true', '.homeContent');
            entry_handler.nextPageURL = '';
        },
        userPage: function(key) {
            this.loadJSON('/user/' + key + '/?is_ajax=true');
            entry_handler.nextPageURL = '/user/' + key + '/?is_ajax=true';
        },
        searchPage: function(query, filter) {
            query = query.replace(/&#47;/g,'/'); filter = filter.replace(/&#47;/g,'/');
            this.loadJSON('/search/?q=' + query + '&filter=' + filter + '&is_ajax=true&full_page=true', '#main');
            entry_handler.nextPageURL = '/search/?q=' + query + '&filter=' + filter + '&is_ajax=true';
            facetSearch.searchBox.initQuery(query, filter);
        },
        searchFilter: function(query, filter, sub_filter) {
            query = query.replace(/&#47;/g,'/'); filter = filter.replace(/&#47;/g,'/');
            var type = sub_filter.slice(0,sub_filter.indexOf(':'));
            var val = sub_filter.slice(type.length+1);
            sub_filter = ( filter.length>0?",":"" ) + sub_filter;
            if(this.shouldLoadSearch){
                this.loadJSON('/search/?q=' + query + '&filter=' + filter + sub_filter + '&is_ajax=true', 'FROM_SEARCH');
                entry_handler.nextPageURL = '/search/?q=' + query + '&filter=' + filter + "," + sub_filter + '&is_ajax=true';
                $('.homeSideBar li').removeClass('selected');
                $('.homeSideBar a[type="'+type+'"][name="'+val+'"]').parent().addClass('selected');
            }else{
                $('#load_links').addClass('loading');
                self = this;
                $.getJSON('/search/?q=' + query + '&filter=' + filter + '&is_ajax=true&full_page=true', function(obj) {
                    entry_handler.showLinks(obj, "#main");
                    self.loadJSON('/search/?q=' + query + '&filter=' + filter + sub_filter + '&is_ajax=true', 'FROM_SEARCH');
                    entry_handler.nextPageURL = '/search/?q=' + query + '&filter=' + filter + sub_filter + '&is_ajax=true';
                    $('.homeSideBar li').removeClass('selected');
                    $('.homeSideBar a[type="'+type+'"][name="'+val+'"]').parent().addClass('selected');
                });
                this.initialLoadDone = true;
            }
            
            facetSearch.searchBox.initQuery(query, filter);
        }
    });

    home_router = new HomeRoutes();
    // Start Backbone history a neccesary step for bookmarkable URL's
    Backbone.history.start();

    $('a.push').live('click', function(e) {
        e.preventDefault();

        var href = $(this).attr('href'); // TODO: This may break in IE due to incorrect href attr returned
        href = href.slice(1, href.length);
        // use backbone to shift url
        if (window.location.pathname == Backbone.history.options.root) home_router.navigate(href, true);
        else window.location = "/" + href;
    });
    $('.homeSideBar li').live('click', function(e) {
        $('.homeSideBar li').removeClass("selected");
        $(this).addClass('selected');
    });
    $('a.search_filter').live('click', function(e) {
        var facets = facetSearch.searchBox.getFacetList();
        var flag = true;
        for (var i = 0; i < facets.length; i += 2) {
            if (facets[i] === $(this).attr('type') && facets[i + 1] === $(this).text()) {
                flag = false;
                break;
            }
        }
        if (flag) {
            home_router.shouldLoadSearch = true;
            var href = $(this).attr('href');
            href = href.slice(1, href.length);
            home_router.navigate(href, true);
        }
    });


    // For Scroll Loader
    $(window).scrollLoader({
        loadContent: function() {
            $(window).trigger("disableLoad"); 
            var scroll = this; // Save a reference
            $('#load_more').addClass('loading');
            var url = entry_handler.getNextPageURL();
            $.getJSON(url, function(obj) {
                entry_handler.showPage(obj, scroll);
            });
        }
    });

    $(window).bind('scroll', function(e) {
        showGoToTop();
    });

    $(document).click(function(e) {
        entry_handler.reset();
    });

    if (!hasNext) {
        $(window).trigger("disableLoad");
        $('#load_more').addClass('hide');
    }

    // $('.entry').live({
    //     'mouseenter': function(e) {
    //         entry_handler.enter(e, $(this));
    //     },
    //     'mouseleave': function(e) {
    //         entry_handler.leave(e, $(this));
    //     }
    // });

    $(document).delegate(".entry", "click", function(e) {
        //$(document).trigger('click');
        entry_handler.click(e, $(this));
    });
    $(document).delegate(".entry form", "submit", function(e) {
        entry_handler.submit(e, $(this));
    });
    $('.more-context').live('click',function(e){
        var link = $(this);
        if(link.hasClass('collapse')){
            link.parents('.contexts').eq(0).animate({'height':35},300,function(){link.removeClass('collapse')});
        } else{
            var parent = link.parents('.contexts').eq(0);
            var h = parent.css('height','auto').height();
            parent.css('height',35);
            link.parents('.contexts').eq(0).animate({'height':h},300,function(){link.addClass('collapse')});
        }
    });

    $(document).bind('keydown', function(e) {
        if (e.target.tagName.toUpperCase() == 'INPUT' || e.target.tagName.toUpperCase() == 'TEXTAREA') {
            return;
        }
        var key = e.which;
        if (key == 40) { //down
            entry_handler.focusNext();
            e.preventDefault();
        }
        if (key == 38) { //'upArrow'
            entry_handler.focusPrev();
            e.preventDefault();
        }
        if (key == 27) { //'esc'
            entry_handler.reset();
        }
    });
    // <div class="twipsy fade above"><div class="twipsy-arrow"></div><div class="twipsy-inner">above</div></div>
    // $("a[rel=twipsy]").live('mouseover',function(e){
    //         var twipsy;
    //         var link = $(this);
    //         if($('div.twipsy',link).length>0){
    //             twipsy = $('div.twipsy',link);
    //         }else{
    //             twipsy = $("<div />",{
    //                 'class':'twipsy fade above',
    //                 'html': '<div class="twipsy-arrow"></div><div class="twipsy-inner">'+link.attr('title')+'</div>'
    //             }).appendTo(link);
    //         }
    //         twipsy.animate({
    //             'top':link.position().top - 50,
    //             'left': link.position().left - twipsy.position().left,
    //             'opacity':1
    //         },200);
    // });
    // $("a[rel=twipsy]").live('mouseover',function(e){
    //         var twipsy = $('div.twipsy',$(this));
    //         twipsy.animate({
    //             'top':50,
    //             'opacity':0
    //         },200);
    // });
    
                    
    // $('.homeSideBar').delegate('li', 'click', function(e){
    //     e.preventDefault();
    //     if(!$(this).hasClass('selected')){
    //         $('#load_links').addClass('loading');
    //         var url = entry_handler.getPageURL($(this));
    //         var self = this;
    //         $.getJSON( url, function(obj){
    //                 entry_handler.showContext(obj,self);
    //         });
    //     }
    // });
});

// Calling script is embedded in the page user_home_ajax_html.html
function invite_action(div, ele){
    var url = ele.hasClass('secondary') ? '/reject_activity_invite/' : '/referal/';
    $('.loader',div).removeClass('hide');
    $.ajax({
        'url':url,
        'type':'POST',
        'data':$('form',div).serialize(),
        success: function(data){
            $('div',div).html(data);
        }
    });
}

function showGoToTop() {
    var link = $('#goto-top').length > 0 ? $('#goto-top').eq(0) : $('<a/>', {
        id: "goto-top",
        'html': '<p>Go to Top <span>⇧</span></p><div></div>',
        'href': '#',
        click: function(e) {
            e.preventDefault();
            $('html,body').stop().animate({
                scrollTop: 0
            }, 500);
        }
    }).appendTo($(document.body));
    var top = $(this).scrollTop();
    link.css('opacity', top / 2000);
}
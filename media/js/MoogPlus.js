var MoogPlus = new Class({

	Extends: Fx.Morph,

	options: {
		rowSelector: '.row',
		activeClass: 'active',
		event: 'click',
		'class': 'MoogPlus',
		link: 'cancel',
		duration: 'short',
		transition: Fx.Transitions.Quad.easeOut
	},

	initialize: function(wrapper, options){
		wrapper = this.wrapper = document.id(wrapper);

		var element = this.element = new Element('div').inject(wrapper);
		this.parent(element, options);
		element.addClass(this.options['class']);

	},

    deactivate:function(){
        this.start({
			top: 0,
			height: 0
		});
		if (this.current) this.current.removeClass(this.options.activeClass);
		currentEntry = null;
    },
	activate: function(element){
		var coords = element.getCoordinates(this.wrapper),
			activeClass = this.options.activeClass;
		this.start({
			top: coords.top,
			height: coords.height
		});
		if (this.current) this.current.removeClass(activeClass);
		this.current = element.addClass(activeClass);
		currentEntry = element;
	}

});

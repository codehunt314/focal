var editModalBox = new ModalBox();

document.addEvent('domready',function(){
	$$('.new_context_link').addEvent('click',function(e){
	    e.stop(); 
	    var id = this.get('rel');
	    var div = $(id).clone();
	    div.removeClass('hide');
	    editModalBox.setContent(div);
	});
	
	$('notification_count').addEvent('click',function(e){
		e.stop();
		$('notification').removeClass('hide');
		this.addClass('tab-active');
		$(document).addEvent('click',function(e){
			$('notification').addClass('hide');
			$('notification_count').removeClass('tab-active');
			this.removeEvent('click');
		});
		$('notification').addEvent('click',function(e){
		    e.stop();
		});
	});
});

function setNotificationHeight(height){
	$('notification').setStyle('height',height);
	$('notification_frame').setStyle('height',height);
}

function set_checked(ele){
    ele.toggleClass('selected');
    ele.getNext().checked = !(ele.getNext().checked);
}
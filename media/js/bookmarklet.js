var Focalify = function() {
    var HOST = "focal.io";
    return {
        HOSTNAME : 'www.focal.io',
		
		isLocalHost : function(hostname) {
            return hostname == HOST ;
		},
        
        getSupportedProp: function(proparray){
            var root=document.documentElement //reference root element of document
            for (var i=0; i<proparray.length; i++){ //loop through possible properties
                if (typeof root.style[proparray[i]]=="string"){ //if the property value is a string (versus undefined)
                    return proparray[i] //return that string
                }
            }
        },
        
        close : function(){
            var el = document.getElementById('Focalify-container');
            el.parentNode.removeChild(el);
        },
        
        resizeDiv:function(){
            var el = document.getElementById('Focalify-container');
            el.getElementsByTagName('img')[1].style.display = 'none';
            el.getElementsByTagName('iframe')[0].style.visibility = 'visible';
        },
        
        init : function(){
            if(document.getElementById('Focalify-container')){
                var el = document.getElementById('Focalify-container');
                el.parentNode.removeChild(el);
            }
            var div = document.createElement('div');
            div.id = 'Focalify-container';
            div.style.width = '400px';
            div.style.height = '400px';
            div.style.position = 'fixed';
            div.style.top = '14px';
            div.style.right = '14px';
            div.style.backgroundColor = '#fff';
            div.style.textAlign = 'center';
            div.style.zIndex = '1000';
            
            var boxradiusprop=this.getSupportedProp(['borderRadius', 'MozBorderRadius', 'WebkitBorderRadius']);
            div.style[boxradiusprop] = '5px';

            var boxshadowprop=this.getSupportedProp(['boxShadow', 'MozBoxShadow', 'WebkitBoxShadow']);
            div.style[boxshadowprop] = "0px 0px 5px #8c8c8c";
            
            document.body.appendChild(div);
            var src = 'http://'+this.HOSTNAME+'/link_status/?link='+encodeURIComponent(window.location.href)+'&title='+encodeURIComponent(document.title);
            div.innerHTML = '<a href="javascript:void(0);" onclick="Focalify.close()"><img style="padding:0px;border:0px;position:absolute;right:-14px;top:-14px;" src="http://'+this.HOSTNAME+'/site_media/img/close.png" alt=""/></a>'+
                '<img src="http://'+this.HOSTNAME+'/site_media/img/22.gif" style="margin:20px auto" />'+
                '<iframe frameborder=0 style="border:0px;visibility:hidden;" onload="Focalify.resizeDiv();" src="'+src+'" width="100%" height="100%"></iframe>';
            
        }

	};

}();
Focalify.init();
function showComments(comments){
    $('commentCount').set('text',comments.length);
    if(comments.length>0){
        $('comment-').removeClass('hide');
    }
    comments.each(function(comment){
        var contextid = [], contextname = '';
        for(var i=0;i<comment.activities.length;i++){
            contextid[i] = comment.activities[i][0];
            contextname += comment.activities[i][1] + ", ";
        }
        contextname = contextname.slice(0,-2);
        
        var data = {
            'parent': comment.parent||'',
            'context':{'id': contextid,'name':contextname},
            'duration': comment.date,
            'text': comment.text,
        };
        showComment(data, comment.by, comment.id);
    });
}

function showComment(data, user, comment_id){
    
    var div = $('sampleComment').clone();
    div.set('id','comment-'+comment_id);
    div.removeClass("hide");
    
    div.getElement('h5').getElement('a').set('text', user);
    
    div.getElement('input.parent').set('value', comment_id);
    div.getElement('p.commentText').set('text',data.text);
    div.getElement('span.duration').set('text',data.duration?data.duration:'few Seconds');
    div.getElement('span.contexts').set('text',data.context.name);
    
    data.context.id.each(function(id){
        var input = new Element('input',{'name':'contexts','type':'hidden'}).inject(div.getElement('form'));
        input.set('value',id);
    });
    
    if(data.parent){
        div.inject($('comment-'+data.parent), 'bottom');
        div.setStyle('margin-left', parseInt($('comment-'+data.parent).getStyle('margin-left')) + 30);
        div.addClass(div.getParent('.commentBody').hasClass('blue')?'ash':'blue');
    }
    else{
        div.inject($('comment-'), 'bottom');
        if(div.getPrevious('.commentBody'))
            div.addClass(div.getPrevious('.commentBody').hasClass('blue')?'ash':'blue');
        else
            div.addClass('blue');
    }
}

function editContext(link){
    link.getParent('span').getNext('select').removeClass('hide');
    link.getParent('span').addClass('hide');
}

document.addEvent('domready',function(e){
    showComments(comments);
    $$('.link-reply').addEvent('click',function(e){
        e.stop();
        this.getParent('p').getNext('form').toggleClass('hide');
    });
});
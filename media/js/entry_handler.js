var EntryHandler = new Class({
    Implements: [Options, Events],
    options: {
    },
    initialize: function(options){
        this.setOptions(options);
        this.container = $('entry_container');
        this.moogplus = new MoogPlus(this.container,{'rowSelector':'.entry'});
        this.windowScroll = new Fx.Scroll(window);
    },
    reset:function(){
        this.moogplus.deactivate();
        this.popup&&this.popup.addClass('hide');
    },
    click:function(e,ele){
        // if Click is on MORE_CONTEXT button (comment form)
        if(e.target.hasClass('more_context')){
            var popup = e.target.getNext('.ul_dropdown');
            if(this.popup && this.popup != popup) this.popup.addClass("hide");
            this.popup = popup.toggleClass('hide');
        }else{
            if(e.target.hasClass('popupli'))
                this.changeLi(e.target);
            else
                this.popup = this.popup?this.popup.addClass('hide'):this.popup;
        }
        
        // for comment form toggle
        if(e.target.hasClass('a_show_form')){
            var id = e.target.get('rel');
            $(id).removeClass("hide");
            this.windowScroll.toElementCenter(id,'y').chain(function(){$(id).getElement('textarea').focus();});
            e.stop();
            this.moogplus.activate(ele);
            return;
        }
        
        if(e.target.hasClass('edit_link')){
        	this.edit_context(e, e.target);
        }
        
        this.moogplus.activate(ele);
        e.stopPropagation();
    },
    enter:function(e,ele){
        ele.getElement('.hidden_link').fancyShow();
    },
    leave:function(e,ele){
        ele.getElement('.hidden_link').fancyHide();
    },
    
    submit:function(e,form){
        e.stop();
        var context = '';
        if(form.getElement('ul.popup')){
            context =   form.getElement('ul.popup').getElements('li').filter(function(item,index){
                            return index!=0 && item.hasClass('selected');
                        }).map(function(item){return item.get('rel')}).join(',');
            var ele = new Element('input',{'type':'hidden','value':context,'name':'contexts'}).inject(form);
        }
        new Request.HTML({
            url: form.get('action')+"?is_ajax=true",
            onRequest:function(){form.getElement('.loader').removeClass('hide');},
            onSuccess:function(responseTree, responseElements, responseHTML, responseJavaScript){
                form.getElement('textarea').set('value','');
                if(form.getParent('.comment')){
                    var div = new Element('div',{'html':responseHTML}).inject(form.getParent('.comment'),'after');
                    form.addClass('hide')
                }else{
                    var div = new Element('div',{'html':responseHTML}).inject(form.getPrevious('.comments_container'),'bottom');
                }
            },
            onComplete:function(){
                form.getElement('.loader').addClass('hide');
            }
        }).post(form);
    },
    changeLi: function(li){
        li.toggleClass('selected');
        var ul = li.getParent('ul');
        if(li==ul.getElement('li')){
            if(li.hasClass('selected'))
                ul.getElements('li').addClass('selected');
            else
                ul.getElements('li').removeClass('selected');
        }else{
            if(!li.hasClass('selected'))
                ul.getElement('li').removeClass('selected');
        }
        if(ul.getElements('li.selected').length==0){
            ul.getParent('form').getElement('input[type="submit"]').disabled = true;
            ul.getParent('form').getElement('input[type="submit"]').addClass('disabled')
        }else{
            ul.getParent('form').getElement('input[type="submit"]').disabled = false;
            ul.getParent('form').getElement('input[type="submit"]').removeClass('disabled');
        }
    },
    getNextPageURL: function(){
        /*
            TODO 
            - Add mechanism to have correct url for next page
        */
        return new URI().toString();
        return window.location;
    },
    
    showPage: function(obj,scroll){
        this.currentPage = parseInt(obj.page.number);
        if(obj.page.has_next == "True"){
            scroll.attach();
            $('load_more').getElement('span').set('text','More...');
        }else{
            $('load_more').getElement('span').set('text','No More Links');
        }
        $('entry_container').append(obj.html);
        $('load_more').removeClass('loading');        
    },
    
    edit_context: function(e,ele){
        e.stop();
        var entry = ele.getParent('.entry');
    	var div = $('div_edit_context').clone();
    	div.removeClass('hide');
    	var a = entry.getElement('a.bookmark');
    	div.getElement('h3').set('text',a.get('text'));
    	div.getElement('span.link').set('text',a.get('href'));
    	div.getElement('span.link').set('rel',a.get('rel'));
    	div.getElement('h4').set('text',entry.getElement('h4.title').get('text'));
    	entry.getElements('.context').each(function(ele){
    		var id = ele.get('id').slice(2);
    		
    		if(ele.get('edit')=='false'){
    			div.getElement('a.a-'+id).addClass('noedit');
    			div.getElement('a.a-'+id).set('tip', ele.get('tip'));
    		}else{
    			div.getElement('a.a-'+id).addClass('selected');
    		}
    	});
    	var faces = new FaceTip(div.getElements('a.noedit'));
    	editModalBox.setContent(div);
    }
    
});

Element.implement({
    //implement show
    fancyShow: function() {
        this.set('tween', {duration: 300});
        this.fade('in');
    },
    //implement hide
    fancyHide: function() {
        this.fade('out');
    },
    append: function(newhtml){
        return this.adopt(new Element('div', {html: newhtml}).getChildren());
    }
});

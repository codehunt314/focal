var currentEntry, moogplus;

document.addEvent('domready',function(){
    moogplus = new MoogPlus('entry_container',{'rowSelector':'.entry'});
    $('searchBox').addEvent('keydown',function(e){
        e.stopPropagation();
    })
})
document.addEvent('keydown:keys(down)', function(e,ele){
    e.stop();
    currentEntry = currentEntry ? currentEntry.getNext('.entry') : $('entry_container').getElement('.entry');
    setPosition(currentEntry);
});
document.addEvent('keydown:keys(up)', function(e,ele){
    e.stop();
    currentEntry = currentEntry ? currentEntry.getPrevious('.entry') : $('entry_container').getElement('.entry');
    setPosition(currentEntry);
});
document.addEvent('click',function(){
    unselect();
    hide_popup();
});

document.addEvent('keydown:keys(esc)',function(){unselect();});
function setPosition(entry){
    if(entry)
        moogplus.activate(entry);
    new Fx.Scroll(window).toElementCenter(entry, 'y');
}
function unselect(){
    moogplus.deactivate();
}
function hide_popup(){
    $$('.popup').addClass('hide');
}
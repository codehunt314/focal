//var commentHTML = '';
var editModalBox = new ModalBox();

document.addEvent('domready',function(e){
    /*var lightbox = new LightBox({'margin':37});
    $$('.comment_link').addEvent('click',function(e){
        e.stop();
        //lightbox.setContent(this.get('text'));
    });*/
    $('entry_container').addEvent('click:relay(li.popupli)',function(e,ele){
       e.stopPropagation();e.stop();
       console.log('helo');
    });
    $$('.hidden_link').set('tween', {duration: 300});
    $('entry_container').addEvents({
        'mouseenter:relay(.entry)':function(e,ele){
            ele.getElements('.hidden_link').tween('opacity',1);
        },
        'mouseleave:relay(.entry)':function(e,ele){
            ele.getElements('.hidden_link').tween('opacity',0);
        }
    });
    $('entry_container').addEvent('click:relay(.edit_link)',function(e,ele){
        edit_context.bind(ele,e)();
    });
    //$$('.edit_link').addEvent('click',edit_context);
    $$('.new_context_link').addEvent('click',function(e){
        e.stop(); 
        var id = this.get('rel');
        var div = $(id).clone();
        div.removeClass('hide');
        editModalBox.setContent(div);
    });
    if( gup('tour') == 'true' ){
        var l = new Element('link',{
            'href':'/site_media/css/tour.css',
            'rel':'stylesheet'
        });
        l.inject(document.getElement('link'),'before');
        var s = new Element('script',{
            'src':'/site_media/js/tour.js',
            'type':'text/javascript',
            'async':true
        });
        s.inject(document.getElement('link'),'before');
    }
    
    var scroll = new ScrollLoader({
        onScroll: function(){
            this.detach(); // While waiting, we detach the listener so the event does not fire accidentally

            var scroll = this; // Save a reference
            
            $('load_more').addClass('loading');
            var url = getNextPageURL();
            new Request.JSON({url: url, onSuccess: function(obj){
                currentPage = parseInt(obj.page.number);
                if(obj.page.has_next == "True"){
                    scroll.attach();
                    $('load_more').getElement('span').set('text','More...');
                }else{
                    $('load_more').getElement('span').set('text','No More Links');
                }
                obj.links.each(show_link);
                $('load_more').removeClass('loading');
            }}).get({
                'page_num': currentPage + 1,
                'is_ajax':true
            });
        }
    });
    if(!hasNext){
        scroll.detach();
        $('load_more').addClass('hide');
    }
});

function commentForm(div){
    div.toggleClass('hide');
    if(!div.hasClass('hide'))
        div.getElement('textarea').focus();
    return false;
}
function show_dropdown_context(btn){
    btn.getNext('.ul_dropdown').toggleClass('hide');
    this.event.stopPropagation();
}
function getNextPageURL(){
	return '/'
}

function show_link(link, index, array){
    var entry = new Element('div',{
        'class':'entry',
        'events':{
            'mouseenter':function(e){
                this.getElements('.hidden_link').tween('opacity',1);
            },
            'mouseleave':function(e){
                this.getElements('.hidden_link').tween('opacity',0);
            }
        }
    });
    
    var html = ''
    if(link.favicon)
        html += '<img class="favi" src="'+link.favicon+'" />'; 
    html += '<a rel="'+link.id+'" class="bookmark" href="'+link.url+'">'+link.title+'</a> <em>(<span class="domain">'+link.domain+'</span>)</em>'
    var h3 = new Element('h3',{
        'html':html
    }).inject(entry);
    
    html = '';
    for(var i=0; i < link.shared.length ; i++){
        html += ' <a href="#">'+link.shared[i].user+'</a> Posted in '
        for( var j=0; j < link.shared[i].contexts.length; j++){
            var a = link.shared[i].contexts[j];
            html += '<a class="context" id="c-'+a.id+'" '
            if(a.can_delete == 'False')
                html += 'tip="Cannot remove link from this Channel" edit="false" '
            html += 'href="?activity='+a.name+'">'+a.name+'</a>'
            
            if(j<link.shared[i].contexts.length-1)
                html += '<br />'
        }
    }
    html += ' <a href="javascript:void(0);" rel="div_edit_context" class="edit_link hidden_link">edit</a><br />';
    html += link.timesince + ' ago | <a class="comment_link" href="/discuss/'+link.id+'/">'+link.comments.count+' Comments</a>'
    
    var div = new Element('div', {'class':'subtext', 'html':html}).inject(entry);
    div.getElement('.edit_link').addEvent('click',edit_context);
    entry.inject('entry_container');
}

function edit_context(e){
    e.stop();
    var ele = this;
	var entry = ele.getParent('.entry');
	var div = $('div_edit_context').clone();
	div.removeClass('hide');
	var a = entry.getElement('a.bookmark');
	div.getElement('h3').set('text',a.get('text'));
	div.getElement('span.link').set('text',a.get('href'));
	div.getElement('span.link').set('rel',a.get('rel'));
	div.getElement('h4').set('text',entry.getElement('h4.title').get('text'));
	entry.getElements('.context').each(function(ele){
		var id = ele.get('id').slice(2);
		
		if(ele.get('edit')=='false'){
			div.getElement('a.a-'+id).addClass('noedit');
			div.getElement('a.a-'+id).set('tip', ele.get('tip'));
		}else{
			div.getElement('a.a-'+id).addClass('selected');
		}
	});
	var faces = new FaceTip(div.getElements('a.noedit'));
	editModalBox.setContent(div);
}

function share_in_channel(link){
    if($(link).hasClass('loading') || $(link).hasClass('noedit'))
        return;
    var actId = $(link).get('rel');
    var url = $(link).hasClass('selected') ? "/deletealink/" : "/sharealink/";
    link.set('prev',link.className);
    $(link).removeClass('selected').addClass('loading');
    var div = link.getParent('.modal_content');
    var span = div.getElement('span.link');
    var jsonRequest = new Request.JSON({url: url, onSuccess: function(obj){
        if(obj.status == 0){
            this.className = this.get('prev');
            
        }else{
            this.className = obj.status == 1 ? 'selected' : '';
        }
    }.bind($(link))}).post({
        'contexts': actId, 
        'link':span.get('text')
    });
}


function set_checked(ele){
    ele.toggleClass('selected');
    ele.getNext().checked = !(ele.getNext().checked);
}
function gup( name )
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return "";
  else
    return results[1];
}


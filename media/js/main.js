/* Some change */
document.addEvent('domready', function(e){
    $$('a.vote').addEvent('click',function(e){
        e.stop();
        save_vote(this);
    });
});

function save_vote(ele){
    var div = ele.getParent('div.vote');
    var url = '/idea/'+div.get('idea')+'/vote/';
    
    if(ele.hasClass('active')){
        return;
    }else{
        if(ele.hasClass('inactive')){
            num_vote = 0;
        }else{
            if(ele.hasClass('up'))
                num_vote = 1;
            if(ele.hasClass('down'))
                num_vote = -1;
        }
    }
    
    
    var myRequest = new Request.HTML({url: url, method: 'post', useSpinner: true, spinnerTarget: div, spinnerOptions:{fxOptions:{duration:0}},
        onSuccess: function(responseTree, responseElements, responseHTML, responseJavaScript) {
            div.set('html',responseHTML);
            div.getElements('a.vote').addEvent('click',function(e){
                e.stop();
                save_vote(this);
            });
        },
        onFailure: function(xhr){
            console.log(xhr.responseText);
        }
    }).send('num_vote='+num_vote);
}

function post_comment(){
    if($('id_comment').get('value').trim() != ''){
        $('comment_form').submit();
        return;
    }
    alert('Error : enter some comment text');
    return;
}
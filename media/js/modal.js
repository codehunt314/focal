var ModalBox = new Class(
{
	Implements: [Options, Events],
	options:
	{
       	top:-500,
       	actionClass:'action'
    },
	initialize: function(options)
	{	
		this.setOptions(options);
		this.initUI();
	},
    initUI:function(){
        this.divBG = new Element('div',{
            'class':'light-bg',
            events:{
                'click':function(e){
                	if(e.target.hasClass('light-bg'))
                    	this.hide();
                }.bind(this)
            }
        }).inject(document.body);
        this.divContent = new Element('div',{
            'class' : 'modalBox drop-shadow raised',
            'html' : '<a href="javascript:void(0);"><img class="modal-close" src="/site_media/img/close.png" /></a><div class="modal-container">asdf</div>'
        });
		this.divContent.set('tween',{'duration':250});
        this.divContent.inject(this.divBG);
        this.divContainer = this.divContent.getElement('div.modal-container');
        $(document).addEvent('keydown', function(event){
			if(event.key=="esc"){
				this.hide();
			}
		}.bind(this));
		this.divContent.getElement('a').addEvent('click',function(e){
		    e.stop();
		    this.hide();
		}.bind(this));
    },
    setContent:function(html){
    	this.divContainer.empty();
    	document.body.addClass('theaterMode');
    	if(typeof(html) === 'string'){
    		this.divContainer.set('html',html);
    	}else{
    		html.inject(this.divContainer);
    	}
    	this.divContent.tween('top',100);
    	if(this.divContent.getElement('input[type="text"]')){
    		this.divContent.getElement('input[type="text"]').focus();
    	}
    },
    hide:function(){
        document.body.removeClass('theaterMode');
        this.divContent.tween('top',this.options.top);
    }
});


function submit_form(form){
    new Request({
        url: form.get("action") + '?is_ajax=true',
        method: "post",
        data: form,
        onRequest: function() {
            form.getElement('.loader').removeClass('hide');
            form.getElement('.message').set('html','');
            //form.getElement('.message').set("html", "sending...");
        },
        onComplete: function() {
            form.getElement('.loader').addClass('hide');
            try{
                var obj = JSON.decode(this.response.text);
                var message = ''
                if(obj.status == 1){
                    message = "You're done. Hangon we are redirecting you to the correct page";
                    window.location = obj.next;
                }else{
                    message = obj.message;
                }
            }catch(e){
                message = "Some error occurred. Try Again Later";
            }
            form.getElement('.message').set("html", message);
        }
    }).send();

    return false;
}
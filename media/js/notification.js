document.addEvent('domready',function(e){
    showHome();
});

function showHome(){
	setHeight($('main_pane'));
}

function setHeight(ele){
	var height = typeOf(ele) == 'number' ? ele : ele.getHeight();
	if(parent.setNotificationHeight)
		parent.setNotificationHeight(height);
}

/* Code Below this was working for Notifications like Google+ */
/*
document.addEvent('domready',function(e){
    showHome();
    $('notification').addEvent('click:relay("li.notification-li")',function(e){
    	e.stop();
		var id = 'notification-'+this.get('rel');
		loadRight(id);
    });
    
    $$('textarea.inactive').addEvents({
    	'focus':function(e){
    		var h1 = this.getParent('.notification-pane').getHeight();
    		this.removeClass('inactive');
    		this.set('value','');
    		this.getNext('.buttons').removeClass('hide');
    		var h2 = this.getParent('.notification-pane').getHeight();
    		$('notification_container').setStyle('height', $('notification_container').getHeight() + (h2-h1));
    		setHeight($('next_pane').getHeight() + (h2-h1));
    	}
    });
    
});

var current_notification;

function loadRight(id, left){
	$('next_pane').removeClass('hide');
	$('main_pane').addClass('hide');
	
	
	if(! $('next_pane').hasClass('shown') ){
		$$(".notification-pane").addClass('hide');
		$(id).removeClass('hide');
		$('notification_container').setStyle('height', $(id).getHeight());
		
		setHeight($('next_pane'));
		
		var myEffect = new Fx.Morph('next_pane', {
		    duration: '150',
		    transition: Fx.Transitions.Sine.easeOut,
		    onComplete: function(){
		    	$('next_pane').addClass('shown');
		    }
		});
		myEffect.start({
			'left':[400,0]
		});
	}else{
		
		$(current_notification).addClass('hide');
		$(id).removeClass('hide');
		$('notification_container').setStyle('height', $(id).getHeight());
		
		
		setHeight($('next_pane'));
		var myEffect = new Fx.Morph(id, {
		    duration: '150',
		    transition: Fx.Transitions.Sine.easeOut
		});
		
		var dir = left ? [-400,0] : [400,0];
					
		myEffect.start({
			'left':dir
		});
	}
	if(!$(id).getPrevious('.notification-pane'))
		$('link-prev').addClass('disabled').setStyle('color','#888');
	else
		$('link-prev').removeClass('disabled').setStyle('color','#56d9fc');
		
	if(!$(id).getNext('.notification-pane'))
		$('link-next').addClass('disabled').setStyle('color','#888');
	else
		$('link-next').removeClass('disabled').setStyle('color','#56d9fc');
		
		
	current_notification = id; 
}

function showPrev(ele){
	if(!ele.hasClass('disabled')){
		var id = $(current_notification).getPrevious('.notification-pane').get('id');
		loadRight(id, true);
	}
}
function showNext(ele){
	if(!ele.hasClass('disabled')){
		var id = $(current_notification).getNext('.notification-pane').get('id');
		loadRight(id);
	}
}

function showHome(){
	$('next_pane').addClass('hide');
	$('main_pane').removeClass('hide');
	setHeight($('main_pane'));

	if($('next_pane').hasClass('shown') ){
		setHeight($('main_pane'));
		
		var myEffect = new Fx.Morph('main_pane', {
		    duration: '150',
		    transition: Fx.Transitions.Sine.easeOut,
		    onComplete: function(){
		    	$('next_pane').removeClass('shown');
		 	}
		});
		myEffect.start({
			'left':[-400,0]
		});
	}
}

function setHeight(ele){
	var height = typeOf(ele) == 'number' ? ele : ele.getHeight();
	$('notification').setStyle('height',height);
	if(parent.setNotificationHeight)
		parent.setNotificationHeight(height);
}
*/
var URL = {
    'twitter':'/auth/twitter/',
    'facebook':'https://graph.facebook.com/oauth/authorize?client_id=146906412044571&redirect_uri=http://www.focal.io/auth/facebook/&scope=offline_access,publish_stream',
	'instapaper':'/auth/instapaper/'
};
function open_win(site, ele){
    // var img = new Element('img',{'class':'load', 'src':'/site_media/img/load.gif'});
    ele = $(ele);
    var img = $('<img/>',{'class':'load', 'src':'/site_media/img/load.gif'});
    var pos = ele.position();
    img.css({'position':'absolute', 'left':pos.left+ele.width()+25, 'top':pos.top});
    img.appendTo(ele.parent());
    
    var Timer = function(){
        var myfun = function(){
            if(this.myRef.closed==true){
                //twitter_linked("False");
                site_linked(this.ele, "False");
                clearInterval(this.timer);
            }
        };
        return {
            init : function(){
                this.myRef = window.open(URL[site], 'mywin','left=20,top=20,width=1000,height=505,toolbar=0,resizable=1,location=0,menubar=0');
                this.timer = myfun.periodical(100, this);
                this.ele = ele;
            }
        }
    }().init();
}
 
 
function site_linked(ele, flag){
    if(flag=="True"){
        var img = $('<img/>',{'src':'/site_media/img/ok.gif'});
        $('img.load', ele.parent()).remove();// .getElement('img.load').dispose();
        var pos = ele.position();
        img.css({'margin-left':12});
        img.appendTo(ele.parent());
        ele.onclick="";
        ele.css('cursor','text');
    }else{
    	// TODO : DO Something on success
        $('img.load', ele.parent()).remove();
        window.location.reload(false);
    }
}
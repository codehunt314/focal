document.addEvent('domready',function(e){
    var divBG = new Element('div',{
        'class':'light-bg'
    }).inject(document.body);
    var divContent = new Element('div',{
        'class' : 'drop-shadow raised tourBox',
        html : '<div id="tourDiv1"><img src="/site_media/img/22.gif" /></div><div class="hide" id="tourDiv2"><img src="/site_media/img/22.gif" /></div><div class="hide" id="tourDiv3"><img src="/site_media/img/22.gif" /></div>'
    });
    divContent.inject(divBG);
    document.body.addClass('theaterMode');
    
    $('tourDiv1').set('load', {evalScripts: true});
    $('tourDiv1').load('/site_media/tour/1.html');
    $('tourDiv2').set('load', {evalScripts: true});
    $('tourDiv2').load('/site_media/tour/2.html');
    $('tourDiv3').set('load', {evalScripts: true});
    $('tourDiv3').load('/site_media/tour/3.html');
});
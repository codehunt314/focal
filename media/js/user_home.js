document.addEvent('domready',function(e){
    var entry_handler = new EntryHandler();
    entry_handler.currentPage = currentPage;
    
    $('entry_container').addEvents({
        'click:relay(.entry)':function(e,ele){
            entry_handler.click(e,ele);
        },
        'mouseenter:relay(.entry)':function(e,ele){
            entry_handler.enter(e,ele);
        },
        'mouseleave:relay(.entry)':function(e,ele){
            entry_handler.leave(e,ele);
        },
        'submit:relay(form)':function(e,ele){
            entry_handler.submit(e,ele);
        }
    });
    
    
    document.addEvent('click',function(){entry_handler.reset();})
    var scroll = new ScrollLoader({
        onScroll: function(){
            this.detach(); // While waiting, we detach the listener so the event does not fire accidentally
			
            var scroll = this; // Save a reference
            $('load_more').addClass('loading');
            var url = entry_handler.getNextPageURL();
            new Request.JSON({url: url, onSuccess: function(obj){
                entry_handler.showPage(obj,scroll);
                // currentPage = parseInt(obj.page.number);
                // if(obj.page.has_next == "True"){
                //     scroll.attach();
                //     $('load_more').getElement('span').set('text','More...');
                // }else{
                //     $('load_more').getElement('span').set('text','No More Links');
                // }
                // obj.links.each(show_link);
                // $('load_more').removeClass('loading');
            }}).get({
                'page_num': entry_handler.currentPage + 1,
                'is_ajax':true
            });
        }
    });
    if(!hasNext){
        scroll.detach();
        $('load_more').addClass('hide');
    }
});
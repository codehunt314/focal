var SearchParser = {

  // Matches `category: "free text"`, with and without quotes.
  ALL_FIELDS : /('.+?'|".+?"|[^'"\s]{2}\S*):\s*('.+?'|".+?"|[^'"\s]\S*)/g,

  // Matches a single category without the text. Used to correctly extract facets.
  CATEGORY   : /('.+?'|".+?"|[^'"\s]{2}\S*):\s*/,

  // Called to parse a query into a collection of `SearchFacet` models.
  parse : function(query) {
    var searchFacets = this._extractAllFacets(query);
    return searchFacets;
  },

  // Walks the query and extracts facets, categories, and free text.
  _extractAllFacets : function(query) {
    var facets = {};
    var originalQuery = query;

    while (query) {
      var category, value;
      originalQuery = query;
      var field = this._extractNextField(query);
      if (!field) {
        category = 'text';
        value    = this._extractSearchText(query);
        query    = VS.utils.inflector.trim(query.replace(value, ''));
      } else if (field.indexOf(':') != -1) {
        category = field.match(this.CATEGORY)[1].replace(/(^['"]|['"]$)/g, '');
        value    = field.replace(this.CATEGORY, '').replace(/(^['"]|['"]$)/g, '');
        query    = VS.utils.inflector.trim(query.replace(field, ''));
      } else if (field.indexOf(':') == -1) {
        category = 'text';
        value    = field;
        query    = VS.utils.inflector.trim(query.replace(value, ''));
      }

      if (category && value) {
          if(facets[category])
            facets[category].push(value);
          else
            facets[category] = [value];
      }
      if (originalQuery == query) break;
    }

    return facets;
  },

  // Extracts the first field found, capturing any free text that comes
  // before the category.
  _extractNextField : function(query) {
    var textRe = /^\s*(\S+)\s+(?=\w+:\s?(('.+?'|".+?")|([^'"]{2}\S*)))/;
    var textMatch = query.match(textRe);
    if (textMatch && textMatch.length >= 1) {
      return textMatch[1];
    } else {
      return this._extractFirstField(query);
    }
  },

  // If there is no free text before the facet, extract the category and value.
  _extractFirstField : function(query) {
    var fields = query.match(this.ALL_FIELDS);
    return fields && fields.length && fields[0];
  },

  // If the found match is not a category and facet, extract the trimmed free text.
  _extractSearchText : function(query) {
    query = query || '';
    var text = VS.utils.inflector.trim(query.replace(this.ALL_FIELDS, ''));
    return text;
  }

};

$(document).ready(function(){
    var query = SearchParser.parse(searchQuery);
    var html = " ";
    if(query['text'])
        html += (' for <em>&ldquo;' + query['text'].join(' ') + '&rdquo;</em>');
    if(query['tag'])
        html += (' tagged with <em>&ldquo;' +  query['tag'].join(', ') + '&rdquo;</em>');
    if(query['context'])
        html += (' under context <em>&ldquo;' +  query['context'].join(', ') + '&rdquo;</em>');
    if(query['from'])
        html += (' by <em>&ldquo;' +  query['from'].join(', ') + '&rdquo;</em>');
    $('#facets').html($('#facets').html() + html);
});
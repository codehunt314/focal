from lxml import html
from myproject.activity.models import *
from django.contrib.contenttypes.models import ContentType
import urllib2, re
from datetime import datetime

def get_charset(headers, default='utf-8'):
    try:
        content_type = headers['content-type'].lower()
        if content_type.find('charset=') > 0:
            return content_type.split('charset=')[-1].lower()
    except KeyError:
        pass
    return default


def read_url(url, until=None, chunk=100):
    try:
        response = urllib2.urlopen(url)
    except urllib2.URLError:
        return u''
    encoding = get_charset(response.headers)
    if until:
        next, data, trunk_at = True, '', None
        while next:
            next = response.read(chunk)
            data += next
            until_match = re.search(until, data, re.IGNORECASE)
            if until_match:
                response.close()
                data = unicode(data, encoding)
                return data[:data.find(until) + len(until)]
    else:
        data = response.read()
    return unicode(data, encoding)


def extract_title_of_url(html_data):
    r = html.etree.HTML(html_data)
    return r.find('.//title').text


def extract_url(activity, file_data, submited_by):
    def f(root):
        for chld in root.getchildren():
            if isinstance(chld, html.HtmlComment):
                root.remove(chld)
            elif chld.tag in ['script', 'style']:
                root.remove(chld)
            else:
                f(chld)
    tree = html.fromstring(file_data)
    f(tree)
    ret = []
    all_links = tree.findall('.//a')
    dt = filter(lambda x: x.get('from', None)=="focaluser", tree.findall('.//dt'))
    if len(dt)==0:
        file_data = file_data.replace("<DT><A ", "<A ")
        tree = html.fromstring(file_data)
        alink = tree.findall('.//a')
        link_to_process = []
        for i in alink:
            link = i.get('href', None)
            tags = i.get('tags', None)
            if link and not link.startswith('javascript') and link.startswith('http'):
                link_to_process.append((link, i.text or link, tags))
        ret.append((activity, link_to_process))
    else:
        for each_dt in dt:
            activityname = each_dt[0].text_content().strip()
            acts = filter(lambda x:x.activityname==activityname, ActivityMember.objects.get_user_activity(submited_by))
            if len(acts)==0:
                act, crtd = Activity.objects.create_activity(activityname=activityname, created_by=submited_by)
            else:
                act = acts[0]
            link_to_process = []
            for i in each_dt.getnext().findall('.//a'):
                link = i.get('href', None)
                tags = i.get('tags', None)
                if link and not link.startswith('javascript') and link.startswith('http'):
                    link_to_process.append((link, i.text or link, tags))
            ret.append((act, link_to_process))
    return ret


def _model_detail(model):
    import django.db.models
    from django.db import connections
    con = connections['default']
    fields = [f for f in model._meta.fields if not isinstance(f, django.db.models.AutoField)]
    table = model._meta.db_table
    column_names = ",".join(con.ops.quote_name(f.column) for f in fields)
    placeholders = ",".join(("%s",) * len(fields))
    return con, fields, table, column_names, placeholders


def insert_into_context(activity, links_to_save, submited_by, reference=''):
    #Insert all Links
    con, fields, table, column_names, placeholders = _model_detail(Link)
    parameters = []
    link_tag_mapping = dict([(i, k)for i,j,k in links_to_save])
    for link, title, tags in links_to_save:
        if len(link)<255:
            lobj = Link(link=link, title=title)
            parameters.append(tuple(f.get_db_prep_save(f.pre_save(lobj, True), connection=con) for f in fields))
    con.cursor().executemany("insert ignore into %s (%s) values (%s);" % (table, column_names, placeholders), parameters)
    all_inserted_link = Link.objects.filter(link__in=[lnk for lnk, tt, tags in links_to_save]).values()
    from collections import defaultdict
    d = defaultdict(lambda:{})
    for elink in all_inserted_link:
        d[elink.get('id')] = {'link': elink.get('link'), 'tags': link_tag_mapping.get(elink.get('link'))}

    #Add all links into Activity
    con, fields, table, column_names, placeholders = _model_detail(ActivityLink)
    parameters = []
    for link_obj_id in d.keys():
        al = ActivityLink(activity=activity, actlink_id=link_obj_id, submited_by=submited_by, submited_on=datetime.now(), reference=reference)
        parameters.append(tuple(f.get_db_prep_save(f.pre_save(al, True), connection=con) for f in fields))
    con.cursor().executemany("insert ignore into %s (%s) values (%s);" % (table, column_names, placeholders), parameters)
    
    #Insert all Tags
    con, fields, table, column_names, placeholders = _model_detail(Tag)
    parameters = []
    all_available_tags = []
    #[j.strip() for i in d.values() if i.get('tags') is not None for j in i.get('tags').split(',')]
    for i in d.values():
        if i.get('tags') is not None:
            print i.get('tags')
            for j in i.get('tags').split(','): all_available_tags.append(j.strip().lower())

    for i in all_available_tags:
        tag = Tag(name=i)
        parameters.append(tuple(f.get_db_prep_save(f.pre_save(tag, True), connection=con) for f in fields))
    con.cursor().executemany("insert ignore into %s (%s) values (%s);" % (table, column_names, placeholders), parameters)
    tag_dict = dict([(i.get('name'), i.get('id')) for i in Tag.objects.filter(name__in=all_available_tags).values()])

    #Add Tags to ActivivityLink
    almap = ActivityLink.objects.filter(activity=activity, actlink__in=d.keys(), submited_by=submited_by).values()
    con, fields, table, column_names, placeholders = _model_detail(TaggedItem)
    parameters = []
    for i in almap:
        temp_tags = d.get(i.get('actlink_id')).get('tags')
        if temp_tags is not None:
            for each_tag in [j.strip().lower() for j in temp_tags.split(',')]:
                tagged_item = TaggedItem(tag_id=tag_dict[each_tag], object_id=i.get('id'), content_type=ContentType.objects.get(app_label='activity', model='activitylink'))
                parameters.append(tuple(f.get_db_prep_save(f.pre_save(tagged_item, True), connection=con) for f in fields))

    con.cursor().executemany("insert ignore into %s (%s) values (%s);" % (table, column_names, placeholders), parameters)
    return True


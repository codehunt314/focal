import oauth2 as oauth
import urllib

consumer_key = "J0trQnc0f1OKdZHqYoCvyWmpgZEHUJAMpVHA6imTnVvjUTR5VU"
consumer_secret = "RtlnKe4O9xVm8oTKPoBk9h6lvEdKY0DoiwUSLy74ymkWuU0KBl"

def get_access_token(username, password):
    consumer = oauth.Consumer(consumer_key, consumer_secret)
    client = oauth.Client(consumer)
    params = {}
    access_token_url = "https://www.instapaper.com/api/1/oauth/access_token"
    params["x_auth_username"] = username
    params["x_auth_password"] = password
    params["x_auth_mode"] = 'client_auth'
    client.set_signature_method = oauth.SignatureMethod_HMAC_SHA1()
    resp, token = client.request(access_token_url, method="POST",body=urllib.urlencode(params))
    return token

def save_bookmark_in_instapaper(access_token, url, title):
    token_param = dict([tuple(i.split('=')) for i in access_token.split('&')])
    token = oauth.Token(token_param['oauth_token'], token_param['oauth_token_secret'])
    consumer = oauth.Consumer(consumer_key, consumer_secret)
    client = oauth.Client(consumer, token)
    url = "https://www.instapaper.com/api/1/bookmarks/add"
    params = {}
    params['url'] = url
    if title:
        params['title'] = title
    client.set_signature_method = oauth.SignatureMethod_HMAC_SHA1()
    r = client.request(url, method="POST", body=urllib.urlencode(params))


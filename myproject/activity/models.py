from djangosphinx.models import SphinxSearch

from django.db import models
from url_encode import encode_url, decode_url
from settings import DOMAIN
from hashlib import sha1
import random
import urllib
from django.utils import simplejson as json
from datetime import datetime, timedelta
from time import gmtime, strftime
from tagging.models import Tag, TaggedItem
from myproject.focaluser.models import *

class LinkManager(models.Manager):
    def get(self, *args, **kwargs):
        if kwargs.get('link', None):
            kwargs['link_hash'] = sha1(kwargs['link']).hexdigest()
            kwargs.pop('link')
        try:
            return super(LinkManager, self).get_query_set().get(*args, **kwargs)
        except self.model.DoesNotExist:
            return None

    def get_or_create_link(self, *args, **kwargs):
        from myproject.my_html2txt import extract_title
        if kwargs['defaults']['title']=='':
            try:
                kwargs['defaults']['title'] = extract_title(kwargs['link']).strip()
            except:
                kwargs['defaults']['title'] = ''
        web_url, crtd = self.get_query_set().get_or_create(*args, **kwargs)
        return web_url, crtd

class Link(models.Model):
    #link = models.URLField(unique=True, max_length=255)
    link = models.TextField()
    link_hash = models.CharField(unique=True, max_length=255)
    title = models.TextField(blank=True)
    body = models.TextField(blank=True)
    meta_tags = models.TextField(blank=True)
    favicon = models.URLField(blank=True, null=True)
    thumb = models.URLField(blank=True, null=True)
    metadesc = models.TextField(blank=True, null=True)
    processed = models.BooleanField(default=False)

    search = SphinxSearch(
                            index='links_index delta',
                        )
    objects = LinkManager()

    def __unicode__(self):
        return '%d---%s'%(self.id, self.link)


class ActivityManager(models.Manager):
    def get(self, *args, **kwargs):
        try:
            return super(ActivityManager, self).get_query_set().get(*args, **kwargs)
        except self.model.DoesNotExist:
            return None

    def create_activity(self, activity_name, user, private=False):
        if not private:
            salt = sha1(str(random.random())).hexdigest()[:5]
            referal_code = sha1(salt + user.username + str(activity_name)).hexdigest()
        else:
            referal_code = None
        act, crtd = self.get_query_set().get_or_create(
                            activityname = activity_name,
                            created_by = user,
                            defaults = {
                                'private' : private,
                                'referal_code': referal_code
                            }
                        )
        if crtd:
            ar = ActivityReferal.objects.get_or_create(activity=act)
            am = ActivityMember.objects.create_activity_member(act, user)
        return act, crtd


class Activity(models.Model):
    activityname = models.CharField(max_length=50)
    created_on = models.DateTimeField(auto_now=False, auto_now_add=True)
    private = models.BooleanField(default=False)
    created_by = models.ForeignKey(ActUserProfile)
    active = models.BooleanField(default=True)
    view_mode = models.CharField(max_length=10, default='none')
    ipaddress = models.IPAddressField(blank=True, null=True)
    referal_code = models.CharField(max_length=500, blank=True, null=True)

    objects = ActivityManager()

    def get_activity_dict(self):
        return {'id': self.id, 'name': self.activityname, 'created_by':self.created_by.username}
    def get_referal_url(self):
        return '%s/referal/?code=%s'%(DOMAIN, self.referal_code)
    def get_activity_key(self):
        return encode_url(self.id)
    def get_activity_id(self, activity_key):
        return decode_url(activity_key)


class ActivityReferal(models.Model):
    activity = models.ForeignKey(Activity)
    view_count = models.PositiveIntegerField(default=0)
    conversion = models.PositiveIntegerField(default=0)
    expired = models.BooleanField(default=False)
    
    def increment_viewcount(self):
        self.view_count += 1
        self.save()

class ActivityLinkManager(models.Manager):
    def get_activities_of_link(self, linkobj):
        return [al.activity for al in self.get_query_set().filter(actlink=linkobj, deleted=False)]

    def get_links_of_activity(self, activity, num=20):
        al = self.get_query_set().filter(activity=activity, deleted=False).values('actlink_id').order_by('submited_on')[:num]
        links = []
        for i in al:
            link = i.get('actlink_id', None)
            if link: links.append(link)
        return Link.objects.filter(id__in=links)

    def sharealink(self, activity, web_url, user):
        shared = False
        al, crtd = self.get_query_set().get_or_create(
                            activity = activity,
                            actlink = web_url,
                            defaults = {
                                'linktitle': web_url.title.strip(),
                                'submited_by': user
                            }
                        )
        if not crtd:
            if al.deleted:
                al.deleted = False
                al.deleted_on = None
                al.submited_on = datetime.now()
                al.save()
        
        shared = True

        if activity.activityname in ['Share on Twitter', 'Read It Later', 'Share on Facebook']:
            ssq = SocialShareQueue.objects.create(user=user, url=web_url, activityname=activity.activityname)
        elif activity.activityname!='Personal':
            param = { 'link': web_url, 'by': user, 'activity': activity, 'submited_on': al.submited_on}
            r = ActivityMember.objects.send_mail_to_activity_member(al, param, 'new_link')
        else:
            pass

        return al, shared

    def can_delete(self, activity, link, user):
        try:
            return self.get_query_set().get(activity=activity, actlink=link, submited_by=user)
        except:
            return False

    def delalink(self, activity, link, user):
        ret = self.can_delete(activity, link, user)
        if ret:
            ret.deleted = True
            ret.deleted_on = datetime.now()
            ret.save()
            return json.dumps({'status':1, 'message':'successfully deleted'})
        else:
            return json.dumps({'status':0,'message':'you can only delete links you have shared'})

    def export(self, user):
        user_activities = ActivityMember.objects.get_user_activity(user)
        from collections import defaultdict
        d = defaultdict(lambda: set())
        for i in self.get_query_set().filter(activity__in=user_activities):
            d[i.activity].add(i.actlink)
        return dict(d)

    def get_link_detail(self, link_obj, user):
        ret = {'status': 'ok', 'message': None, 'activities': [], 'favorite': False, 'tags': [], 'suggested_tags': [], 'tag_set': []}
        ret_val = []
        for ua in ActivityMember.objects.get_user_activity(user):
            if link_obj is None:
                d = {"id": ua.id, "name": ua.activityname, "members":[], "order": 1, "added":False, "can_delete": True, "by": None}
                d["members"] = [m.nickname for m in ActivityMember.objects.get_activity_member(ua)]
                ret_val.append(d)
            else:
                if ua.activityname=='Personal':
                    fav = self.get_query_set().filter(activity=ua, actlink=link_obj, deleted=False)
                    if len(fav)==1:
                        ret['favorite'] = True
                        ret['tags'] = [i.name for i in Tag.objects.get_for_object(fav[0])]
                else:
                    d = {"id": ua.id, "name": ua.activityname, "members":[], "order": 1, "added":False, "can_delete": True, "by": None}
                    d["members"] = [m.nickname for m in ActivityMember.objects.get_activity_member(ua)]
                    r = self.get_query_set().filter(activity=ua, actlink=link_obj, deleted=False)
                    if len(r)==1:
                        d["added"] = True
                        if r[0].submited_by!=user:
                            d["can_delete"] = False
                        d["by"] = r[0].submited_by.nickname
                    ret_val.append(d)
        ret['activities'] = filter(lambda x: x.get('name') not in ['Personal', 'Focal Tips'], ret_val)
        return ret


class ActivityLink(models.Model):
    actlink = models.ForeignKey(Link)
    activity = models.ForeignKey(Activity)
    linktitle = models.CharField(max_length=500, blank=True, null=True)
    submited_by = models.ForeignKey(ActUserProfile)
    submited_on = models.DateTimeField(auto_now=False, auto_now_add=True)
    deleted = models.BooleanField(default=False)
    deleted_on = models.DateTimeField(blank=True, null=True)
    ipaddress = models.IPAddressField(blank=True, null=True)
    note = models.TextField(max_length=1000, blank=True, null=True)
    reference = models.TextField(max_length=1000, blank=True, null=True)

    objects = ActivityLinkManager()

    class Meta:
        unique_together = (("activity", "actlink"),)


#tagging on ActivityLink objects
import tagging
try:
    tagging.register(ActivityLink)
except tagging.AlreadyRegistered:
    pass


def _sort_activity(a, b):
    if a.link_count<b.link_count: return 1
    if a.link_count==b.link_count: return 0
    if a.link_count>b.link_count: return -1

class ActivityMemberManager(models.Manager):
    def get_user_activity(self, user):
        try:
            return user.activities
        except:
            from django.db.models import Count
            d = {}
            user_activity_id = [i['activity_id'] for i in user.activitymember_set.values()] # + [i['id'] for i in user.activity_set.values()]
            activity_dict = Activity.objects.in_bulk(user_activity_id)
            personal_act = None
            for i in activity_dict.values():
                i.__setattr__('link_count', 0)
                i.__setattr__('escore', ActivityMember.objects.get(activity=i, member=user))
                if i.activityname=='Personal':
                    personal_act = i
            for i in ActivityLink.objects.filter(deleted=False).values('activity').annotate(Count('activity')):
                a = activity_dict.get(i['activity'], None)
                if a: a.__setattr__('link_count', i['activity__count'])

            uact = filter(lambda x: x.activityname!='Personal', activity_dict.values())
            uact.sort(cmp=_sort_activity)
            if personal_act: uact.insert(0, personal_act)
            user.__setattr__('activities', uact)
            return uact

    def get_activity_member(self, activity):
        try:
            activity_member_id = [i['member_id'] for i in activity.activitymember_set.values() if i.get('status', None)=='confirm'] #+ [activity.created_by.id]
            user_dict = ActUserProfile.objects.in_bulk(activity_member_id)
            return user_dict.values()
        except:
            return []

    def get_user_friend_network(self, user, from_activity=None):
        if from_activity:
            ua_list = from_activity
        else:
            ua_list = self.get_user_activity(user)
        ua_list = filter(lambda x: x.activityname not in ['Focal Tips'], ua_list)
        friend_id_list = [i.get('member_id') for i in self.get_query_set().filter(activity__in=ua_list).values('member_id')] #+ [i.created_by_id for i in ua_list]
        friend_dict = ActUserProfile.objects.in_bulk(friend_id_list)
        frnd_focaltips = Activity.objects.get(activityname='Focal Tips').created_by
        friend_dict[frnd_focaltips.id] = frnd_focaltips
        return friend_dict

    def create_activity_member(self, activity, member, status='confirm'):
        am, crtd = self.get_query_set().get_or_create(activity=activity, member=member, defaults={'status': status})
        if am.status!=status:
            am.status = 'confirm'
            am.save()
        activity_referal, crtd = ActivityReferal.objects.get_or_create(activity=activity)
        activity_referal.conversion += 1
        activity_referal.save()
        return True

    def send_mail_to_activity_member(self, activitylink, param, ntype='new_link'):
        from django.template.loader import render_to_string
        email_mesg = render_to_string('email_templates/new_link_email_template.html', param)
        to = dict([(member.email, member) for member in self.get_activity_member(activitylink.activity) if member.notification_perm(ntype) and member!=activitylink.submited_by and member.email!=''])
        email_subject = 'New Link posted in %s by %s' % (activitylink.activity.activityname, activitylink.submited_by.nickname)

        from django.contrib.contenttypes.models import ContentType
        ctype = ContentType.objects.get(app_label=activitylink._meta.app_label, model=activitylink._meta.module_name)

        if len(to.keys())>0:
            eq = EmailQueue.objects.create(content_type=ctype, object_pk=activitylink.pk, email_subject=email_subject, message_body=email_mesg, from_email='Focal <help@focal.io>', to_emails=','.join(to.keys()))

        #ntfction_mesg = render_to_string('site_notification_templates/new_link_template.html', param)
        str_c_time = strftime("%d/%b/%Y %H:%M:%S +0000", gmtime())
        ntfction_mesg = json.dumps({
                            'type':'link',
                            'user': {'name': param['by'].nickname, 'id': param['by'].get_user_key() },
                            'link': {'url':  param['link'].link, 'title': param['link'].title, 'id':param['link'].id },
                            'context': {'title': param['activity'].activityname, 'id': param['activity'].get_activity_key()}
                        })
        for to_user in to.values():
            sn = SiteNotification.objects.create(to_user=to_user, ntype='newlink', action=False, mesg=ntfction_mesg)
        return True


class ActivityMember(models.Model):
    activity = models.ForeignKey(Activity)
    member = models.ForeignKey(ActUserProfile)
    escore = models.IntegerField(default=100)
    status = models.CharField(max_length=100, default='confirm')   #other status email-invitation-sent

    objects = ActivityMemberManager()    

    class Meta:
        unique_together = (("activity", "member"),)


class ActivityLinkStatus(models.Model):
    activitylink = models.ForeignKey(ActivityLink)
    activitymember = models.ForeignKey(ActivityMember)
    read = models.BooleanField(default=False)
    staytime = models.IntegerField(default=0)    #in seconds
    clickedlink = models.TextField(blank=True)
    reachedlast = models.FloatField(default=0.0)
    ipaddress = models.IPAddressField(blank=True, null=True)


class Synctable(models.Model):
    for_user = models.ForeignKey(ActUserProfile)
    notification_mesg = models.TextField()
    notification_action_url = models.CharField(max_length=200)
    action_status = models.CharField(max_length=100, default='ready_to_dispatch')
    sync_time = models.DateTimeField(auto_now=False, auto_now_add=True)


class BulkImport(models.Model):
    submited_by = models.ForeignKey(ActUserProfile)
    url_list = models.TextField()
    activity = models.ForeignKey(Activity)
    submited_on = models.DateTimeField(auto_now=False, auto_now_add=True)    
    source = models.CharField(max_length=100)

class SocialShareQueue(models.Model):
    user = models.ForeignKey(ActUserProfile)
    url = models.ForeignKey(Link)
    activityname = models.CharField(max_length=50)


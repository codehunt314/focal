from urlparse import urlsplit
from django import template

register = template.Library()

class DomainNode(template.Node):
    def __init__(self, obj):
        self.obj = obj
    def render(self, context):
        url = template.resolve_variable(self.obj, context)
        context['domain'] = urlsplit(url).hostname
        return ''

def get_domain(parser, token):
    """
        {% domain for url%}
    """
    
    tokens = token.contents.split()
    if len(tokens)!=3:
        raise template.TemplateSyntaxError("%r tag accept two argument" % tokens[0])
    if tokens[1]!='for':
        raise template.TemplateSyntaxError("%r tag: first argument must be 'for'" % tokens[0])
    
    return DomainNode(tokens[2])


def trim_time(t):
    from datetime import datetime
    cur_time = datetime.now()
    td = cur_time - t
    if td.days>0:
        return '%d days'%(td.days)
    else:
        min = td.seconds/60
        sec = td.seconds%60
        if min<60: return '%d min'%(min)
        else: return '%d hrs'%(min/60)

register.tag('domain', get_domain)
register.filter('trim_time', trim_time)

from urlparse import urlsplit
from django import template

from activity.models import *
from focaluser.models import *
from collections import defaultdict
from django.db.models import Count

register = template.Library()

class FriendNode(template.Node):
    def __init__(self, obj):
        self.obj = obj
    def render(self, context):
        user = template.resolve_variable(self.obj, context)
        friends = defaultdict(lambda: 0)
       
        user_activity = ActivityMember.objects.get_user_activity(user)
        user_friends = ActivityMember.objects.get_user_friend_network(user)
        for uf in user_friends.values(): friends[uf] = 0
        for i in ActivityLink.objects.filter(activity__in=filter(lambda x: x not in ['Focal Tips'], user_activity), deleted=False).values('submited_by_id').annotate(count=Count('actlink', distinct=True)):
            friends[user_friends[i.get('submited_by_id')]] = i.get('count')
        context['friends'] = friends.items()
        return ''

def get_friends(parser, token):
    """
        {% friends of user%}
    """
    
    tokens = token.contents.split()
    if len(tokens)!=3:
        raise template.TemplateSyntaxError("%r tag accept two argument" % tokens[0])
    if tokens[1]!='of':
        raise template.TemplateSyntaxError("%r tag: first argument must be 'for'" % tokens[0])
    
    return FriendNode(tokens[2])

def lookup(hash, key):
    return hash[key]


register.tag('friends', get_friends)
register.filter('lookup', lookup)

from urlparse import urlsplit
from django import template

from activity.models import *
from focaluser.models import *
from collections import defaultdict
from django.db.models import Count
from settings import DOMAIN

register = template.Library()


class HasInviteNode(template.Node):
    def __init__(self, user, activity):
        self.user = user
        self.activity = activity
    def render(self, context):
        user = template.resolve_variable(self.user, context)
        activity = template.resolve_variable(self.activity, context)
        
        context['invitation_sent'] = False 
        try:
            am = ActivityMember.objects.get(activity=activity, member=user)
            if am.status!='confirm':
                context['invitation_sent'] = True
        except:
            pass
        return ''

class InviteNode(template.Node):
    def __init__(self, user):
        self.user = user
    def render(self, context):
        user = template.resolve_variable(self.user, context)
        ret = []
        for i in ActivityMember.objects.filter(member=user, status='email-invitation-sent'):
            ret.append({
                'url':'%s/referal/?code=%s'%(DOMAIN, i.activity.referal_code),
                'context':i.activity
            })
        context['pending_invitation'] = ret
        return ''


def has_invitation_sent(parser, token):
    """
        {%has_invitation_sent to user for activity%}
    """
    
    tokens = token.contents.split()
    if len(tokens)!=5:
        raise template.TemplateSyntaxError("%r tag accept four argument" % tokens[0])
    if tokens[1]!='to':
        raise template.TemplateSyntaxError("%r tag: first argument must be 'to'" % tokens[0])
    if tokens[3]!='for':
        raise template.TemplateSyntaxError("%r tag: third argument must be 'for'" % tokens[0])
    
    return HasInviteNode(tokens[2], tokens[4])


def pending_invitation(parser, token):
    """
        {%pending_invitation for user%}
    """

    tokens = token.contents.split()
    if len(tokens)!=3:
        raise template.TemplateSyntaxError("%r tag accept two argument" % tokens[0])
    if tokens[1]!='for':
        raise template.TemplateSyntaxError("%r tag: first argument must be 'for'" % tokens[0])

    return InviteNode(tokens[2])

register.tag('has_invitation_sent', has_invitation_sent)
register.tag('pending_invitation', pending_invitation)



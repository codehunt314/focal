import httplib
from django.conf import settings

import oauth.oauth as oauth


SERVER = getattr(settings, 'OAUTH_SERVER', 'api.twitter.com')
REQUEST_TOKEN_URL =  getattr(settings, 'OAUTH_REQUEST_TOKEN_URL', 'https://%s/oauth/request_token' % SERVER)
ACCESS_TOKEN_URL =  getattr(settings, 'OAUTH_ACCESS_TOKEN_URL', 'https://%s/oauth/access_token' % SERVER)
AUTHORIZATION_URL =  getattr(settings, 'OAUTH_AUTHORIZATION_URL', 'http://%s/oauth/authorize' % SERVER)
CONSUMER_KEY =  getattr(settings, 'CONSUMER_KEY', 'H3jeq7N6rhPUm09cB6svg')
CONSUMER_SECRET =  getattr(settings, 'CONSUMER_SECRET', 'wYFsifIclxGzvwbcSHw3dJreM7fCfJZd2tLEUvV3ck')

#connection = httplib.HTTPSConnection(SERVER)
consumer = oauth.OAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET)
signature_method = oauth.OAuthSignatureMethod_HMAC_SHA1()

STATUS_UPDATE_URL = 'https://api.twitter.com/1/statuses/update.json'
TWITTER_CHECK_AUTH = 'https://twitter.com/account/verify_credentials.json'


def request(url, access_token, parameters=None):
    """
    usage: request( '/url/', your_access_token, parameters=dict() )
    Returns a OAuthRequest object
    """
    oauth_request = oauth.OAuthRequest.from_consumer_and_token(
        consumer, token=access_token, http_url=url, parameters=parameters,
    )
    oauth_request.sign_request(signature_method, consumer, access_token)
    return oauth_request


def fetch_response(oauth_request):
    connection = httplib.HTTPSConnection(SERVER)
    url = oauth_request.to_url()
    connection.request(oauth_request.http_method,url)
    response = connection.getresponse()
    s = response.read()
    return s


def get_unauthorised_request_token(callback=None):
    oauth_request = oauth.OAuthRequest.from_consumer_and_token(consumer, callback=callback, http_url=REQUEST_TOKEN_URL)
    oauth_request.sign_request(signature_method, consumer, None)
    connection = httplib.HTTPSConnection(SERVER)
    resp = fetch_response(oauth_request)
    token = oauth.OAuthToken.from_string(resp)
    return token


def get_authorisation_url(token):
    oauth_request = oauth.OAuthRequest.from_consumer_and_token(consumer, token=token, http_url=AUTHORIZATION_URL)
    oauth_request.sign_request(signature_method, consumer, token)
    return oauth_request.to_url()


def exchange_request_token_for_access_token(request_token):
    oauth_request = oauth.OAuthRequest.from_consumer_and_token(consumer, token=request_token, http_url=ACCESS_TOKEN_URL)
    oauth_request.sign_request(signature_method, consumer, request_token)
    connection = httplib.HTTPSConnection(SERVER)
    resp = fetch_response(oauth_request)
    return oauth.OAuthToken.from_string(resp) 


def is_authenticated(access_token):
    oauth_request = request(TWITTER_CHECK_AUTH, access_token)
    connection = httplib.HTTPSConnection(SERVER)
    json = fetch_response(oauth_request)
    if 'screen_name' in json:
        return json
    return False


def status_update(access_token, status):
    token = oauth.OAuthToken.from_string(access_token)
    oauth_request = oauth.OAuthRequest.from_consumer_and_token(consumer, token=token, http_url=STATUS_UPDATE_URL, http_method='POST', parameters={'status': status})
    oauth_request.sign_request(signature_method, consumer, token)
    connection = httplib.HTTPSConnection(SERVER)
    json = fetch_response(oauth_request)
    return json



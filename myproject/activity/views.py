from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, redirect
from django.template.loader import render_to_string
from django.utils.datastructures import SortedDict
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.utils import simplejson as json
from django.template import RequestContext
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.contrib.comments.models import Comment
from django.contrib.comments.models import Comment
from django.views.decorators.csrf import csrf_exempt
from django.core.validators import email_re
from django.core.paginator import Paginator
from django.core.urlresolvers import reverse
from django.utils.timesince import timesince
from django.contrib.contenttypes.models import ContentType

import os, urllib, cgi, random
from hashlib import sha1
from datetime import datetime, timedelta
from time import time, mktime
from copy import deepcopy
from collections import defaultdict

from tagging.models import TaggedItem
from models import *
from myproject.tcomment.models import *
from myproject.tcomment.views import _get_comment
from myproject.focaluser.models import *

from activity.tw_share import *
from myproject.url_encode import encode_url, decode_url

from myproject.settings import DOMAIN


def _cmp(x,y):
    if x.shared_time<y.shared_time: return 1
    elif x.shared_time==y.shared_time: return 0
    else: return -1

def _result(almap, ret, ua_dict, user):
    ulist = ActivityMember.objects.get_user_friend_network(user, from_activity=ua_dict.values())

    content_type=ContentType.objects.get(app_label='activity', model='activitylink')
    almap_tag_id = defaultdict(lambda: [])
    all_tags = set()
    for i in TaggedItem.objects.filter(object_id__in=[i.get('id') for i in almap], content_type=content_type).values('tag_id', 'object_id'):
        almap_tag_id[i.get('object_id')].append(i.get('tag_id'))
        all_tags.update([i.get('tag_id')])
    tag_id_map = Tag.objects.in_bulk(all_tags)

    #for bulk import feed in home page
    for i in almap:
        actlink = i.get('actlink_id', None)
        activity = ua_dict.get(i.get('activity_id'))
        submit_by = ulist.get(i.get('submited_by_id'))
        setattr(activity, 'can_delete', submit_by==user)
        ret[actlink]['submited_on'].append(i.get('submited_on'))
        #ret[actlink]['linkshared_status'][submit_by].append(activity)   #for user segregated activityname
        ret[actlink]['linkshared_status'].append((submit_by, activity, i.get('submited_on')))
        ret[actlink]['almap_id'].append(i.get('id'))

    return_link_list = []
    comment_map = defaultdict(lambda: set())
    comment_map_value = CommentMap.objects.filter(activitylink__in=[i.get('id') for i in almap]).values()
    comment_id_list = [i.get('comment_id') for i in comment_map_value]
    comment_object_map = {}

    for i in MyComment.objects.filter(pk__in=comment_id_list).values():
        c_dict = {'id':i.get('id'), 'text': i.get('comment_text'), 'by':ulist.get(i.get('user_id')).nickname, 'date':i.get('submit_date'), 'activities':[], 'parent':i.get('parent_id'), 'level':i.get('level')}
        comment_object_map[i.get('id')] = c_dict

    for i in comment_map_value:
        activitylink = i.get('activitylink_id')
        comment_map[activitylink].add(i.get('comment_id'))

    for each_link in Link.objects.filter(id__in=ret.keys()):
        each_link.__setattr__('shared_time', max(ret[each_link.id]['submited_on']))
        #each_link.__setattr__('linkshared_status', ret[each_link.id]['linkshared_status'].items())   #for user segregated activityname
        each_link.__setattr__('linkshared_status', ret[each_link.id]['linkshared_status'])
        cmnt = set()
        for i in ret.get(each_link.id).get('almap_id'): cmnt.update(comment_map[i])
        each_link.__setattr__('comment_count', len(cmnt))
        each_link.__setattr__('comments', [comment_object_map[i] for i in cmnt][:3])
        tags = ''
        for i in ret[each_link.id]['almap_id']:
            if almap_tag_id.get(i) is not None:
                tags = ', '.join([tag_id_map.get(j).name for j in almap_tag_id.get(i)])
        each_link.__setattr__('tags', tags)
        return_link_list.append(each_link)
    return return_link_list

def new_set_cookie(rsp_redirect, user):
    max_age = 180*24*60*60
    expires = datetime.strftime(datetime.utcnow() + timedelta(seconds=max_age), "%a, %d-%b-%Y %H:%M:%S GMT")
    rsp_redirect.set_cookie('username', value=user.username, max_age=max_age, expires=expires, path='/', domain=None, secure=None)
    rsp_redirect.set_cookie('useremail', value=user.email, max_age=max_age, expires=expires, path='/', domain=None, secure=None)
    return rsp_redirect


def is_active(f):
  def wrap(request, *args, **kwargs):
    if request.user.id is not None:
        if not request.user.is_active:
            return HttpResponseRedirect("/activate/")
    return f(request, *args, **kwargs)
  return wrap

@csrf_exempt
def create_activity(request):
    if request.user.id is None:
        return HttpResponseRedirect('/login/')
    if request.method=='POST':
        is_ajax = request.GET.has_key('is_ajax')
        activity_name = request.POST.get('activity', '')

        error = None
        if len(activity_name) == 0:
            error = "Activity can't be blank"
        elif activity_name in ['Personal', 'Focal Tips']:
            error = "Activity with name \'%s\' already exist"%(activity_name)
        else:
            act, crtd = Activity.objects.create_activity(activity_name, request.user)
 
        if error:
            if is_ajax:
                return HttpResponse(json.dumps({'status': 0, 'message': error}))
            else:
                return render_to_response('createactivity.html', {'actuser': request.user, 'error':error}, context_instance=RequestContext(request))
        else:
            if is_ajax:
                return HttpResponse(json.dumps({'status':1,'next':'/#/context/'+encode_url(act.id)+'/manage'}))
            else:
                return HttpResponseRedirect('/#/context/'+encode_url(act.id)+'/manage')

    return render_to_response('createactivity.html', {'actuser': request.user}, context_instance=RequestContext(request))


def activity_home(request, actid):
    if request.user.id is None:
        return HttpResponseRedirect('/login/')

    activity = Activity.objects.get(id=decode_url(actid))
    activity_member = ActivityMember.objects.get_activity_member(activity)
    if request.user not in activity_member:
        return HttpResponse('you can only see the activity you belong to')

    activity_link = ActivityLink.objects.get_links_of_activity(activity)
    if request.GET.get('is_ajax',''):
        return render_to_response('activity_home_ajax.html', {'actuser': request.user, 'activity': activity, 'activity_member': activity_member, 'activity_link': activity_link, 'mesg': False}, context_instance=RequestContext(request))
    else:
        return render_to_response('activity_home.html', {'actuser': request.user, 'activity': activity, 'activity_member': activity_member, 'activity_link': activity_link, 'mesg': False}, context_instance=RequestContext(request))


def activity_referal_page(request):
    if request.method=='POST':
        username = request.POST.get('email', False)
        email = request.POST.get('email', False)
        pass1 = request.POST.get('pass1', False)
        password = request.POST.get('password', None)
        referalcode = request.POST.get('referalcode', None)

        if username and email and pass1 and referalcode:
            first_name = request.POST.get('first_name', '')
            last_name = request.POST.get('last_name', '')
            nickname = request.POST.get('nickname', False)
            
            if not email_re.match(email):
                return HttpResponse(json.dumps({'status': 0, 'message': 'Incorrect e-mail address'}))
            if not nickname:
                nickname = email.split('@')[0]
            activity = Activity.objects.get(referal_code=referalcode)
            error = None
            try:
                new_user = ActUserProfile.objects.register_user(username, email, pass1, nickname, first_name, last_name, referalcode)
                auth_login(request, user)
            except Exception,e:
                error = 'A login already exists for this email-id(%s)'%(email)
            
            if request.GET.get('is_ajax',None) :
                if error:
                    return HttpResponse(json.dumps({'status': 0, 'message': error}))
                else:
                    return HttpResponse(json.dumps({'status': 1, 'next': "/#/context/%s"%(activity.get_activity_key())}))

        if username and password and referalcode:
            user = authenticate(username=username, password=password)
            error = None
            if user is not None:
                if user.is_active:
                    activity = Activity.objects.get(referal_code=referalcode)
                    am = ActivityMember.objects.create_activity_member(activity, user)
                    auth_login(request, user)
                    
                else:
                    error = 'Account is not activated!!'
            else:
                error = 'Authentication Failed!!'

            if request.GET.get('is_ajax',None) :
                if error:
                    return HttpResponse(json.dumps({'status': 0, 'message': error}))
                else:
                    rsp_redirect = HttpResponse(json.dumps({'status': 1, 'next': '/#/context/%s'%(activity.get_activity_key())}))
                    return new_set_cookie(rsp_redirect, user)
        

        if request.user.id and username and referalcode:
            user = ActUserProfile.objects.get(username=username)
            activity = Activity.objects.get(referal_code=referalcode)
            am = ActivityMember.objects.create_activity_member(activity, user)
            redirect_link = '/#/context/%s'%(activity.get_activity_key())
            if request.POST.get('is_ajax','') == 'true':
                return HttpResponse('Successfully joined <a href="%s">%s</a> context.'%(redirect_link, activity.activityname))
            else:
                return HttpResponseRedirect(redirect_link)

        return HttpResponse(json.dumps({'status': 0, 'message': 'All the fields are compulsory'}))

    else:
        referal_code = request.GET.get('code', None)
        email = request.GET.get('email', '')
        if referal_code:
            activity = Activity.objects.get(referal_code=referal_code)
            activity_referal = ActivityReferal.objects.get(activity=activity)
            activity_referal.increment_viewcount()
            if request.user in ActivityMember.objects.get_activity_member(activity):
                return HttpResponseRedirect("/?activity=%s"%(activity.get_activity_key()))
            if activity_referal.expired:
                return HttpResponse('referal code expired')
            else:
                if request.user.id:
                    almap = ActivityLink.objects.filter(activity=activity).order_by('-submited_on').values()
                    #ret = defaultdict(lambda: {'submited_on': [], 'linkshared_status':defaultdict(lambda: []), 'almap_id': []})
                    ret = defaultdict(lambda: {'submited_on': [], 'linkshared_status':[], 'almap_id': []})
                    return render_to_response('join_activity_logged_in.html', {'activity': activity, 'activity_links': _result(almap, ret, {activity.id: activity}, request.user)}, context_instance=RequestContext(request))
                else:
                    return render_to_response('join_activity.html', {'activity': activity, 'email': email, 'activity_links': ActivityLink.objects.get_links_of_activity(activity)}, context_instance=RequestContext(request))
        else:
            return HttpResponse('referal code is invalid')


def get_all_tags(request):
    if request.user.id is None:
        return HttpResponseRedirect('/login/')
    all_tags = [i.name for i in Tag.objects.usage_for_model(ActivityLink, filters=dict(activity__activityname='Personal', submited_by=request.user))]
    return HttpResponse(json.dumps(all_tags))


@csrf_exempt
def tag(request):
    if request.user.id is None:
        return HttpResponseRedirect('/login/')

    user = request.user
    if request.method=='POST':
        link = request.POST.get('url', False)
        if not link:
            return HttpResponse(json.dumps({'status': 0, 'message': 'link is blank'}))
        import pdb; pdb.set_trace()
        tagname = request.POST.get('tag', '')
        if tagname=='':
            return HttpResponse(json.dumps({'status': 0, 'message': 'tag is blank'}))
        try:
            link_obj = Link.objects.get(link=link)
            activity = Activity.objects.get(created_by=user, activityname='Personal')
            al = ActivityLink.objects.get(actlink=link_obj, activity=activity)
            action = request.POST.get('action', None)
            tagname = json.dumps(tagname)
            if action=='create':
                Tag.objects.add_tag(al, tagname)
                return HttpResponse(json.dumps({'status': 1, 'message': 'added'}))
            elif action=='delete':
                ctype = ContentType.objects.get_for_model(al)
                tag_to_remove = Tag.objects.get(name=tagname)
                TaggedItem._default_manager.filter(content_type=ctype, object_id=al.pk, tag=tag_to_remove).delete()
                return HttpResponse(json.dumps({'status': 1, 'message': 'removed'}))
            else:
                return HttpResponse(json.dumps({'status': 0, 'message': 'wrong action'}))
        except Exception, e:
            return HttpResponse(json.dumps({'status': 0, 'message': 'some error ' + str(e)}))

@csrf_exempt
def sharealink(request, act):
    if request.user.id is None:
        return HttpResponseRedirect('/login/')

    user = request.user
    if request.method=='POST':
        link = request.POST.get('link', False)
        if not link:
            return HttpResponse(json.dumps({'status': 0, 'message': 'link is blank'}))
    
        sessionkey = request.GET.get('sessionkey', None)
        if sessionkey:
            from django.contrib.sessions.models import Session
            s = Session.objects.get(pk=sessionkey)
            decoded_data = s.get_decoded()
            user = ActUserProfile.objects.get(id=decoded_data['_auth_user_id'])

        if act=='in_favorite':
            activity = Activity.objects.get(created_by=user, activityname='Personal')
            added = request.POST.get('favorite', 'false')
        elif act=='in_activity':
            context = request.POST.get('id', None)
            if context is None:
                return HttpResponse(json.dumps({'status': 0, 'message': 'no context selected'}))
            activity = Activity.objects.get(id=int(context.strip()))
            added = request.POST.get('added', 'false')
        else:
            return HttpResponse(json.dumps({'status': 0, 'message': 'unknown error'}))

        if added=='true':
            link_hash = sha1(link).hexdigest()
            web_url, created = Link.objects.get_or_create_link(link_hash=link_hash, defaults = {'link': link, 'title': request.POST.get('title', ''), 'favicon': request.POST.get('favicon', ''), 'thumb': request.POST.get('thumb', ''), 'metadesc': request.POST.get('desc', ''), 'processed': False })
            #web_url, created = Link.objects.get_or_create_link(link = link, defaults = { 'title': request.POST.get('title', ''), 'favicon': request.POST.get('favicon', ''),
            #                                                            'thumb': request.POST.get('thumb', ''), 'metadesc': request.POST.get('desc', ''), 'processed': False })
            al, shared = ActivityLink.objects.sharealink(activity, web_url, user)
            return HttpResponse(json.dumps({'status': 1, 'message': 'Shared in context', 'next':'/'}))
        else:
            web_url = Link.objects.get(link=link)
            return HttpResponse(ActivityLink.objects.delalink(activity, web_url, request.user))
    else:
        user_activity = ActivityMember.objects.get_user_activity(user)
        return render_to_response('unused_templates/sharealink.html', {'actuser': user, 'user_activitylist': user_activity}, context_instance=RequestContext(request))


def discuss(request, linkid):
    if request.user.id is None:
        return HttpResponseRedirect('/login/')

    activities = ActivityMember.objects.get_user_activity(request.user)
    link = Link.objects.get(id=int(linkid))
    actlink = [al for al in ActivityLink.objects.filter(actlink=link) if al.activity in activities]
    all_comment = _get_comment(request.user, actlink)
    if request.GET.get('is_ajax','') == 'true':
        return render_to_response('discuss_ajax.html', {'actuser': request.user,'all_comment':all_comment, 'link': link, 'actlink': actlink, 'link_activites':[al.activity for al in actlink], 'activities': activities}, context_instance=RequestContext(request))
    else:
        return render_to_response('discuss.html', {'actuser': request.user,'all_comment':all_comment, 'link': link, 'actlink': actlink, 'link_activites':[al.activity for al in actlink], 'activities': activities}, context_instance=RequestContext(request))


def get_link_detail(request):
    sessionkey = request.GET.get('sessionkey', None)
    link = request.GET.get('link', None)
    ret = {'status': 'ok', 'message': None, 'activities': [], 'favorite': False, 'tags': [], 'suggested_tags': [], 'tag_set': []}

    if sessionkey is None or link is None:
        ret['status'] = 'error'
        ret['message'] = 'wrong data'
        return HttpResponse(json.dumps(ret))
    try:
        from django.contrib.sessions.models import Session
        s = Session.objects.get(pk=sessionkey)
        decoded_data = s.get_decoded()
        user = ActUserProfile.objects.get(id=decoded_data['_auth_user_id'])
    except:
        ret['status'] = 'error'
        ret['message'] = 'Invalid Session Key'
        return HttpResponse(json.dumps(ret))
        
    link_obj = Link.objects.get(link=link)
    return HttpResponse(json.dumps(ActivityLink.objects.get_link_detail(link_obj, user)))


def get_link_page(request):
    link_detail, link_obj, link, title = None, None, None, None
    logged_in = False
    if request.user.id is not None:
        link = request.GET.get('link', None)
        title = request.GET.get('title', None)
        if link is None:
            return HttpResponse('Missing Parameter')
        link_obj = Link.objects.get(link=link)
        link_detail = ActivityLink.objects.get_link_detail(link_obj, request.user)
        logged_in = True
    return render_to_response('link_add_edit.html', {'logged_in':logged_in, 'link_detail':json.dumps(link_detail), 'link':link_obj, 'url':link,'title':title}, context_instance=RequestContext(request))

def get_bookmarklet(request):
    if request.user.id is None:
        return render_to_response('link_detail_bookmarklet.html',{})
    link = request.GET.get('link', None)
    error = ''
    if link is None:
        error = 'Link Missing'
    
    link_obj = Link.objects.get(link=link)
    activities = ActivityLink.objects.get_link_detail(link_obj, request.user)
    return render_to_response('link_detail_bookmarklet.html',{
        'actuser':request.user, 
        'activities':activities, 
        'link_obj':link_obj,
        'link':link,
        'title':request.GET.get('title', None)
    })
    
def _handle_twitter(activityname, req):
    unauthed_token = req.session.get('unauthed_token', None)
    oauth_token = req.GET.get('oauth_token', None)
    oauth_verifier = req.GET.get('oauth_verifier', None)
    denied = req.GET.get('denied', None)
    if unauthed_token and oauth_token and oauth_verifier:
        token = oauth.OAuthToken.from_string(unauthed_token)
        if token.key != oauth_token:
            return HttpResponse('token are not matching, smthng wrng')

        access_token = exchange_request_token_for_access_token(token)
        req.session['access_token'] = access_token.to_string()
        req.user.save_tw_access_token(access_token.to_string())

        del req.session['unauthed_token']
        return render_to_response('twitter_authorized.html', { 'actuser': req.user, 'accept':True, 'service':'Twitter' }, context_instance=RequestContext(req))
    elif unauthed_token and denied:
        return render_to_response('twitter_authorized.html', { 'actuser': req.user, 'accept':False, 'service':'Twitter' }, context_instance=RequestContext(req))
    else:
        callback_url = DOMAIN + '/auth/%s/' % activityname
        token = get_unauthorised_request_token(callback=callback_url)
        auth_url = get_authorisation_url(token)
        response = HttpResponseRedirect(auth_url)
        req.session['unauthed_token'] = token.to_string()
        return response

@csrf_exempt
def social_auth(req, service_name):
    if req.user.id is None:
        return HttpResponseRedirect('/login/')
    
    if service_name == "twitter":
        return _handle_twitter(service_name, req)
    if service_name == "facebook":
        code = req.GET.get('code', None)
        error = req.GET.get('error',None)
        accept = False
        if code:
            req.user.save_fb_access_token(code)
            accept = True
        return render_to_response('twitter_authorized.html', {'actuser': req.user, 'accept':accept, 'service':'Facebook'}, context_instance=RequestContext(req))
    if service_name == "instapaper":
        if req.method=='GET':
            return render_to_response('instapaper_login_page.html', {}, context_instance=RequestContext(request))
        insta_username = req.POST.get('username', None)
        insta_password = req.POST.get('password', None)
        if insta_username is None or insta_password is None:
            return render_to_response('instapaper_login_page.html', {'mesg': 'Username or Password is blank!'}, context_instance=RequestContext(request))
        if req.user.save_instapaper_access_token(insta_username, insta_password):
            return render_to_response('instapaper_login_page.html', {'mesg': 'successfully saved'}, context_instance=RequestContext(request))
        else:
            return render_to_response('instapaper_login_page.html', {'mesg': 'user authentication with instapaper failed'}, context_instance=RequestContext(request))


def import_export(request):
    if request.user.id is None:
        return HttpResponseRedirect('/login/')
    
    return render_to_response('settings/import_export.html', {}, context_instance=RequestContext(request))


@csrf_exempt
def bulk_import(request):
    if request.method=='POST':
        links = request.POST.get('links', None)
        activity = Activity.objects.get(activityname='Personal', created_by=request.user)
        from bulk_import import extract_title_of_url, read_url, insert_into_context
        if links and activity:
            links_to_save = [(el, extract_title_of_url(read_url(el, until='</title>')) or el, None) for el in links.split()]
            r = insert_into_context(activity, links_to_save, request.user, 'bulkimport')
            mesg = 'url sent for processing'
        else:
            mesg = 'wrong data'
    else:
        mesg = 'wrong request type'
    return render_to_response('settings/import_export.html', {'mesg': mesg}, context_instance=RequestContext(request))


@csrf_exempt
def bulk_import_from_file(request):
    if request.method=='POST':
        f_bookmark = request.FILES.get('bookmarks', None)
        activity = Activity.objects.get(activityname='Personal', created_by=request.user)
        if f_bookmark and activity:
            from bulk_import import extract_url, insert_into_context
            for act, ret_val in extract_url(activity, f_bookmark.read(), request.user):
                r = insert_into_context(act, ret_val, request.user, 'bulkimport')
            mesg = 'Booksmarks have been successfully imported to your Personal Context.'
        else:
            mesg = 'Some Error Occurred. Bookmarks have not been imported.'
    else:
        mesg = 'wrong request type'
    return render_to_response('settings/import_export.html', {'mesg': mesg, 'form':'bulk_import' }, context_instance=RequestContext(request))


def export(request):
    response = render_to_response('export_template.html', {'d': ActivityLink.objects.export(request.user)}, context_instance=RequestContext(request))
    response['Content-Type'] = 'application/html'
    response['Content-Disposition'] = 'attachment; filename=focal-bookmarks.html'
    return response

def test_home(request, act=None, u=None):
    user = request.user
    if user.id is None or not user.is_active:
        return render_to_response('home.html',{'invite_code': request.GET.get('invite_code', None)}, context_instance=RequestContext(request))
        #return HttpResponse('not logged in or not active')

    #activity_filter_list =  request.GET.get('activity').split(':') if request.GET.get('activity', None) else None
    #user_filter_list = request.GET.get('user').split(':') if request.GET.get('user', None) else None
    page_num = request.GET.get('page_num', 1)

    ua_dict = dict([(a.id, a)for a in ActivityMember.objects.get_user_activity(user)])
    selected_activity = []
    if act is None:
        ua_list = ua_dict.values()
    else:
        ua_list = filter(lambda x: ua_dict[x].get_activity_key() in [act], ua_dict)
        selected_activity = [ua_dict.get(decode_url(act))]

    selected_users = []
    if u is None:
        if request.META.get("PATH_INFO", None)=='/':
            temp_list = ActivityLink.objects.filter(activity__in=ua_list, deleted=False, reference=None).order_by('-submited_on')
        else:
            temp_list = ActivityLink.objects.filter(activity__in=ua_list, deleted=False).order_by('-submited_on')
    else:
        if request.META.get("PATH_INFO", None)=='/':
            temp_list = ActivityLink.objects.filter(activity__in=ua_list, submited_by__in=[decode_url(k) for k in [u]], deleted=False, reference=None).order_by('-submited_on')
        else:
            temp_list = ActivityLink.objects.filter(activity__in=ua_list, submited_by__in=[decode_url(k) for k in [u]], deleted=False).order_by('-submited_on')
        selected_users = [ActUserProfile.objects.get(id=decode_url(u))]
    
    p = Paginator(temp_list, 25)
    if int(page_num) <= p.num_pages:
        page = p.page(int(page_num))
    else:
        return HttpResponse("page doesn't exist")
    object_list = page.object_list.values()
    obj_submit_time_list = dict([(i.get('submited_on'), True) for i in object_list]).keys()
    """
        bulk import feed (focal home page feed should not get flooded)
    """
    bi_feed_links = []
    if request.META.get("PATH_INFO", None) == '/':
        bulk_imported_links = []
        if len(obj_submit_time_list)>0:
            bulk_imported_links = ActivityLink.objects.filter(activity__in=ua_list, submited_on__gte=min(obj_submit_time_list), submited_on__lt=max(obj_submit_time_list)).exclude(reference=None).order_by('-submited_on').values()
        if len(bulk_imported_links)>0:
            bi_dict = defaultdict(lambda: defaultdict(int))
            for i in bulk_imported_links:
                bi_dict[i.get('submited_on')][ua_dict.get(i.get('activity_id'))] += 1
            bi_list = bi_dict.items()
            bi_list.sort(reverse=True)
            shared_time = bi_list[0][0]
            for each_bi in bi_list:
                for a,c in each_bi[1].items():
                    l = Link(link='/#/context/%s'%(a.get_activity_key()), title='<span>%d</span> Links imported into context <a href="/#/context/%s">%s</a>' %(c, a.get_activity_key(), a.activityname.title()))
                    l.__setattr__('shared_time', shared_time)
                    l.__setattr__('linkshared_status', [])
                    l.__setattr__('comment_count', 0)
                    l.__setattr__('comments', [])
                    l.__setattr__('tags', '')
                    l.__setattr__('special', True)
                    bi_feed_links.append(l)

    almap = ActivityLink.objects.filter(activity__in=ua_dict.values(), actlink__in=[i.get('actlink_id') for i in object_list if i.get('actlink_id', None)], deleted=False).order_by('-submited_on').values()
    #ret = defaultdict(lambda: {'submited_on': [], 'linkshared_status':defaultdict(lambda: []), 'almap_id': []})  #for user segregated activityname
    ret = defaultdict(lambda: {'submited_on': [], 'linkshared_status':[], 'almap_id': []})

    return_link_list = _result(almap, ret, ua_dict, user)
    return_link_list.extend(bi_feed_links)
    return_link_list.sort(cmp=_cmp, reverse=False)
    if request.GET.get('is_ajax',None):
        s = render_to_string('single_entry.html', {
                                        'page':page,
                                        'actuser': user, 
                                        'ret_list': return_link_list, 
                            }, context_instance=RequestContext(request))

        p = { 'items':    page.paginator.count, 'has_next': str(page.has_next()), 'number':   page.number }
        return HttpResponse(json.dumps({
            "page":p, 
            "html":s, 
            'context':  [{'name':c.activityname, 'key':c.get_activity_key()} for c in selected_activity],
            'user':     [{'name':u.nickname, 'key':u.get_user_key()} for u in selected_users]
        }))
    elif request.GET.get('full_page',None):
        s = render_to_string('user_home_ajax_html.html', {
                                              'page':page,
                                              'actuser': user,
                                              'selected_activity' : selected_activity,
                                              'selected_users': selected_users,
                                              'ret_list': return_link_list,
                            }, context_instance=RequestContext(request)) 
        p = { 'items':    page.paginator.count, 'has_next': str(page.has_next()), 'number':   page.number }
        return HttpResponse(json.dumps({
            "page":p, 
            "html":s, 
            'context':  [{'name':c.activityname, 'key':c.get_activity_key()} for c in selected_activity],
            'user':     [{'name':u.nickname, 'key':u.get_user_key()} for u in selected_users]
        }))
    else:
        return render_to_response('user_home.html', {
                                              'page':page,
                                              'actuser': user,
                                              'selected_activity' : selected_activity,
                                              'selected_users': selected_users,
                                              'ret_list': return_link_list,
                            }, context_instance=RequestContext(request)) 


def redirect_link(request):
    if request.GET.get('url', None):
        return HttpResponseRedirect(request.GET.get('url'))
    return HttpResponse('wrong url')


def test_linkdetail(request):
    link = request.GET.get('link', None)
    user = request.user
    ret = {'status': 'ok', 'message': None, 'activities': [], 'favorite': False, 'tags': [], 'suggested_tags': [], 'tag_set': []}
    link_obj = Link.objects.get(link=link)
    ret = json.dumps(ActivityLink.objects.get_link_detail(link_obj, user))
    return render_to_response('test_linkdetail.html', {'ret': ret}, context_instance=RequestContext(request))

@csrf_exempt
def activity_friend_invite(request):
    user = request.user
    if user.id is None:
        return HttpResponseRedirect('/')

    if request.method=='POST':
        activity_key = request.POST.get('activity_key', None)
        friend_key = request.POST.get('friend_key', None)
        if not friend_key and not activity_key:
            return HttpResponse('wrong data')
        activity = Activity.objects.get(id=int(decode_url(activity_key)))
        friend = ActUserProfile.objects.get(id=int(decode_url(friend_key)))
        if friend not in ActivityMember.objects.get_user_friend_network(user).values():
            return HttpResponse('user is not in your context-network')
        if activity.created_by!=user:
            return HttpResponse('unauthorized request')
    
        ret = ActivityMember.objects.create_activity_member(activity, friend, 'email-invitation-sent')
        #email sending section
        email_subject = '%s %s invited you to join %s' %(user.first_name, user.last_name, activity.activityname)
        email_mesg = render_to_string('activity_invite_email.html', {'activity': activity, 'sent_by': user, 'invite_link': '%s/referal/?code=%s'%(DOMAIN, activity.referal_code)})
        eq = EmailQueue.objects.create(content_type=None, object_pk=None, email_subject=email_subject, message_body=email_mesg, from_email='Focal <help@focal.io>', to_emails=friend.email)
        return HttpResponse('Invitation Sent')
    else:
        return HttpResponse('Incorrect Request')


def reject_invitation(request):
    user = request.user
    if user.id is None:
        return HttpResponseRedirect('/')

    if request.method=='POST':
        activity_key = request.POST.get('activity_key', None)
        if not activity_key:
            return HttpResponse('wrong data')
        activity = Activity.objects.get(id=int(decode_url(activity_key)))
        try:
            am = ActivityMember.objects.get(member=user, activity=activity, status='email-invitation-sent')
            am.delete()
            return HttpResponse('You have successfully rejected invitaion to join context.')
        except:
            return HttpResponse('This is a invalid invitation request.')
    else:
        return HttpResponse('Incorrect Request Format')



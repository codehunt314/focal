from django_assets import Bundle, register
js = Bundle('../media/js/mootools.js', 
            filters='jsmin', output='gen/packed.js')
register('js_all', js)
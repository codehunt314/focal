from django.db import models
from myproject.activity.models import ActivityLink, ActivityMember
from myproject.focaluser.models import ActUserProfile
from collections import defaultdict

class Badge(models.Model):
    name = models.CharField(max_length=50, choices=(('Well Written', 'Well Written'), ('Very Informative', 'Very Informative'), ('Interesting', 'Interesting')))
    icon = models.URLField()


{'badge': [{'badge_name':badge_name, 'badge_icon': badge_icon, 'badge_by': badge_by}, {}, {}]}

class BadgeActivityLinkManager(models.Manager):
	def get_badges_for_link(self, actlink_obj):
		d = defaultdict(list)
		for i in self.get_query_set().filter(assign_to=actlink_obj):
			d[i.badge.name].append({'badge_name': i.badge.name, 'badge_icon': i.badge.icon, 'badge_by': i.assigned_by})
		return d

	def assign_badge(self, badge_name, link_id, actiivty_id):
		self.get_query_set().create()

class BadgeActivityLink(models.Model):
    badge = models.ForeignKey(Badge)
    assign_to = models.ForeignKey(ActivityLink)
	assigned_by = models.ForeignKey(ActUserProfile)

	objects = BadgeActivityLinkManager()


class ActivityMemberBadge(models.Model):
	badge = models.ForeignKey(Badge)
	activity_member = models.ForeignKey(ActivityMember)
	assigned_by = models.ForeignKey(ActUserProfile)


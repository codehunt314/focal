"""
for chrome/ff
"""
import os, time, sys
PROJECT_PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.append(PROJECT_PATH + "/../")
os.environ['DJANGO_SETTINGS_MODULE'] = 'myproject.settings'

from myproject.activity.models import *

from lxml import html
from my_html2txt import extract_text


def url_from_bookmark_html(activity, text_str, submited_by):
    def f(root):
        for chld in root.getchildren():
            if isinstance(chld, html.HtmlComment):
                root.remove(chld)
            elif chld.tag in ['script', 'style']:
                root.remove(chld)
            else:
                f(chld)
    tree = html.fromstring(text_str)
    f(tree)
    ret = []
    all_links = tree.findall('.//a')
    dt = filter(lambda x: x.get('from', None)=="focaluser", tree.findall('.//dt'))
    if len(dt)==0:
        alink = tree.findall('.//a')
        for i in alink:
            link = i.get('href', None)
            if link and not link.startswith('javascript') and link.startswith('http'):
                ret.append((activity, submited_by, link))
    else:
        for each_dt in dt:
            activityname = each_dt[0].text_content().strip()
            acts = filter(lambda x:x.activityname==activityname, ActivityMember.objects.get_user_activity(submited_by))
            if len(acts)==0:
                activity, crtd = Activity.objects.get_or_create(activityname=activityname, created_by=submited_by)
            else:
                activity = acts[0]
            for i in each_dt.getnext().findall('.//a'):
                link = i.get('href', None)
                if link and not link.startswith('javascript') and link.startswith('http'):
                    ret.append((activity, submited_by, link))
    return ret


def _save_urls(url_list):
    import django.db.models
    from django.db import connections
    con = connections['default']

    fields = [f for f in ActivityLink._meta.fields if not isinstance(f, django.db.models.AutoField)]
    table = ActivityLink._meta.db_table
    column_names = ",".join(con.ops.quote_name(f.column) for f in fields)
    placeholders = ",".join(("%s",) * len(fields))

    parameters = []

    for activity, submited_by, url in url_list:
        web_url, created = Link.objects.get_or_create(link=url)
        if not web_url.processed:
            try:
                data = extract_text(web_url.link)
                web_url.title = data['title']
                web_url.body = data['data']
                web_url.processed = True
                web_url.save()
            except Exception, e:
                pass
        al = ActivityLink(activity=activity, actlink=web_url, linktitle=web_url.title, submited_by=submited_by)
        parameters.append(tuple(f.get_db_prep_save(f.pre_save(al, True), connection=con) for f in fields))

    con.cursor().executemany("insert ignore into %s (%s) values (%s);" % (table, column_names, placeholders), parameters)
    return True

def _process_import(each_import):
    url_list = None
    if each_import.source == 'url_list_from_text':
        url_list = []
        for i in each_import.url_list.split():
            url_list.append((each_import.activity, each_import.submited_by, i.strip()))
    elif each_import.source in ['ff_html', 'chrome_html']:
        url_list = url_from_bookmark_html(each_import.activity, each_import.url_list, each_import.submited_by)
    elif each_import.source in 'ff_json':
        url_list = []
    else:
        url_list = []

    _save_urls(url_list)


while True:
    print 'processing imports:'
    for each_import in BulkImport.objects.all():
        _process_import(each_import)
        each_import.delete()

    print 'finished current job, now going for sleep for 90 sec'
    time.sleep(90)


#for firefox (from json)

"""
from simplejson import loads

def url_from_ff_json(url):
  def _parse_json_ff(jobj):
    if jobj.get('children', None):
      for ec in jobj.get('children'):
        _parse_json_ff(ec)
    else:
      uri = jobj.get('uri', None)
      if jobj.get('type', None)=='text/x-moz-place' and uri and uri.startswith('http'):
        ret.append((uri, jobj.get('title1', '')))
  json_data = loads(urlopen(url).read())
  ret = []
  _parse_json_ff(json_data)
  return ret

data1 = url_from_ff_json('file:///home/vips/bookmarks-2011-05-08.json')
"""
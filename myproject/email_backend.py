from django.conf import settings
from django.contrib.auth.models import User
from activity.models import ActUserProfile

class EmailOrUsernameModelBackend(object):
    supports_object_permissions = False
    supports_anonymous_user = False
    supports_inactive_user = False

    def authenticate(self, username=None, password=None):
        if '@' in username:
            kwargs = {'email': username}
        else:
            kwargs = {'username': username}
        try:
            user = ActUserProfile.objects.get(**kwargs)
            if user.check_password(password):
                return user
        except User.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return ActUserProfile.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


"""
for chrome/ff
"""
import os, time, sys
PROJECT_PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.append(PROJECT_PATH + "/../")
os.environ['DJANGO_SETTINGS_MODULE'] = 'myproject.settings'

from myproject.focaluser.models import EmailQueue
from django.core.mail import EmailMultiAlternatives

while True:
    for each_email in EmailQueue.objects.filter(delivered=False):
        try:
            msg = EmailMultiAlternatives(each_email.email_subject, each_email.message_body, each_email.from_email, each_email.to_emails.split(','))
            msg.attach_alternative(each_email.message_body, "text/html")
            msg.send()
            each_email.delete()
        except:
            each_email.delivered = True
            each_email.save()
    print 'finshed this batch will wait for 90 sec before sending next batch'
    time.sleep(90)



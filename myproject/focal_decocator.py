from django.http import HttpResponseRedirect
def is_active(f):
  def wrap(request, *args, **kwargs):
    if request.user.id is not None:
      if not request.user.is_active:
        return HttpResponseRedirect("/activate/")
    return f(request, *args, **kwargs)
  return wrap


from django.db import models
from django.contrib.auth.models import User, UserManager
from settings import DOMAIN
from hashlib import sha1
import random
import urllib
from datetime import datetime
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.template.loader import render_to_string
from myproject.activity.instapaper_share import *
from url_encode import encode_url, decode_url

class UserProfileManager(UserManager):
    def send_registration_email(self, to, mesg):
        from django.core.mail import EmailMultiAlternatives
        email_subject = 'Your new Focal account confirmation'
        eq = EmailQueue.objects.create(content_type=None, object_pk=None, email_subject=email_subject, message_body=mesg, from_email='Focal <help@focal.io>', to_emails=','.join(to))
        return True

    def register_user(self, username, email, password, nickname=None, first_name='', last_name='', invite_req=None):
        new_user = super(UserProfileManager, self).create_user(username, email, password)
        new_user.nickname = nickname
        new_user.first_name = first_name
        new_user.last_name = last_name

        if invite_req is None:
            salt = sha1(str(random.random())).hexdigest()[:5]
            activation_code = sha1(salt+new_user.username).hexdigest()
            new_user.is_active = False
            new_user.activation_code = activation_code
            new_user.save()
            mesg = 'click on link to confirm your mailid %s/confirm?act_code=%s&email=%s'%(DOMAIN, new_user.activation_code, new_user.email)
            self.send_registration_email([new_user.email], mesg)
        else:
            new_user.is_active = True
            new_user.save()
            new_user.create_default_notification_setting()
            if not isinstance(invite_req, unicode):
                invite_req.registered = True
                invite_req.registered_date = datetime.now()
                invite_req.save()
            from myproject.activity.models import Activity, ActivityMember
            a = Activity.objects.create_activity('Personal', new_user, private=True)
            act_focaltips = Activity.objects.get(activityname='Focal Tips')
            am = ActivityMember.objects.create_activity_member(act_focaltips, new_user)
        welcome_mesg = render_to_string('welcome_email.html', {'new_user': new_user})
        eq = EmailQueue.objects.create(content_type=None, object_pk=None, email_subject="Welcome to Focal", message_body=welcome_mesg, from_email='Focal <help@focal.io>', to_emails=new_user.email)
        return new_user


class ActUserProfile(User):
    nickname = models.CharField(max_length=500, blank=True)
    url = models.URLField("Website", blank=True)
    profilephoto = models.URLField("profile pic", blank=True)
    activation_code = models.CharField(max_length=40)
    twitter_access_token = models.CharField(max_length=500)
    facebook_access_token = models.CharField(max_length=500)
    instapaper_access_token = models.CharField(max_length=500)

    objects = UserProfileManager()

    class Meta:
        db_table = 'activity_actuserprofile'

    def get_user_key(self):
        return encode_url(self.id)

    def get_user_id(self, user_key):
        return decode_url(user_key)

    def create_default_notification_setting(self):
        for ntype in NotificationType.objects.all():
            if ntype.label=='summary':
                ns = NotificationSetting.objects.create(user=self, notification_type=ntype, permission=True, freq='twice_a_week')
            else:
                ns = NotificationSetting.objects.create(user=self, notification_type=ntype, permission=True, freq='now')
        return True

    def confirm_registration(self):
        self.is_active = True
        self.save()
        from myproject.activity.models import Activity
        a = Activity.objects.create_activity('Personal', self, private=True)
        self.create_default_notification_setting()
        return True

    def send_account_activation_email(self, to_email):
        from django.core.mail import EmailMultiAlternatives
        email_subject = 'Your new Focal account confirmation'
        mesg = 'click on link to confirm your mailid %s/confirm?act_code=%s&email=%s'%(DOMAIN, self.activation_code, self.email)
        eq = EmailQueue.objects.create(content_type=None, object_pk=None, email_subject=email_subject, message_body=mesg, from_email='Focal <help@focal.io>', to_emails=self.email)
        return True

    def send_forgot_password_mail(self, mesg):
        from django.core.mail import EmailMultiAlternatives
        email_subject = 'Your new Focal account Password Change'
        eq = EmailQueue.objects.create(content_type=None, object_pk=None, email_subject=email_subject, message_body=mesg, from_email='Focal <help@focal.io>', to_emails=self.email)
        return True

    def change_nickname(self, nickname):
        self.nickname = nickname
        self.save()
        return True

    def change_name(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name
        self.save()
        return True

    def change_password(self, password):
        self.set_password(password)
        self.save()
        return True

    def save_fb_access_token(self, code):
        import cgi
        from myproject.activity.fb_share import FACEBOOK_APP_SECRET, FACEBOOK_APP_ID
        args = dict(client_id=FACEBOOK_APP_ID, redirect_uri='%s/auth/facebook/'%(DOMAIN), client_secret=FACEBOOK_APP_SECRET, code=code)
        response = cgi.parse_qs(urllib.urlopen("https://graph.facebook.com/oauth/access_token?" + urllib.urlencode(args)).read())
        access_token = response["access_token"][-1]
        self.facebook_access_token = access_token
        self.save()
        self.set_profile_photo()
        from myproject.activity.models import Activity
        a = Activity.objects.create_activity('Share on Facebook', self, private=False)
        return True

    def save_tw_access_token(self, twitter_access_token):
        self.twitter_access_token = twitter_access_token
        self.save()
        from myproject.activity.models import Activity
        a = Activity.objects.create_activity('Share on Twitter', self, private=False)
        return True

    def save_instapaper_access_token(self, username, password):
        from myproject.activity.instapaper_share import get_access_token
        instapaper_access_token = get_access_token(username, password)
        if instapaper_access_token=='Invalid xAuth credentials.':
            return False
        self.instapaper_access_token = instapaper_access_token
        self.save()
        from myproject.activity.models import Activity
        a = Activity.objects.create_activity('Read It Later', self, private=False)
        return True

    def notification_perm(self, ntype):
        nt = NotificationType.objects.get(label=ntype)
        ns = NotificationSetting.objects.get(user=self, notification_type=nt)
        return ns.permission

    def set_profile_photo(self):
        if self.facebook_access_token:
            try:
                from django.utils import simplejson as json
                args = dict(access_token=self.facebook_access_token, fields='picture')
                self.profilephoto = json.loads(urllib.urlopen("https://graph.facebook.com/me?" + urllib.urlencode(args)).read())["picture"]
                self.save()
            except:
                pass
        return True
FREQ = (('none', 'none'),('daily', 'daily'),('weekly', 'weekly'), ('now', 'now'))

class NotificationType(models.Model):
    label = models.CharField(max_length=50, unique=True)
    desc = models.TextField(blank=True, null=True)
    template = models.TextField(blank=True, null=True)   #template

    #class Meta:
    #    db_table = 'notification_notificationtype'


class NotificationSetting(models.Model):
    user = models.ForeignKey(ActUserProfile)
    notification_type = models.ForeignKey(NotificationType)
    permission = models.BooleanField(default=True)
    freq = models.CharField(max_length=50, choices=FREQ, default='now')

    class Meta:
        unique_together = ("user", "notification_type")
        #db_table = 'notification_notificationsetting'

class EmailQueue(models.Model):
    content_type   = models.ForeignKey(ContentType, null=True, blank=True, default=None)
    object_pk      = models.IntegerField(null=True, blank=True, default=0)
    content_object = generic.GenericForeignKey(ct_field="content_type", fk_field="object_pk")
    email_subject  = models.TextField()
    message_body   = models.TextField()
    from_email     = models.TextField(default='Focal <help@focal.io>')
    to_emails      = models.TextField()
    delivered      = models.BooleanField(default=False)


class SiteNotification(models.Model):
    to_user = models.ForeignKey(ActUserProfile)
    ntype = models.CharField(max_length=50, choices=(('newlink', 'newlink'),('newcomment', 'newcomment'),('invitation', 'invitation')))
    action = models.BooleanField(default=False)
    mesg = models.TextField()
    creation_time = models.DateTimeField(auto_now=False, auto_now_add=True)


class InviteQueue(models.Model):
    email_id = models.EmailField(blank=True, null=True)
    launch_invite_code = models.CharField(max_length=255)
    registered = models.BooleanField(default=False)
    add_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    send_invite_date = models.DateTimeField(blank=True, null=True)
    registered_date = models.DateTimeField(blank=True, null=True)


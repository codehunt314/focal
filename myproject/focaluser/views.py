from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, redirect
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.utils import simplejson as json
from django.template import RequestContext
from django.template.loader import render_to_string
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.views.decorators.csrf import csrf_exempt
from django.core.validators import email_re
from settings import DOMAIN

from datetime import datetime, timedelta
from time import time, mktime
from hashlib import sha1
import random

from myproject.focaluser.models import *
from focal_decocator import is_active
from myproject.tcomment.views import _get_comment
from myproject.activity.models import *

def about(request):
    if request.user.id is None:
        return HttpResponseRedirect('/login/')
    return render_to_response('about.html', {}, context_instance=RequestContext(request))

def new_set_cookie(rsp_redirect, user):
    max_age = 180*24*60*60
    expires = datetime.strftime(datetime.utcnow() + timedelta(seconds=max_age), "%a, %d-%b-%Y %H:%M:%S GMT")
    rsp_redirect.set_cookie('username', value=user.username, max_age=max_age, expires=expires, path='/', domain=None, secure=None)
    rsp_redirect.set_cookie('useremail', value=user.email, max_age=max_age, expires=expires, path='/', domain=None, secure=None)
    return rsp_redirect


def activate(request):
    if request.method == 'GET':
        return render_to_response('activate.html', {}, context_instance=RequestContext(request))
    else:
        email = request.POST.get('email', None)
        try:
            u = ActUserProfile.objects.get(email=email)
        except:
            u = None
        u.send_account_activation_email(email)
        return HttpResponse(json.dumps({'status':1,'message':'Email Sent'}))


def register(request):
    if request.user.id:
        return HttpResponseRedirect('/')

    if request.method=='POST':
        username = request.POST.get('email', False)
        email = request.POST.get('email', False)
        pass1 = request.POST.get('pass1', False)
        nickname = request.POST.get('nickname', False)
        first_name = request.POST.get('first_name', '')
        last_name = request.POST.get('last_name', '')
        invite_code = request.POST.get('invite_code', None)
        invite_req = None
        if invite_code:
            try:
                invite_req = InviteQueue.objects.get(launch_invite_code=invite_code)
            except:
                invite_req = None

        error = None
        if not email_re.match(email):
            error = 'Email address seems incorrect'
        if not nickname:
            nickname = email.split('@')[0]

        try:
            new_user = ActUserProfile.objects.register_user(username, email, pass1, nickname, first_name, last_name, invite_req)
        except:
            error = 'A login already exists for this email-id'

        if error:
            if request.GET.get('is_ajax',None) :
                return HttpResponse(json.dumps({'status': 0, 'message': error}))
            return render_to_response('register.html', {'error': error, 'invite_req': invite_req}, context_instance=RequestContext(request))
        
        user = authenticate(username=email, password=pass1)
        user.backend = 'myproject.email_backend.EmailOrUsernameModelBackend'
        auth_login(request, user)

        if request.GET.get('is_ajax', None) :
            if not new_user.is_active:
                return HttpResponse(json.dumps({'status':1,'next':'/activate/'}))
        if not new_user.is_active:
            return HttpResponseRedirect('/activate/')
        return HttpResponseRedirect('/?tour=true')
    else:
        invite_code = request.GET.get('invite_code', None)
        invite_req = None
        if invite_code:
            try:
                invite_req = InviteQueue.objects.get(launch_invite_code=invite_code)
            except:
                invite_req = None
            if invite_req and invite_req.registered is False:
                return render_to_response('register.html', {'invite_req': invite_req}, context_instance=RequestContext(request))
            else:
                return HttpResponse('invalid invitation code')
        else:
            return HttpResponseRedirect('/')


def confirm_email(request):
    act_code = request.GET.get('act_code', None)
    email = request.GET.get('email', None)
    referal_code = request.GET.get('referal_code', None)

    if act_code is None:
        return HttpResponse('invalid activation code!! please try to register again or mail to help@focal.io with subject line \'Registration: Activation Fail\'')

    actprofile = ActUserProfile.objects.get(email=email)
    if actprofile and actprofile.activation_code==act_code:
        r = actprofile.confirm_registration()

        actprofile.backend = 'myproject.email_backend.EmailOrUsernameModelBackend'
        auth_login(request, actprofile)

        if referal_code:
            activity = Activity.objects.get(referal_code=referal_code)
            am, crtd = ActivityMember.objects.create_activity_member(activity, actprofile.user)
            rsp_redirect = HttpResponseRedirect('/activity/%s/'%(activity.get_activity_key()))
        else:
            rsp_redirect = HttpResponseRedirect('/?tour=%s'%('true'))
        return new_set_cookie(rsp_redirect, actprofile)
    else:
        return HttpResponse('invalid activation code!! please try to register again, or mail to help@focal.io with subject line \'Registration: Activation Fail\'')


def auth(request):
    if request.method == "POST":
        user = authenticate(username=request.POST['email'], password=request.POST['password'])
        if user is not None:
            user.backend = 'myproject.email_backend.EmailOrUsernameModelBackend'
            auth_login(request, user)
            ret = {'status':1,'next':'/login/'}
        else:
            ret = {'status':0,'message':'Authentication Error !!'}
    else:
        ret = {'status':0, 'message':'Must be a post request'}
    return HttpResponse(json.dumps(ret))


@is_active
def login(request):
    if request.method == 'POST':
        user = authenticate(username=request.POST['email'], password=request.POST['password'])
        if user is not None:
            if user.is_active:
                user.backend = 'myproject.email_backend.EmailOrUsernameModelBackend'
                auth_login(request, user)
                if request.POST.get('next', False):
                    rsp_redirect =  HttpResponseRedirect(request.POST['next'])
                else:
                    #rsp_redirect = HttpResponseRedirect('/?client_session=%s'%(request.session.session_key))
                    rsp_redirect = HttpResponseRedirect('/') # removed session key from url
                return new_set_cookie(rsp_redirect, user)
            else:
                return HttpResponseRedirect('/activate/')
        else:
            msg = 'Authentication Failed!!'
            return render_to_response('login.html', {'message':msg}, context_instance=RequestContext(request))
    else:
        if request.user.id:
            rsp_redirect = HttpResponseRedirect('/')
            return new_set_cookie(rsp_redirect, request.user)
        else:
            return render_to_response('login.html', {}, context_instance=RequestContext(request))


def logout(request):
    response = HttpResponseRedirect('/')
    response.delete_cookie('useremail')
    response.delete_cookie('username')
    auth_logout(request)
    return response


def forgot_password(request):
    mesg = None
    error = None
    if request.method=='POST':
        email = request.POST.get('email', None)
        if email:
            try:
                actuser = ActUserProfile.objects.get(email=email)
                random_password = ActUserProfile.objects.make_random_password()
                actuser.change_password(random_password)
                link = '%s/change_password/'%(DOMAIN)
                mesg = render_to_string('email_send_mesg.html', { 'link': link, 'new_password': random_password})
                actuser.send_forgot_password_mail(mesg)
            except ActUserProfile.DoesNotExist:
                error = "No user with this emailid is registered. Please enter valid emailid"
        else:
            error = 'Email id doesn\'t seem valid'
    return render_to_response('forgotpassword.html', {'mesg': mesg, 'error':error}, context_instance=RequestContext(request))


@csrf_exempt
def change_pasword(request):
    if request.user.id is None:
        return HttpResponseRedirect('/login/')

    mesg = None
    if request.method=='POST':
        pass1 = request.POST.get('pass1', None)
        pass2 = request.POST.get('pass2', None)
        if pass1==pass2:
            u = request.user.change_password(pass1)
            mesg = 'password changed, we will also send email regarding this for future reminder'
        else:
            mesg = 'password didn\'t match'
    else:
        mesg = 'wrong request'
    return redirect('user_settings')

def user_settings(request):
    if request.user.id is None:
        return HttpResponseRedirect('/login/')
    
    return render_to_response('settings/user_settings.html', {}, context_instance=RequestContext(request))
    

def account_settings(request):
    if request.user.id is None:
        return HttpResponseRedirect('/login/')
    
    return render_to_response('settings/account_settings.html', {}, context_instance=RequestContext(request))

def browser_plugins(request):
    if request.user.id is None:
        return HttpResponseRedirect('/login')
    return render_to_response('settings/browser_plugins.html', {}, context_instance=RequestContext(request))

@csrf_exempt
def change_nickname(request):
    if request.user.id is None:
        return HttpResponseRedirect('/login/')

    mesg = None
    if request.method=='POST':
        nickname = request.POST.get('nickname', None)
        if nickname:
            request.user.change_nickname(nickname)
            mesg = 'Nickname changed'
        else:
            mesg = 'Incorrect data'
    else:
        mesg = 'wrong request type'
    return redirect('user_settings')

@csrf_exempt
def change_name(request):
    if request.user.id is None:
        return HttpResponseRedirect('/login/')

    mesg = None
    if request.method=='POST':
        first_name = request.POST.get('first_name', '')
        last_name = request.POST.get('last_name', '')
        request.user.change_name(first_name, last_name)
        if first_name or last_name:
            request.user.change_name(first_name, last_name)
            mesg = 'Name changed'
        else:
            mesg = 'Incorrect data'
    else:
        mesg = 'wrong request type'
    return redirect('user_settings')

@csrf_exempt
def notification_settings(request):
    if request.user.id is None:
        return HttpResponseRedirect('/login/')
    mesg = None
    l = ["summary", "new_link", "new_comment"]
    if request.method=='POST':
        for var in l:
            ntype = NotificationType.objects.get(label=var)
            if var=='summary':
                if request.POST.get(var, '') in ["daily", "twice_a_week", "weekly", "no_summary"]:
                    permission = True
                    freq = request.POST.get(var, '')
                else:
                    permission = False
                    freq = ''
            else:
                freq = 'now'
                if request.POST.get(var, False)=='on':
                    permission = True
                else:
                    permission = False
            ns,crtd = NotificationSetting.objects.get_or_create(user=request.user, notification_type=ntype, defaults = {'permission': permission, 'freq': freq})
            if not crtd:
                ns.permission = permission
                ns.freq = freq
                ns.save()
        mesg = 'settings saved'

    noti_setting_dict = dict([(i.notification_type.label, i)for i in NotificationSetting.objects.filter(user=request.user)])
    return render_to_response('settings/notification_settings.html', {'mesg': mesg, 'noti_setting_dict': noti_setting_dict}, context_instance=RequestContext(request))


def site_notification(request):
    if request.user.id is None:
        return HttpResponseRedirect('/login/')
    notifiction_list = SiteNotification.objects.filter(action=False, to_user=request.user).order_by('-creation_time')
    notifications = []
    for noti in notifiction_list:
        noti.data = json.loads(noti.mesg)
        '''
        if noti.ntype=='newlink':
            activities = ActivityMember.objects.get_user_activity(request.user)
            link = Link.objects.get(id=noti.data['link']['id'])
            actlink = [al for al in ActivityLink.objects.filter(actlink=link) if al.activity in activities]
            noti.__setattr__('comments', _get_comment(request.user, actlink))
        '''
        notifications.append(noti)
    return render_to_response('notification_list.html', {'notifiction_list': notifications}, context_instance=RequestContext(request))

def sitenotification_ack(request, notification_id):
    sn = SiteNotification.objects.get(id=int(notification_id))
    sn.action = True
    sn.save()
    return HttpResponse('Done')


def home(request):
    return render_to_response('home.html',{})

@csrf_exempt
def request_for_invite(request):
    if request.user.id:
        return HttpResponseRedirect('/')

    if request.method=='POST':
        if 'email' in request.POST:
            email = request.POST.get('email').strip()
            salt = sha1(str(random.random())).hexdigest()
            invite_code = sha1(salt+email).hexdigest()
            iq, crtd = InviteQueue.objects.get_or_create(email_id=email, defaults={'launch_invite_code': invite_code, 'registered': False})
            if not crtd:
                try:
                    already_registered = ActUserProfile.objects.get(email=email)
                except:
                    already_registered = False
                if already_registered:
                    return HttpResponse('You are already registered, please <a href="/login/">login</a> and start sharing link for you and your group.')
                else:
                    if iq.send_invite_date is not None:
                        form = '<form method="POST" action="/resend_invite/"><input type="hidden" name="email" value="%s"/><input type="submit" value="Click Here" /></form>'%(iq.email_id)
                        return HttpResponse('We have already sent you the invitation, please check you mail again, In case you have lost it <br/><br/> %s  to send it again.'%(form))
                    else:
                        return HttpResponse("You are already on the waitlist, We'll send you an invite soon!!!")
            return HttpResponse('Thanks for dropping by, you\'ll be one of the first to get in when we open.')
        else:
            return HttpResponse('please try again with valid email-id')
    else:
        return render_to_response('invite_request.html', {}, context_instance=RequestContext(request))


@csrf_exempt
def resend_invite(request):
    if request.user.id is None:
        return HttpResponseRedirect('/')
    if not request.user.is_superuser:
        return HttpResponseRedirect('/')

    if request.method=='POST':
        if 'email' in request.POST:
            email = request.POST.get('email').strip()
            iq = InviteQueue.objects.get(email_id=email)
            salt = sha1(str(random.random())).hexdigest()
            invite_code = sha1(salt+email).hexdigest()
            iq.launch_invite_code = invite_code
            #send email
            email_subject = 'Focal early invite'
            email_mesg = render_to_string('early_invite.html', { 'invite_link': '%s/?invite_code=%s'%(DOMAIN, iq.launch_invite_code)})
            eq = EmailQueue.objects.create(content_type=None, object_pk=None, email_subject=email_subject, message_body=email_mesg, from_email='Focal <help@focal.io>', to_emails=iq.email_id)
            iq.send_invite_date = datetime.now()
            iq.save()
            return HttpResponse('Thanks for dropping by, you\'ll be one of the first to get in when we open.')
        return HttpResponse('please try again with valid email-id')
    else:
        return HttpResponse('wrong request')


@csrf_exempt
def invite_management(request):
    if request.user.id is None:
        return HttpResponseRedirect('/')
    if not request.user.is_superuser:
        return HttpResponseRedirect('/')

    if request.method=='POST':
        invite_id = request.POST.get('invite_id')
        iq = InviteQueue.objects.get(id=int(invite_id))
        #send email
        email_subject = 'Focal early invite'
        email_mesg = render_to_string('early_invite.html', { 'invite_link': '%s/?invite_code=%s'%(DOMAIN, iq.launch_invite_code)})
        eq = EmailQueue.objects.create(content_type=None, object_pk=None, email_subject=email_subject, message_body=email_mesg, from_email='Focal <help@focal.io>', to_emails=iq.email_id)
        iq.send_invite_date = datetime.now()
        iq.save()
        return HttpResponseRedirect('/manage_invites/')
    else:
        all_invites = InviteQueue.objects.all()
        return render_to_response('invite_management.html', {'all_invites': all_invites})


@csrf_exempt
def generate_new_invite(request):
    if request.user.id is None:
        return HttpResponseRedirect('/')
    if not request.user.is_superuser:
        return HttpResponseRedirect('/')

    if request.method=='POST':
        email = request.POST.get('email', None)
        salt = sha1(str(random.random())).hexdigest()
        invite_code = sha1(salt+str(datetime.now())).hexdigest()
        iq, crtd = InviteQueue.objects.get_or_create(email_id=email, launch_invite_code=invite_code, registered=False) #, send_invite_date=datetime.now())
        return HttpResponseRedirect('/manage_invites/')
    else:
        return HttpResponse('wrong request')



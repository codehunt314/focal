import os, time, sys
PROJECT_PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.append(PROJECT_PATH + "/../")
os.environ['DJANGO_SETTINGS_MODULE'] = 'myproject.settings'

from my_html2txt import extract_text
from myproject.activity.models import *



while True:
  jobs = list(Link.objects.filter(processed=False))
  print 'processing %d jobs'%(len(jobs))
  while len(jobs)!=0:
    for job in jobs:
      try:
        data = extract_text(job.link)
        job.title = data['title']
        job.body = data['data']
      except Exception, e:
        pass
      job.processed = True
      job.save()
      jobs.remove(job)
      print 'finished processint ' + job.link
  print 'finished current job, now going for sleep for 90 sec'
  time.sleep(90)




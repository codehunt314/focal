import urllib2
from lxml import html

def extract_text(url):
  def f(root):
    for chld in root.getchildren():
      if isinstance(chld, html.HtmlComment):
        root.remove(chld)
      elif chld.tag in ['script', 'style']:
        root.remove(chld)
      else:
        f(chld)
  req = urllib2.Request(url, None, headers = { 'User-Agent' : 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)' })
  u = urllib2.urlopen(req)
  if u.headers.get('content-encoding', None)=='gzip':
    import StringIO, gzip
    gzipper = gzip.GzipFile(fileobj=StringIO.StringIO(u.read()))
    url_text = gzipper.read()
  else:
    url_text = u.read()
  tree = html.fromstring(url_text)
  f(tree.body)
  title = tree.findall('.//title')
  if len(title)==1:
    title = title[0].text_content().strip()
  else:
    title = ''
  data = [' '.join(i.strip().split()) for i in tree.body.text_content().split('\n') if i.strip()!='' and len(i.strip().split())>2]
  s = sum([len(i) for i in data])
  data1 = [(float(len(i))/s, i) for i in data]
  data1.sort(reverse=True)
  return {'data': ' '.join([i[1] for i in data1[:5]]), 'title': title}

def extract_title(url):
  u = urllib2.urlopen(url)
  if u.headers.get('content-encoding', None)=='gzip':
    import StringIO, gzip
    gzipper = gzip.GzipFile(fileobj=StringIO.StringIO(u.read()))
    url_text = gzipper.read()
  else:
    url_text = u.read()
  tree = html.fromstring(url_text)
  title = tree.findall('.//title')
  if len(title)==1:
    title = title[0].text_content().strip()
  else:
    title = ''
  return title


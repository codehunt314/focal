import psutil, os, time, sys
PROJECT_PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.append(PROJECT_PATH + "/../")
os.environ['DJANGO_SETTINGS_MODULE'] = 'myproject.settings'

from django.core.mail import EmailMultiAlternatives

def _tracker():
    d = {
        'searchd --config sphinx.conf': 'searchd', 
        'python data_import.py': 'data_import_deamon', 
        'python manage.py runfcgi host=127.0.0.1 port=8090 --settings=settings': 'enso_server', 
        'python manage.py runfcgi host=127.0.0.1 port=8088 --settings=settings': 'polo_server', 
        'python manage.py runfcgi host=127.0.0.1 port=8089 --settings=settings': 'sophos_server', 
        'python email_sender.py': 'email_sender', 
        'python link_processing.py': 'link_processing', 
        'python manage.py runfcgi host=127.0.0.1 port=8080 --settings=settings': 'focal_server'
    }
    result = set()
    for proc in psutil.get_process_list():
        if len(set(['python', 'searchd']) & set(proc.cmdline))>0:
            result.add(' '.join(proc.cmdline))
    for proc_cmdline, service_name in d.items():
        if proc_cmdline not in result:
            msg = EmailMultiAlternatives('%s is down' % (service_name), '%s is down' % (service_name), 'help@focal.io', ['vipul.shaily@gmail.com'])
            msg.send()


while True:
    _tracker()
    time.sleep(1500)

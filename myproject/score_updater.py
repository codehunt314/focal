import os, time, sys
PROJECT_PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.append(PROJECT_PATH + "/../")
os.environ['DJANGO_SETTINGS_MODULE'] = 'myproject.settings'

import django.db.models
from django.db import connections, transaction
con = connections['default']

from myproject.activity.models import *
from datetime import datetime, timedelta
from collections import defaultdict

dd = defaultdict(lambda:0)
d = defaultdict(lambda:dd)
al = ActivityLink.objects.filter(submited_on__lt=datetime.now(), submited_on__gte=datetime.now()-timedelta(10)).values('activity', 'submited_by')
for entry in al:
    d[entry.get('submited_by')][entry.get('activity')] += 5

s = ''
for entry in ActivityMember.objects.values('id', 'activity', 'member', 'escore'):
    new_score = d[entry.get('member')][entry.get('activity')]
    if new_score>0:
        s = s + 'when id=%d then %d '%(entry.get('id'), entry.get('escore') + new_score)
    else:
        s = s + 'when id=%d then %d '%(entry.get('id'), int(entry.get('escore') * 0.98))

custom_query = 'update %s set %s = case %s end;' % (ActivityMember._meta.db_table, con.ops.quote_name('escore'),s)
print custom_query
con.cursor().execute(custom_query)
transaction.commit_unless_managed()
con.close()


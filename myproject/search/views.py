from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.loader import render_to_string
from django.utils.datastructures import SortedDict
from django.utils import simplejson as json
from django.template import RequestContext
from django.conf import settings
from django.utils.encoding import force_unicode
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator

from myproject.tcomment.models import *
from myproject.activity.models import *
from tagging.models import Tag, TaggedItem

from myproject.url_encode import encode_url, decode_url
from collections import defaultdict

import re
ALL_FIELDS = '(\'.+?\'|".+?"|[^\'"\s]{2}\S*):\s*(\'.+?\'|".+?"|[^\'"\s]\S*)'


def _parse(query):
    d = {'context': [], 'from': [], 'tag': [], 'text': []}
    for k,v in re.findall(ALL_FIELDS, query):
        v = v.replace('"', '')
        if k in d: d[k].append(v)
    t = []
    for i in re.sub(ALL_FIELDS, '', query).strip().split('  '):
        if i: t.append(i)
    d['text'].extend(t)
    return d    

def _parse_facet(facet_query, raw_query):
    d = {'context': [], 'from': [], 'tag': [], 'query': []}
    if facet_query is None:
        facet_query = ''
    if len(facet_query.strip()) > 0:
        all_facet = re.split(r'(?<!\\),', facet_query)
        for each_facet in all_facet:
            ind = each_facet.find(':')
            val = each_facet[ind+1:].replace('\\','')
            d[each_facet[:ind]].append(val)
    if raw_query:
        d['query'] = [raw_query.strip().lower()]
    return d

def new_search(q, facet, user, page_num):
    res = _parse_facet(facet, q)
    ua_list = ActivityMember.objects.get_user_activity(user)
    user_activity_dict = dict([(i.id, i)for i in ua_list])
    activity_filter = [k for k,v in user_activity_dict.items() if v.activityname in res['context']]
    if len(activity_filter)==0:
        activity_filter = user_activity_dict.keys()
    user_filter = [u.get('id') for u in ActUserProfile.objects.filter(nickname__in=res['from']).values('id')]
    tag_filter = [t.get('id') for t in Tag.objects.filter(name__in=res['tag']).values('id')]
    query_text = ' '.join(res['query']).strip().lower()
    if len(activity_filter)==0: activity_filter = user_activity_dict.keys()
    rs = Link.search.query(query_text)
    if len(tag_filter)>0:
        rs = rs.filter(tag_id=tag_filter)

    rs = rs.filter(activity_id=activity_filter)
    if len(user_filter)>0:
        rs = rs.filter(submited_by_id=user_filter)

    activity_group = rs.group_by('activity_id', 4, '@rank desc')
    user_activities = {}
    for  i in activity_group:
        ua = user_activity_dict.get(i._sphinx['attrs']['@groupby'], None)
        if ua:
            user_activities[i._sphinx['attrs']['@groupby']] = i._sphinx['attrs']['@count']
    user_activity_list = dict([(user_activity_dict[k], v) for k,v in user_activities.items()])

    user_group = rs.group_by('submited_by_id', 4, '@rank desc')
    user_friends = {}
    for i in user_group:
        user_friends[i._sphinx['attrs']['@groupby']] = i._sphinx['attrs']['@count']
    user_friend_dict = ActUserProfile.objects.in_bulk(user_friends.keys())
    user_friend_list = dict([(user_friend_dict[k], v) for k,v in user_friends.items()])

    limit = 20
    rs._offset = limit * (int(page_num)-1)
    rs._limit = limit #+ rs._offset
    total_result = rs._sphinx['total']
    has_next = True if rs._limit<total_result else False

    almap = ActivityLink.objects.filter(actlink__in=rs, activity__in=ua_list, deleted=False).values()#('actlink', 'activity', 'submited_by', 'submited_on')
    from activity.views import _result
    ret = defaultdict(lambda: {'submited_on': [], 'linkshared_status': [], 'almap_id': []})
    return _result(almap, ret, user_activity_dict, user), user_activity_list, user_friend_list, total_result, has_next


def search(request):
    user = request.user
    if user.id is None:
        return render_to_response('home.html', {}, context_instance=RequestContext(request))

    q = request.GET.get('q', None)
    facet = request.GET.get('filter', None)
    if q is None and facet is None:
        return HttpResponseRedirect('/')
    page_num = request.GET.get('page_num', 1)
    ret_list, user_activity_list, user_friend_list, total_result, has_next = new_search(q, facet, user, page_num)
    if request.GET.get('full_page',None):
        s = render_to_string('search_result_ajax_html.html', {
                                          'sterm': q.strip(),
                                          'filter':facet.strip(),
                                          'actuser': user,
                                          'ret_list': ret_list,
                                          'user_friend_list': user_friend_list,
                                          'user_activity_list': user_activity_list,
                                          'total_result': total_result,
                                          'has_next': has_next
                        }, context_instance=RequestContext(request))
        p = { 'items':total_result, 'has_next':str(has_next), 'number':page_num}
        return HttpResponse(json.dumps({"page":p, "html":s}))

    if request.GET.get('is_ajax',None):
        s = render_to_string('single_entry.html', {
                                        'actuser': user, 
                                        'ret_list': ret_list, 
                            }, context_instance=RequestContext(request))

        p = { 'items':total_result, 'has_next':str(has_next), 'number':page_num}
        return HttpResponse(json.dumps({"page":p, "html":s}))
    else:
        return render_to_response('search_result.html', {
                                          'sterm': q.strip(),
                                          'filter':facet.strip(),
                                          'actuser': user,
                                          'ret_list': ret_list,
                                          'user_friend_list': user_friend_list,
                                          'user_activity_list': user_activity_list,
                                          'total_result': total_result,
                                          'has_next': has_next
                        }, context_instance=RequestContext(request))



"""
def search(request):
    user = request.user
    if user.id is None:
        return render_to_response('home.html', {}, context_instance=RequestContext(request))

    q = request.GET.get('q', None)
    if not q:
      return HttpResponse('wrong query')

    from collections import defaultdict
    from copy import deepcopy
    page_num = request.GET.get('page_num', 1)
    activity_key = request.GET.get('activity', None)
    user_key = request.GET.get('user', None)
    user_friends = {} #defaultdict(lambda: 0)
    user_activities = {} #defaultdict(lambda: 0)
    shared_status_dict = defaultdict(lambda: defaultdict(lambda: {}))
    shared_time = defaultdict(lambda: [])

    ua_list = ActivityMember.objects.get_user_activity(user)
    user_activity_dict = dict([(i.id, i)for i in ua_list])

    rs1 = Link.search.query(q)
    rs = rs1.filter(activity_id=user_activity_dict.keys())

    activity_group = rs.group_by('activity_id', 4, '@rank desc')
    user_group = rs.group_by('submited_by_id', 4, '@rank desc')

    for  i in activity_group:
        ua = user_activity_dict.get(i._sphinx['attrs']['@groupby'], None)
        if ua:
            user_activities[i._sphinx['attrs']['@groupby']] = i._sphinx['attrs']['@count']
    user_activity_list = [(user_activity_dict[k], v) for k,v in user_activities.items()]

    for i in user_group:
        user_friends[i._sphinx['attrs']['@groupby']] = i._sphinx['attrs']['@count']
    user_friend_dict = ActUserProfile.objects.in_bulk(user_friends.keys())
    user_friend_list = [(user_friend_dict[k], v) for k,v in user_friends.items()]

    if user_key:
        rs = rs.filter(submited_by_id=decode_url(user_key))
    if activity_key:
        rs = rs1.filter(activity_id=decode_url(activity_key))

    #rs = rs.order_by('@weights')
    almap = ActivityLink.objects.filter(actlink__in=rs, activity__in=ua_list, deleted=False).values('actlink', 'activity', 'submited_by', 'submited_on')

    result_links = []
   
    for i in almap:
        shared_status_dict[i.get('actlink')][i.get('submited_by')][i.get('activity')] = user.id==i.get('submited_by')
        result_links.append((i.get('actlink'), i.get('submited_by'), i.get('activity')))
        shared_time[i.get('actlink')].append(i.get('submited_on'))


    list_of_links = []
    for linkobj in rs:
        ret = []
        for u,a in shared_status_dict[linkobj.id].items():
            actvity_list = []
            submit_by_uobj = user_friend_dict[u]
            for act, perm in a.items():
                to_add = deepcopy(user_activity_dict[act])
                setattr(to_add, 'can_delete', perm)
                actvity_list.append(to_add)
            ret.append((submit_by_uobj, actvity_list))
        if len(ret)>0:
            setattr(linkobj, 'linkshared_status', ret)
            setattr(linkobj, 'comment_count', 0)
            shared_time[linkobj.id].sort(reverse=True)
            setattr(linkobj, 'shared_time', shared_time[linkobj.id][0])
            list_of_links.append(linkobj)

    return render_to_response('search_result.html', {
                                          'sterm': q.strip(),
                                          'actuser': user, 
                                          'ret_list': filter(lambda x: x is not None, list_of_links),
                                          'user_friend_list': user_friend_list,
                                          'user_activity_list': user_activity_list
                        }, context_instance=RequestContext(request))
"""

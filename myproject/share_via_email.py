import os, sys, re, imaplib, email, time
from email.header import decode_header
from email.Iterators import typed_subpart_iterator

PROJECT_PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.append(PROJECT_PATH + "/../")
os.environ['DJANGO_SETTINGS_MODULE'] = 'myproject.settings'
from myproject.activity.models import *
from myproject.focaluser.models import *

def get_charset(message, default="ascii"):
    if message.get_content_charset():
        return message.get_content_charset()
    if message.get_charset():
        return message.get_charset()
    return default

def get_header_value(raw_header_value):
    if raw_header_value:
        return "".join([unicode(text, charset or "ascii") for text, charset in decode_header(raw_header_value)])
    return ""

def get_emails(email_str):
    if email_str.find(',')!=-1:
        ret = []
        for each in email_str.split(','):
            if each.find('<')!=-1 and each.find('>')!=-1:
                ret.append(each[each.find('<')+1:each.find('>')].strip())
            else:
                ret.append(each.strip())
        return ret
    else:
        if email_str.find('<')!=-1 and email_str.find('>')!=-1:
            return [email_str[email_str.find('<')+1:email_str.find('>')]]
        return [email_str.strip()]

def get_links(body_text):
    return re.findall(r'(https?://\S+)', body_text)

def get_email_body(imap_email, uid):
    result, str_data = imap_email.fetch(uid, '(RFC822)')
    em = email.message_from_string(str_data[0][1])
    x_mailer = em.get("X-Mailer", None)
    if x_mailer is None:
        return None
    x_mailer = x_mailer.lower()
    if x_mailer.find('ipad')==-1:
        return None
    to = [i for i in get_emails(em.get("To", None)) if i.split('@')[1]=='focal.io' and i.split('@')[0].startswith('update')]
    frm = get_emails(em.get("From", None))
    subject = get_header_value(em.get("Subject", None))
    if em.is_multipart():
        body_parts = [part for part in typed_subpart_iterator(em, 'text', 'plain')]
        body_text_list = []
        for part in body_parts:
            cs = get_charset(part, get_charset(em))
            body_text_list.append(unicode(part.get_payload(decode=True), cs, "replace"))
        body = "\n".join(body_text_list)
    else:
        body = unicode(em.get_payload(decode=True), get_charset(em), "replace")
    return {'title': subject, 'links': get_links(body), 'from': frm[0], 'to': to[0]}


while True:
    try:
        mail = imaplib.IMAP4_SSL('imap.gmail.com')
        mail.login('update@focal.io', 'slopeone')
        mail.select("inbox")
        status, data = mail.search(None, '(UNSEEN)')
        unread_id_list = data[0].split()
        unread_id_list.reverse()
        for i in unread_id_list:
            ret = get_email_body(mail, i)
            if ret:
                user = ActUserProfile.objects.get(email=ret.get('from'))
                url_list = ret.get('links', [])
                for link in url_list[:1]:
                    web_url, created = Link.objects.get_or_create_link(link=link, defaults={'title': ret['title'], 'favicon': '', 'thumb': '', 'metadesc': '', 'processed': False})
                    al, shared = ActivityLink.objects.sharealink(Activity.objects.get(activityname='Personal', created_by=user), web_url, user)
        time.sleep(90)
        mail.noop()
    except:
        mail.close()
        mail.logout()
        pass


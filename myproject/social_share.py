import os, sys, urllib
PROJECT_PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.append(PROJECT_PATH + "/../")
os.environ['DJANGO_SETTINGS_MODULE'] = 'myproject.settings'

from django.utils.encoding import smart_str, smart_unicode
from myproject.activity.models import *
from myproject.activity.url_shorter import short_url
from myproject.activity.tw_share import status_update
from myproject.activity.instapaper_share import save_bookmark_in_instapaper

while True:
    for each in SocialShareQueue.objects.all():
        try:
            shared = False
            if each.activityname=='Share on Twitter':
                #shorten_url = short_url(each.url.link)
                shorten_url = each.url.link
                title = smart_str(each.url.title)
                if each.url.title=='':
                    status_mesg = shorten_url
                else:
                    req_ttl_len = 119#-len(shorten_url)
                    if len(title)<=req_ttl_len:
                        ttl = title
                    else:
                        strip_indx = title[:req_ttl_len].rfind(' ')
                        ttl = title[:strip_indx].strip()
                    status_mesg = '%s %s'%(ttl, smart_str(shorten_url))
                r = status_update(each.user.twitter_access_token, status_mesg)
                shared = True
            if each.activityname=='Share on Facebook':
                params = urllib.urlencode({'access_token': each.user.facebook_access_token, 'link': each.url.link})
                f = urllib.urlopen("https://graph.facebook.com/me/feed", params)
                r = json.loads(f.read())
                if r.get('id', None):
                    shared = True
                else:
                    shared = False
            if each.activityname=='Read It Later':
                if each.user.instapaper_access_token:
                    r = save_bookmark_in_instapaper(each.user.instapaper_access_token, each.url.link, each.url.title.encode('utf8'))
                shared = True
            each.delete()
        except Exception, e:
            print e
            pass

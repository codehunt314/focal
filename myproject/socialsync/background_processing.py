from socialsync.models import *
from django.utils import simplejson as json

def get_links_for_socialprofile(sp, service_name='twitter', since_id=None):
    rules = FriendActPipe.objects.filter(created_by=sp)
    home_time_line = sp.get_home_timeline(since_id)
    result = []
    identifier_list = []
    for i in home_time_line:
        d = {'url': [], 'text': None, 'id': None}
        for j in i["entities"]["urls"]:
            d['url'].append(j['expanded_url'] or j['url'])
        d['text'] = i.get('text')
        d['identifier'] = i.get('id')
        d['by_user'] = i['user']['id']
        identifier_list.append(int(d['identifier']))
        result.append(d)
    for each_rule in rules:
        filtered_result = filter(lambda x: str(x['by_user']) in json.loads(each_rule.friend), result)
        for each_res in filtered_result:
            for each_url in each_res['url']:
                obj, created = ExtractedLink.objects.get_or_create(rule=each_rule, link=each_url, shared_by=each_res['by_user'], source=service_name, text=each_res['text'], identifier=each_res['identifier'])
    sp.sinceid = str(max(identifier_list))
    sp.save()

sp = UserSocialProfile.objects.all()[0]
get_links_for_socialprofile(sp, 'twitter')

import httplib
from django.conf import settings
from django.utils import simplejson
import oauth.oauth as oauth

social_services_server = {'TWITTER': 'api.twitter.com'}
social_services_param = {
        'twitter': {
                        'SERVER': social_services_server['TWITTER'],
                        'REQUEST_TOKEN_URL': 'https://%s/oauth/request_token' % (social_services_server['TWITTER']),
                        'ACCESS_TOKEN_URL': 'https://%s/oauth/access_token' % (social_services_server['TWITTER']),
                        'AUTHORIZATION_URL': 'http://%s/oauth/authorize' % (social_services_server['TWITTER']),
                        'CONSUMER_KEY': 'H3jeq7N6rhPUm09cB6svg',
                        'CONSUMER_SECRET': 'wYFsifIclxGzvwbcSHw3dJreM7fCfJZd2tLEUvV3ck'
                    }
    }

signature_method = oauth.OAuthSignatureMethod_HMAC_SHA1()

def get_consumer(service_name='twitter'):
    return oauth.OAuthConsumer(social_services_param[service_name]['CONSUMER_KEY'], social_services_param[service_name]['CONSUMER_SECRET'])

def fetch_response(oauth_request, service_name='twitter'):
    connection = httplib.HTTPSConnection(social_services_param[service_name]['SERVER'])
    url = oauth_request.to_url()
    connection.request(oauth_request.http_method,url)
    response = connection.getresponse()
    s = response.read()
    return s

def get_unauthorised_request_token(callback=None, service_name='twitter'):
    consumer = get_consumer(service_name)
    oauth_request = oauth.OAuthRequest.from_consumer_and_token(consumer, callback=callback, http_url=social_services_param[service_name]['REQUEST_TOKEN_URL'])
    oauth_request.sign_request(signature_method, consumer, None)
    resp = fetch_response(oauth_request)
    token = oauth.OAuthToken.from_string(resp)
    return token

def get_authorisation_url(token, service_name='twitter'):
    consumer = get_consumer(service_name)
    oauth_request = oauth.OAuthRequest.from_consumer_and_token(consumer, token=token, http_url=social_services_param[service_name]['AUTHORIZATION_URL'])
    oauth_request.sign_request(signature_method, consumer, token)
    return oauth_request.to_url()

def exchange_request_token_for_access_token(request_token, service_name='twitter'):
    consumer = get_consumer(service_name)
    oauth_request = oauth.OAuthRequest.from_consumer_and_token(consumer, token=request_token, http_url=social_services_param[service_name]['ACCESS_TOKEN_URL'])
    oauth_request.sign_request(signature_method, consumer, request_token)
    resp = fetch_response(oauth_request)
    r = oauth.urlparse.parse_qs(resp)
    return oauth.OAuthToken.from_string(resp).to_string(), r['user_id'][0], r['screen_name'][0]


def _signedoauth_request_result(access_token, url, service_name, http_method, param={}):
    token = oauth.OAuthToken.from_string(access_token)
    consumer = get_consumer(service_name)
    oauth_request = oauth.OAuthRequest.from_consumer_and_token(consumer, token=token, http_url=url, http_method=http_method, parameters=param)
    oauth_request.sign_request(signature_method, consumer, token)
    return fetch_response(oauth_request)

def get_hometimeline(access_token, service_name='twitter', since_id=None):
    HOME_TIMELINE_URL = 'https://api.twitter.com/1/statuses/home_timeline.json?'
    return _signedoauth_request_result(access_token, HOME_TIMELINE_URL, service_name, 'GET', {'include_entities': 'true', 'count': 200, 'since_id':since_id or '1'})

def _get_user_detail(access_token, userid_list, service_name='twitter', index=0, ret={}):
    if index*100>len(userid_list): return None
    USER_LOOKUP_URL = "https://api.twitter.com/1/users/lookup.json?"
    json = _signedoauth_request_result(access_token, USER_LOOKUP_URL, service_name, 'GET', {'user_id': ','.join(userid_list[index*100:(index+1)*100]), 'include_entities': 'false'})
    data = simplejson.loads(json)
    for i in data:
        ret[i.get('id')] = {'name': i.get('name'), 'screen_name': i.get('screen_name'), 'profile_image': i.get('profile_image_url')}
    _get_user_detail(access_token, userid_list, service_name='twitter', index=index+1, ret=ret)

def get_user_friends(access_token, screen_name, service_name='twitter'):
    USER_FRIENDS_URL = 'https://api.twitter.com/1/friends/ids.json?'
    json = _signedoauth_request_result(access_token, USER_FRIENDS_URL, service_name, 'GET', {'cursor': '-1', 'screen_name': screen_name})
    ret_val = {}
    r = _get_user_detail(access_token, [str(i).strip() for i in simplejson.loads(json)['ids']], service_name=service_name, ret=ret_val)
    return ret_val


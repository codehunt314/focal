from django.db import models
from activity.models import Activity
from focaluser.models import ActUserProfile

from helpers import *


class UserSocialProfile(models.Model):
    user = models.ForeignKey(ActUserProfile)
    access_token = models.CharField(max_length=255, blank=True, null=True)
    profilename = models.CharField(max_length=255, blank=True, null=True)
    profilepics = models.URLField(max_length=255, blank=True, null=True)
    other_info = models.TextField(blank=True, null=True)
    service_name = models.CharField(max_length=255)
    sinceid = models.TextField(blank=True, null=True)

    class Meta:
        unique_together = ('user', 'service_name')

    def get_user_friends(self):
        return get_user_friends(self.access_token, self.profilename, self.service_name)

    def get_home_timeline(self, fliter_link=True):
        time_line = get_hometimeline(self.access_token, self.service_name, self.sinceid)
        timeline_obj = simplejson.loads(time_line)
        return filter(lambda x: x.get("entities") and x.get("entities").get("urls"), timeline_obj)
        

class FriendActPipe(models.Model):
    created_by = models.ForeignKey(UserSocialProfile)
    activity = models.ForeignKey(Activity)
    friend = models.TextField()
    source = models.CharField(max_length=100)


class ExtractedLink(models.Model):
    rule = models.ForeignKey(FriendActPipe)
    expanded_link = models.TextField(blank=True, null=True)
    link = models.TextField()
    shared_by = models.CharField(max_length=255)
    source = models.CharField(max_length=255)
    text = models.TextField(blank=True, null=True)
    identifier = models.CharField(max_length=255, blank=True, null=True)
    processed = models.BooleanField(default=False)


from twisted.internet import reactor, protocol
from twisted.web import http

class TwitterStreamer(http.HTTPClient):
    def connectionMade(self):
        self.sendCommand('GET', self.factory.url)
        self.sendHeader('Host', self.factory.host)
        self.sendHeader('User-Agent', self.factory.agent)
        self.sendHeader('Authorization', self.factory.oauth_header)
        self.endHeaders()
  
    def handleStatus(self, version, status, message):
        if status != '200':
            self.factory.tweetError(ValueError("bad status"))
  
    def lineReceived(self, line):
        self.factory.tweetReceived(line)

    def connectionLost(self, reason):
        self.factory.tweetError(reason)



class TwitterStreamerFactory(protocol.ClientFactory):
      protocol = TwitterStreamer
  
      def __init__(self, oauth_header):
          self.url = TWITTER_STREAM_API_PATH
          self.agent='Twisted/TwitterStreamer'
          self.host = TWITTER_STREAM_API_HOST
          self.oauth_header = oauth_header
  
      def clientConnectionFailed(self, _, reason):
          self.tweetError(reason)
  
      def tweetReceived(self, tweet):
        print tweet
  
      def tweetError(self, error):
        print error


def load_access_token():
    with open(ACCESS_TOKEN_FILE) as f:
        lines = f.readlines()

    str_key=lines[0].strip().split('=')[1]
    str_secret=lines[1].strip().split('=')[1]
    return oauth.Token(key=str_key, secret=str_secret)


def build_authorization_header(access_token):
    url = "http://%s%s" % (TWITTER_STREAM_API_HOST, TWITTER_STREAM_API_PATH)
    params = {
        'oauth_version': "1.0",
        'oauth_nonce': oauth.generate_nonce(),
        'oauth_timestamp': int(time.time()),
        'oauth_token': access_token.key,
        'oauth_consumer_key': CONSUMER.key
    }

    # Sign the request.
    req = oauth.Request(method="GET", url=url, parameters=params)
    req.sign_request(oauth.SignatureMethod_HMAC_SHA1(), CONSUMER, access_token)

    # Grab the Authorization header
    header = req.to_header()['Authorization'].encode('utf-8')
    print "Authorization header:"
    print "     header = %s" % header
    return header




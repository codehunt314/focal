import urllib, urllib2, getpass, base64
from django.utils import simplejson as json

def fetch(uri, username='', password='', data=None):
  headers = {}
  if username and password:
    headers['Authorization'] = 'Basic ' + base64.b64encode('%s:%s' % (username,
        password))
    headers['User-Agent'] = 'TwitterStreamingSample'
  req = urllib2.Request(uri, headers=headers)
  if data:
    req.add_data(urllib.urlencode(data))
  f = urllib2.urlopen(req)
  return f


def main():
  username = raw_input('Twitter Username: ')
  password = getpass.getpass('Twitter Password: ')
  track = raw_input('Tracking keyword? ')
  try:
    f = fetch('http://stream.twitter.com/1/statuses/filter.json', username,
        password, {'track': track})
    print 'Tracking... [Control + C to stop]'
    print
    while True:
      line = f.readline()
      if line:
        status = json.loads(line)
        try:
          print '%s: %s' % (status['user']['screen_name'], status['text'])
        except KeyError, e:
          # Something we don't handle yet.
          print '* FIXME *', line
      else:
        time.sleep(0.1)
  except urllib2.HTTPError, e:
    # Deal with unexpected disconnection
    raise e
  except KeyboardInterrupt:
    # End
    f.close()
    print 'Bye!'

main()

from django.conf.urls.defaults import *


urlpatterns = patterns('',
	(r'^$', 'socialsync.views.socialsync'),
    (r'^(?P<service_name>[a-z]+)/$', 'socialsync.views.service_home'),
    (r'^(?P<service_name>[a-z]+)/sync/$', 'socialsync.views.service_sync'),
	(r'^(?P<service_name>[a-z]+)/addrule/$', 'socialsync.views.service_addrule'),
	(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': '/Users/vipul/Projects/23spaces/focal/myproject/socialsync/templates'}),
)

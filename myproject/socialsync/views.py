from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, redirect
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.utils import simplejson as json

from activity.models import *
from socialsync.models import *
from helpers import *


def _get_social_profile(user, service_name=None):
    ret = dict([(i.service_name, i) for i in UserSocialProfile.objects.filter(user=user)])
    if len(ret)==0: return None
    if service_name:
        return ret[service_name]
    else:
        return dict([(i.service_name, i) for i in UserSocialProfile.objects.filter(user=user)])

def socialsync(request):
    return render_to_response('socialsync.html', {'user': request.user})

def service_home(request, service_name='twitter'):
    user_activity = []
    user_friends = []
    rule_dict = {}
    social_profile = _get_social_profile(request.user, service_name)
    if social_profile and social_profile.access_token:
        user_activity = ActivityMember.objects.get_user_activity(request.user)
        other_info = simplejson.loads(social_profile.other_info)
        if not other_info.get('friend_list'):
            user_friend_dict = social_profile.get_user_friends()
            other_info['friend_list'] = [(k, v['name'], v['screen_name'], v['profile_image']) for k,v in user_friend_dict.items()]
            social_profile.other_info = simplejson.dumps(other_info)
            social_profile.save()
            user_friends = other_info['friend_list']
        else:
            user_friends = other_info.get('friend_list')
    for i in FriendActPipe.objects.filter(activity__in=user_activity):
        friend_list = filter(lambda x: str(x[0]) in json.loads(i.friend), user_friends)
        rule_dict[i.activity] = {'source': i.source, 'friend_list': friend_list}
    return render_to_response('service_home.html', {'social_profile': social_profile, 'user_activity': user_activity, 'user_friends': user_friends, 'rule_dict': rule_dict})


def service_sync(request, service_name='twitter'):
    unauthed_token = request.session.get('unauthed_token', None)
    oauth_token = request.GET.get('oauth_token', None)
    oauth_verifier = request.GET.get('oauth_verifier', None)
    denied = request.GET.get('denied', None)
    if unauthed_token and denied:
        return render_to_response('service_home.html', {'user': request.user, 'accept': False})
    elif unauthed_token and oauth_token and oauth_verifier:
        token = oauth.OAuthToken.from_string(unauthed_token)
        if token.key != oauth_token:
            return HttpResponse('token are not matching, smthng wrng')
        access_token, user_id, screen_name = exchange_request_token_for_access_token(token)
        request.session['access_token'] = access_token
        #request.user.save_tw_access_token(access_token)
        obj, created = UserSocialProfile.objects.get_or_create(user=request.user, service_name='twitter', defaults={'access_token': access_token, 'profilename': screen_name, 'other_info': json.dumps({'user_id': user_id})})
        return HttpResponseRedirect('/socialsync/%s/'%(service_name))
    else:
        callback_url = 'http://localhost:8008/socialsync/twitter/sync/'
        token = get_unauthorised_request_token(callback=callback_url, service_name='twitter')
        request.session['unauthed_token'] = token.to_string()
        auth_url = get_authorisation_url(token, 'twitter')
        return HttpResponseRedirect(auth_url)

@csrf_exempt
def service_addrule(request, service_name='twitter'):
    post_data = dict(request.POST)
    if request.method=='POST':
        activity_id = post_data.get('activity', None)
        if not activity_id: return HttpResponse('wrong data')
        activity_obj = Activity.objects.get(id=activity_id[0])
        friend_list_id = post_data.get('friend_list')
        print friend_list_id
        if isinstance(friend_list_id, list):
            friend_list = friend_list_id
        else:
            friend_list = [friend_list_id]
        social_profile = _get_social_profile(request.user, service_name)
        rule, created = FriendActPipe.objects.get_or_create(created_by=social_profile, activity=activity_obj, source=service_name)
        rule.friend = json.dumps(friend_list)
        rule.save()
        return HttpResponseRedirect('/socialsync/twitter/')
    else:
        return HttpResponseRedirect('/socialsync/twitter/')

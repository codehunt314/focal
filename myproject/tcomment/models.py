from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.contrib.auth.models import User
from datetime import datetime
from django.utils.translation import ugettext_lazy as _

from myproject.activity.models import ActivityLink
from myproject.focaluser.models import ActUserProfile

class MyComment(models.Model):
    comment_text = models.TextField()
    parent = models.ForeignKey('self', null=True, blank=True, default=None, verbose_name=_('Parent'))
    level = models.IntegerField(default=0)
    user = models.ForeignKey(ActUserProfile)
    submit_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    ipaddress = models.CharField(max_length=20)

class CommentMap(models.Model):
    activitylink = models.ForeignKey(ActivityLink)
    comment = models.ForeignKey(MyComment)


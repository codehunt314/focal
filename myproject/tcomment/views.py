from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from models import *
from django.utils import simplejson as json
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.utils.timesince import timesince

from myproject.activity.models import *
from myproject.tcomment.models import *
from datetime import datetime, timedelta

from django.core.urlresolvers import reverse

import urllib


def send_new_comment_mail(new_comment, link, activities, param, ntype):
    from django.template.loader import render_to_string
    email_mesg = render_to_string('email_templates/new_comment_mail_template.html', param)
    to_list = set()
    al = ActivityLink.objects.filter(actlink=link, activity__in=activities)
    for cm in CommentMap.objects.filter(activitylink__in=al):
        to_list.add(cm.comment.user)
    for i in al:
        to_list.add(i.submited_by)
    to_list.remove(new_comment.user)
    to_email = dict([(u.email, u) for u in to_list if u.notification_perm(ntype)])
    email_subject = 'New Comment by %s %s'%(new_comment.user.first_name, new_comment.user.last_name)

    from django.contrib.contenttypes.models import ContentType
    ctype = ContentType.objects.get(app_label=new_comment._meta.app_label, model=new_comment._meta.module_name)

    eq = EmailQueue.objects.create(content_type=ctype, object_pk=new_comment.pk, email_subject=email_subject, message_body=email_mesg, from_email='Focal <help@focal.io>', to_emails=','.join(to_email.keys()))

    ntfction_mesg = render_to_string('site_notification_templates/new_comment_template.html', param)
    for to_user in to_email.values():
        sn = SiteNotification.objects.create(to_user=to_user, ntype='newcomment', action=False, mesg=ntfction_mesg)

    return True


@csrf_exempt
def create_comment(request):
    if request.user.id is None:
        return HttpResponseRedirect('/login/')
    if request.method=='GET':
        return HttpResponse('must be post request')

    url = request.POST.get('url', None)
    try:
        link = Link.objects.get(link=url)
    except:
        link = None
    comment_text = request.POST.get('comment', None)
    activityids = request.POST.get('contexts', None)
    if link is None or activityids is None or comment_text is None:
        return HttpResponse('invalid data')
    activity_list = [Activity.objects.get(id=int(i)) for i in activityids.split(',') if i.isdigit()]

    parent_id = request.POST.get('parent', None)
    parent = None
    if parent_id and parent_id.isdigit():
        parent = MyComment.objects.get(id=int(parent_id))
        activity_list = [i.activitylink.activity_id for i in CommentMap.objects.filter(comment=parent)]
    
    submit_date = datetime.now()
    ret = []
    my_c = MyComment(comment_text=comment_text, parent=parent, level=(parent.level+1 if parent else 0), user=request.user, submit_date=submit_date, ipaddress='')
    my_c.save()
    
    d = {}
    to = set()
    for act in activity_list:
        al = ActivityLink.objects.get(actlink=link, activity=act)
        cm, created = CommentMap.objects.get_or_create(comment=my_c, activitylink=al)

        for each in ActivityMember.objects.get_activity_member(act):
            if each!=request.user:
                if d.has_key(each):
                    d[each].append(act)
                else:
                    d[each] = [act]

    param = {'link': link, 'activities': activity_list, 'new_comment': my_c}
    r = send_new_comment_mail(my_c, link, activity_list, param, 'new_comment')

    for mem,actl in d.items():
        notification_mesg = {'action': 'newcomment', 'activity': [a.id for a in actl], 'url':link.link, 'comment_id':my_c.id, 'text':my_c.comment_text, 'by':my_c.user.username,'date':(datetime.now()-my_c.submit_date).seconds, 'parent':parent and parent.id or ''}
        sync_obj = Synctable.objects.create(for_user=mem, notification_mesg=json.dumps(notification_mesg), notification_action_url='/')
    
    if request.GET.get('is_ajax','false') == 'true':
        return render_to_response('single_comment.html', {'actids':activityids, 'comment':my_c, 'link':link}, context_instance=RequestContext(request))
        #return HttpResponse(json.dumps({'comment_id':my_c.id, 'activity_list':[a.activityname for a in activity_list], 'url':link.link}))
    else:
        return HttpResponseRedirect(reverse('link-comment', args=[link.id]))


def _sort_func(x, y):
    if x.submit_date<y.submit_date:
        return 1
    elif x.submit_date==y.submit_date:
        return 0
    else:
        return -1


def _get_comment(usr, actlink_list, serialise=True):
    ret = []
    comment_of_al = [e_comment_map.comment for e_comment_map in CommentMap.objects.filter(activitylink__in=actlink_list)]
    d = {}
    parent = None
    for e_comment_map in CommentMap.objects.filter(activitylink__in=actlink_list):
        if d.has_key(e_comment_map.comment):
            if e_comment_map.activitylink.activity.id not in d[e_comment_map.comment]:
                d[e_comment_map.comment].append([e_comment_map.activitylink.activity.id, e_comment_map.activitylink.activity.activityname])
        else:
            d[e_comment_map.comment] = [[e_comment_map.activitylink.activity.id, e_comment_map.activitylink.activity.activityname]]
    for cmnt, act_list in d.items():
        if cmnt.parent:
            parent = cmnt.parent.id
        else:
            parent = None
        ret.append({'id':cmnt.id, 'text':cmnt.comment_text, 'by':cmnt.user.nickname,'date':cmnt.submit_date, 'activities':act_list, 'parent':parent, 'level':cmnt.level})
    if serialise:
        return json.dumps(ret)
    else:
        return ret


def urlpage(request, urlid):
    if request.user.id is None:
        return HttpResponseRedirect('/login/')
    link = Link.objects.get(id=int(urlid))
    activity_list = []
    for al in ActivityLink.objects.filter(actlink=link):
        if al.activity in [am.activity for am in ActivityMember.objects.filter(member=request.user)] or al.activity.created_by==request.user:
            activity_list.append(al.activity)
    comment = _get_comment(request.user, activity_list)
    return render_to_response('show_link.html', {'comment':comment, 'link': link, 'activity_list': activity_list, 'all_actid_str':':'.join([str(i.id) for i in activity_list])}, context_instance=RequestContext(request))


def getcomment(request):
    if request.user.id is None:
        return HttpResponseRedirect('/login/')
    u = request.user
    linkid = request.GET.get('linkid', None)
    link = None
    if linkid:
        link = Link.objects.get(id=int(linkid))

    user_activity = [e.activity for e in ActivityMember.objects.filter(member=u)] + [a for a in Activity.objects.filter(created_by=u)]
    actlink = [al for al in ActivityLink.objects.filter(actlink=link) if al.activity in user_activity]
    obj = _get_comment(request.user, actlink, False)
    return render_to_response('discuss_ajax.html',{'comments':obj, 'link':link, 'actlink':actlink}, context_instance=RequestContext(request))



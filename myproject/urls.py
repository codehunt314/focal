import os
#PROJECT_PATH = os.path.dirname(os.path.realpath(__file__))
#DOC_ROOT = os.path.join(PROJECT_PATH, '../ext_download')
#MEDIA_ROOT = '/home/vips/PersonalProject/focal/media'
from settings import MEDIA_ROOT
from django.conf.urls.defaults import *

#from focal_decocator import is_active

urlpatterns = patterns('',
    #for activity app
    (r'^$', 'activity.views.test_home'),
    #(r'^$', 'focaluser.views.home'),
    (r'^invite_request/$', 'focaluser.views.request_for_invite'),
    (r'^resend_invite/$', 'focaluser.views.resend_invite'),
    (r'^manage_invites/$', 'focaluser.views.invite_management'),
    (r'^generate_invite/$', 'focaluser.views.generate_new_invite'),
    (r'^register/$', 'focaluser.views.register'),
    (r'^authenticate/', 'focaluser.views.auth'),
    (r'^activate/', 'focaluser.views.activate'),
    (r'^login/', 'focaluser.views.login'),
    (r'^logout/', 'focaluser.views.logout'),
    url(r'^user_settings/', 'focaluser.views.user_settings', name="user_settings"),
    (r'^forgotpassword/', 'focaluser.views.forgot_password'),
    (r'^change_password/', 'focaluser.views.change_pasword'),
    (r'^confirm/', 'focaluser.views.confirm_email'),
    (r'^sharealink/$', 'activity.views.sharealink', {'act': 'in_activity'}),
    (r'^addtofav/$', 'activity.views.sharealink', {'act': 'in_favorite'}),
    (r'^context/(?P<act>[a-zA-Z0-9]+)/$', 'activity.views.test_home'),
    url(r'^context/(?P<actid>[a-zA-Z0-9]+)/manage/$', 'activity.views.activity_home', name="activity-settings"),
    (r'^user/(?P<u>[a-zA-Z0-9]+)/$', 'activity.views.test_home'),
    (r'^tag/$', 'activity.views.tag'),
    (r'^get_tags/$', 'activity.views.get_all_tags'),
    url(r'^discuss/(?P<linkid>[0-9]+)/$', 'activity.views.discuss', name="link-comment"),
    (r'^getcomment/$', 'tcomment.views.getcomment'),
    (r'^postcomment/$', 'tcomment.views.create_comment'),
    (r'^create/', 'activity.views.create_activity'),
    (r'^getlinkdetail/', 'activity.views.get_link_detail'),
    (r'^test_linkdetail/', 'activity.views.test_linkdetail'),
    (r'^link_status/', 'activity.views.get_link_page'),
    (r'^get_bookmarklet/', 'activity.views.get_bookmarklet'),
    (r'^referal/', 'activity.views.activity_referal_page'),
    (r'^act_frnd_invite/$', 'activity.views.activity_friend_invite'),
    (r'^reject_activity_invite/$', 'activity.views.reject_invitation'),
    #url(r'^activity/(?P<actid>[a-zA-Z0-9]+)/$', 'activity.views.activity_home', name="activity-settings"),
    url(r'^auth/(?P<service_name>[a-z]+)/$', 'activity.views.social_auth', name="social_auth"),
    #(r'^activity_setting/', 'activity.views.activity_settings'),
    url(r'^settings/', 'focaluser.views.account_settings', name='service_settings'),
    (r'^import_export/', 'activity.views.import_export'),
    url(r'^notification_settings/', 'focaluser.views.notification_settings', name='notification_settings'),
    url(r'^browser_plugins/', 'focaluser.views.browser_plugins', name='browser_plugins'),
    (r'^search/', 'search.views.search'),
    (r'^change_nickname/', 'focaluser.views.change_nickname'),
    (r'^change_name/', 'focaluser.views.change_name'),
    (r'^bulk_import/', 'activity.views.bulk_import'),
    (r'^import_from_file/', 'activity.views.bulk_import_from_file'),
    (r'^export/', 'activity.views.export'),
    (r'^site_notification/$', 'focaluser.views.site_notification'),
    (r'^sn_ack/(?P<notification_id>[0-9]+)/$', 'focaluser.views.sitenotification_ack'),
    (r'^redirect/', 'activity.views.redirect_link'),
    (r'^about/$', 'focaluser.views.about'),
    #rest
    (r'^site_media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': MEDIA_ROOT}),
    #(r'^mailtest/', 'activity.views.test_mail'),
)
